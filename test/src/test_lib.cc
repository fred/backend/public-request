/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test/setup/fixtures.hh"

#include "src/lib/get.hh"
#include "src/lib/get_types.hh"
#include "src/lib/invalidate.hh"
#include "src/lib/list.hh"
#include "src/lib/resolve.hh"
#include "src/unwrapper/common_types.hh"

#include <boost/optional.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/variant.hpp>

#include <algorithm>
#include <chrono>
#include <exception>
#include <map>
#include <set>
#include <tuple>
#include <vector>
#include <utility>

namespace LibFred {
namespace PublicRequest {

bool operator==(const PublicRequestData::ContactDataLight& lhs, const PublicRequestData::ContactDataLight& rhs)
{
    BOOST_CHECK_EQUAL(lhs.id, rhs.id);
    BOOST_CHECK_EQUAL(lhs.uuid, rhs.uuid);
    BOOST_CHECK_EQUAL(lhs.handle, rhs.handle);
    return std::make_tuple(lhs.id, lhs.uuid, lhs.handle) == std::make_tuple(rhs.id, rhs.uuid, rhs.handle);
}

bool operator==(const PublicRequestData::DomainDataLight& lhs, const PublicRequestData::DomainDataLight& rhs)
{
    BOOST_CHECK_EQUAL(lhs.id, rhs.id);
    BOOST_CHECK_EQUAL(lhs.uuid, rhs.uuid);
    BOOST_CHECK_EQUAL(lhs.fqdn, rhs.fqdn);
    return std::make_tuple(lhs.id, lhs.uuid, lhs.fqdn) == std::make_tuple(rhs.id, rhs.uuid, rhs.fqdn);
}

bool operator==(const PublicRequestData::KeysetDataLight& lhs, const PublicRequestData::KeysetDataLight& rhs)
{
    BOOST_CHECK_EQUAL(lhs.id, rhs.id);
    BOOST_CHECK_EQUAL(lhs.uuid, rhs.uuid);
    BOOST_CHECK_EQUAL(lhs.handle, rhs.handle);
    return std::make_tuple(lhs.id, lhs.uuid, lhs.handle) == std::make_tuple(rhs.id, rhs.uuid, rhs.handle);
}

bool operator==(const PublicRequestData::NssetDataLight& lhs, const PublicRequestData::NssetDataLight& rhs)
{
    BOOST_CHECK_EQUAL(lhs.id, rhs.id);
    BOOST_CHECK_EQUAL(lhs.uuid, rhs.uuid);
    BOOST_CHECK_EQUAL(lhs.handle, rhs.handle);
    return std::make_tuple(lhs.id, lhs.uuid, lhs.handle) == std::make_tuple(rhs.id, rhs.uuid, rhs.handle);
}

bool operator==(const PublicRequestData::RegistrarDataLight& lhs, const PublicRequestData::RegistrarDataLight& rhs)
{
    BOOST_CHECK_EQUAL(lhs.id, rhs.id);
    BOOST_CHECK_EQUAL(lhs.uuid, rhs.uuid);
    BOOST_CHECK_EQUAL(lhs.handle, rhs.handle);
    return std::make_tuple(lhs.id, lhs.uuid, lhs.handle) == std::make_tuple(rhs.id, rhs.uuid, rhs.handle);
}

}//namespace LibFred::PublicRequest
}//namespace LibFred

BOOST_AUTO_TEST_SUITE(TestLib)

namespace {

namespace Unwrapper {

class GetRequest : public Fred::PublicRequest::Unwrapper::GetRequest<GetRequest>
{
public:
    explicit GetRequest(unsigned long long id)
        : public_request_id_{std::to_string(id)}
    { }
    explicit GetRequest(const boost::uuids::uuid& uuid)
        : public_request_id_{to_string(uuid)}
    { }
    const Fred::PublicRequest::Lib::PublicRequestId& public_request_id() const
    {
        return public_request_id_;
    }
private:
    Fred::PublicRequest::Lib::PublicRequestId public_request_id_;
};

template <typename V>
class ObjectIdVisitor : boost::static_visitor<decltype(std::declval<V>()(std::declval<Fred::PublicRequest::Lib::ContactId>()))>
{
public:
    explicit ObjectIdVisitor(const V& visitor)
        : visitor_{visitor}
    { }
    template <typename T>
    decltype(auto) operator()(T&& value) const
    {
        return visitor_(std::forward<T>(value));
    }
private:
    const V& visitor_;
};

class RegistryObjectReference
{
public:
    using Id = boost::variant<
            Fred::PublicRequest::Lib::ContactId,
            Fred::PublicRequest::Lib::DomainId,
            Fred::PublicRequest::Lib::KeysetId,
            Fred::PublicRequest::Lib::NssetId>;
    explicit RegistryObjectReference(Id registry_object_reference)
        : id_{std::move(registry_object_reference)}
    { }
    template <typename Visitor>
    decltype(auto) operator()(const Visitor& visitor) const
    {
        return boost::apply_visitor(ObjectIdVisitor<Visitor>{visitor}, id_);
    }
private:
    Id id_;
};

class ListRequest : public Fred::PublicRequest::Unwrapper::ListRequest<ListRequest>
{
public:
    explicit ListRequest(RegistryObjectReference registry_object_reference)
        : registry_object_reference_{std::move(registry_object_reference)}
    { }
    decltype(auto) registry_object_reference() const
    {
        return registry_object_reference_;
    }
private:
    RegistryObjectReference registry_object_reference_;
};

class InvalidateRequest : public Fred::PublicRequest::Unwrapper::InvalidateRequest<InvalidateRequest>
{
public:
    explicit InvalidateRequest(unsigned long long id)
        : public_request_id_{std::to_string(id)}
    { }
    explicit InvalidateRequest(const boost::uuids::uuid& uuid)
        : public_request_id_{to_string(uuid)}
    { }
    InvalidateRequest& set_resolving_id(unsigned long long resolving_id)
    {
        resolving_id_ = Fred::PublicRequest::Lib::LogEntryId{std::to_string(resolving_id)};
        return *this;
    }
    Fred::PublicRequest::Lib::PublicRequestId public_request_id() const
    {
        return public_request_id_;
    }
    Fred::PublicRequest::Lib::LogEntryId resolving_id() const
    {
        if (this->has_resolving_id())
        {
            return *resolving_id_;
        }
        return Fred::PublicRequest::Lib::LogEntryId{std::string{}};
    }
    bool has_resolving_id() const
    {
        return resolving_id_ != Fred::PublicRequest::Lib::LogEntryId::nullopt;
    }
private:
    Fred::PublicRequest::Lib::PublicRequestId public_request_id_;
    Fred::PublicRequest::Lib::LogEntryId::Optional resolving_id_;
};

class ResolveRequest : public Fred::PublicRequest::Unwrapper::ResolveRequest<ResolveRequest>
{
public:
    explicit ResolveRequest(unsigned long long id)
        : public_request_id_{std::to_string(id)}
    { }
    explicit ResolveRequest(const boost::uuids::uuid& uuid)
        : public_request_id_{to_string(uuid)}
    { }
    ResolveRequest& set_resolving_id(unsigned long long resolving_id)
    {
        resolving_id_ = Fred::PublicRequest::Lib::LogEntryId{std::to_string(resolving_id)};
        return *this;
    }
    Fred::PublicRequest::Lib::PublicRequestId public_request_id() const
    {
        return public_request_id_;
    }
    Fred::PublicRequest::Lib::LogEntryId resolving_id() const
    {
        if (this->has_resolving_id())
        {
            return *resolving_id_;
        }
        return Fred::PublicRequest::Lib::LogEntryId{std::string{}};
    }
    bool has_resolving_id() const
    {
        return resolving_id_ != Fred::PublicRequest::Lib::LogEntryId::nullopt;
    }
private:
    Fred::PublicRequest::Lib::PublicRequestId public_request_id_;
    Fred::PublicRequest::Lib::LogEntryId::Optional resolving_id_;
};

}//namespace {anonymous}::Unwrapper

namespace Wrapper {

class GetReply : public Fred::PublicRequest::Wrapper::GetReply<GetReply>
{
public:
    explicit GetReply(const LibFred::PublicRequest::PublicRequestData* data)
        : data_{data},
          set_data_invoked_{false},
          set_exception_public_request_does_not_exist_invoked_{false}
    { }
    void set_data(const LibFred::PublicRequest::PublicRequestData& data)
    {
        BOOST_CHECK(set_data_invoked_ ^= true);
        BOOST_REQUIRE(data_ != nullptr);
        BOOST_CHECK_EQUAL(data.id, data_->id);
        BOOST_CHECK_EQUAL(data.uuid, data_->uuid);
        BOOST_CHECK(data.object_data == data_->object_data);
        BOOST_CHECK_EQUAL(data.type, data_->type);
        BOOST_CHECK_EQUAL(data.registrar_data == boost::none, data_->registrar_data == boost::none);
        if ((data.registrar_data != boost::none) && (data_->registrar_data != boost::none))
        {
            BOOST_CHECK(*data.registrar_data == *(data_->registrar_data));
        }
        BOOST_CHECK_EQUAL(data.create_time.time_since_epoch().count(), data_->create_time.time_since_epoch().count());
        BOOST_CHECK_EQUAL(data.create_request_id, data_->create_request_id);
        BOOST_CHECK_EQUAL(data.resolve_time == boost::none, data_->resolve_time == boost::none);
        if ((data.resolve_time != boost::none) && (data_->resolve_time != boost::none))
        {
            BOOST_CHECK_EQUAL(data.resolve_time->time_since_epoch().count(), data_->resolve_time->time_since_epoch().count());
        }
        BOOST_CHECK_EQUAL(data.resolve_request_id, data_->resolve_request_id);
        BOOST_CHECK_EQUAL(data.status, data_->status);
        BOOST_CHECK_EQUAL(data.on_status_action, data_->on_status_action);
        BOOST_CHECK_EQUAL(data.email_to_answer, data_->email_to_answer);
    }
    void set_exception_public_request_does_not_exist()
    {
        BOOST_CHECK(set_exception_public_request_does_not_exist_invoked_ ^= true);
    }
    bool set_data_invoked() const
    {
        return set_data_invoked_ && !set_exception_public_request_does_not_exist_invoked_;
    }
    bool set_exception_public_request_does_not_exist_invoked() const
    {
        return set_exception_public_request_does_not_exist_invoked_ && !set_data_invoked_;
    }
private:
    const LibFred::PublicRequest::PublicRequestData* data_;
    bool set_data_invoked_;
    bool set_exception_public_request_does_not_exist_invoked_;
};

class GetTypesReply : public Fred::PublicRequest::Wrapper::GetTypesReply<GetTypesReply>
{
public:
    using TypeTraitsIterator = std::map<std::string, Fred::PublicRequest::Lib::PublicRequestTypeTraits>::const_iterator;
    void set_types(std::size_t size, TypeTraitsIterator begin, TypeTraitsIterator end)
    {
        static const std::set<std::string> unresolvable = {
                "contact_conditional_identification",
                "contact_identification",
                "mojeid_conditionally_identified_contact_transfer",
                "mojeid_contact_conditional_identification",
                "mojeid_contact_identification",
                "mojeid_contact_reidentification",
                "mojeid_identified_contact_transfer",
                "mojeid_prevalidated_contact_transfer",
                "mojeid_prevalidated_unidentified_contact_transfer"};
        std::for_each(begin, end, [&](auto&& name_traits_pair)
        {
            const bool is_resolvable = std::find(
                    std::begin(unresolvable),
                    std::end(unresolvable),
                    name_traits_pair.first) == std::end(unresolvable);
            BOOST_CHECK_MESSAGE(name_traits_pair.second.is_resolvable == is_resolvable,
                                name_traits_pair.first << " has to be " <<
                                (is_resolvable ? "resolvable" : "unresolvable"));
            --size;
        });
        BOOST_CHECK_EQUAL(size, 0);
    }
};

class ListReply : public Fred::PublicRequest::Wrapper::ListReply<ListReply>
{
public:
    using PublicRequests = std::vector<Test::PublicRequest>;
    explicit ListReply(PublicRequests data = {})
        : data_{std::move(data)},
          set_public_requests_invoked_{false},
          finish_invoked_{false}
    {
        std::sort(begin(data_), end(data_), [](const Test::PublicRequest& lhs, const Test::PublicRequest& rhs)
        {
            return std::make_tuple(rhs.data.create_time, rhs.data.id) < std::make_tuple(lhs.data.create_time, lhs.data.id);
        });
    }
    using Iter = std::vector<LibFred::PublicRequest::PublicRequestData>::const_iterator;
    void set_public_requests(std::size_t size, Iter begin, Iter end)
    {
        BOOST_CHECK(set_public_requests_invoked_ ^= true);
        BOOST_REQUIRE_EQUAL(size, end - begin);
        BOOST_REQUIRE_EQUAL(size, data_.size());
        auto src_iter = std::begin(data_);
        std::for_each(begin, end, [&](auto&& result)
        {
            BOOST_CHECK_EQUAL(result.id, src_iter->data.id);
            BOOST_CHECK_EQUAL(result.uuid, src_iter->data.uuid);
            BOOST_CHECK(result.object_data == src_iter->data.object_data);
            BOOST_CHECK_EQUAL(result.type, src_iter->data.type);
            BOOST_CHECK_EQUAL(result.registrar_data == boost::none, src_iter->data.registrar_data == boost::none);
            if ((result.registrar_data != boost::none) && (src_iter->data.registrar_data != boost::none))
            {
                BOOST_CHECK(*result.registrar_data == *(src_iter->data.registrar_data));
            }
            BOOST_CHECK_EQUAL(result.create_time.time_since_epoch().count(), src_iter->data.create_time.time_since_epoch().count());
            BOOST_CHECK_EQUAL(result.create_request_id, src_iter->data.create_request_id);
            BOOST_CHECK_EQUAL(result.resolve_time == boost::none, src_iter->data.resolve_time == boost::none);
            if ((result.resolve_time != boost::none) && (src_iter->data.resolve_time != boost::none))
            {
                BOOST_CHECK_EQUAL(result.resolve_time->time_since_epoch().count(), src_iter->data.resolve_time->time_since_epoch().count());
            }
            BOOST_CHECK_EQUAL(result.resolve_request_id, src_iter->data.resolve_request_id);
            BOOST_CHECK_EQUAL(result.status, src_iter->data.status);
            BOOST_CHECK_EQUAL(result.on_status_action, src_iter->data.on_status_action);
            BOOST_CHECK_EQUAL(result.email_to_answer, src_iter->data.email_to_answer);
            ++src_iter;
        });
        BOOST_CHECK(src_iter == std::end(data_));
    }
    void finish() &&
    {
        BOOST_CHECK(finish_invoked_ ^= true);
    }
    bool set_public_requests_invoked() const
    {
        return set_public_requests_invoked_;
    }
    bool finish_invoked() const
    {
        return finish_invoked_;
    }
private:
    PublicRequests data_;
    bool set_public_requests_invoked_;
    bool finish_invoked_;
};

class InvalidateReply : public Fred::PublicRequest::Wrapper::InvalidateReply<InvalidateReply>
{
public:
    explicit InvalidateReply()
        : set_exception_public_request_does_not_exist_invoked_{false},
          set_exception_cannot_be_processed_invoked_{false}
    { }
    void set_exception_public_request_does_not_exist()
    {
        BOOST_CHECK(set_exception_public_request_does_not_exist_invoked_ ^= true);
    }
    void set_exception_cannot_be_processed()
    {
        BOOST_CHECK(set_exception_cannot_be_processed_invoked_ ^= true);
    }
    bool set_exception_public_request_does_not_exist_invoked() const
    {
        return set_exception_public_request_does_not_exist_invoked_;
    }
    bool set_exception_cannot_be_processed_invoked() const
    {
        return set_exception_cannot_be_processed_invoked_;
    }
private:
    bool set_exception_public_request_does_not_exist_invoked_;
    bool set_exception_cannot_be_processed_invoked_;
};

class ResolveReply : public Fred::PublicRequest::Wrapper::ResolveReply<ResolveReply>
{
public:
    explicit ResolveReply()
        : set_exception_public_request_does_not_exist_invoked_{false},
          set_exception_cannot_be_processed_invoked_{false}
    { }
    void set_exception_public_request_does_not_exist()
    {
        BOOST_CHECK(set_exception_public_request_does_not_exist_invoked_ ^= true);
    }
    void set_exception_cannot_be_processed()
    {
        BOOST_CHECK(set_exception_cannot_be_processed_invoked_ ^= true);
    }
    bool set_exception_public_request_does_not_exist_invoked() const
    {
        return set_exception_public_request_does_not_exist_invoked_;
    }
    bool set_exception_cannot_be_processed_invoked() const
    {
        return set_exception_cannot_be_processed_invoked_;
    }
private:
    bool set_exception_public_request_does_not_exist_invoked_;
    bool set_exception_cannot_be_processed_invoked_;
};

}//namespace {anonymous}::Wrapper

}//namespace {anonymous}

BOOST_FIXTURE_TEST_CASE(get_types_ro, Test::RoTxFixture<>)
{
    auto wrapper = Wrapper::GetTypesReply{};
    Fred::PublicRequest::Lib::get_types(ctx, wrapper);
}

BOOST_FIXTURE_TEST_CASE(get_types_rw, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::GetTypesReply{};
    Fred::PublicRequest::Lib::get_types(ctx, wrapper);
}

BOOST_FIXTURE_TEST_CASE(get_by_no_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::GetReply{&db_data.no_public_request_c[0].data};
    Fred::PublicRequest::Lib::get(
            ctx,
            Unwrapper::GetRequest{db_data.no_public_request_c[0].data.id},
            wrapper);
    BOOST_CHECK(wrapper.set_exception_public_request_does_not_exist_invoked());
}

BOOST_FIXTURE_TEST_CASE(get_c1_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_c1), end(db_data.public_request_c1), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.id},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_c1.size());
    BOOST_CHECK_EQUAL(cnt, 1);
}

BOOST_FIXTURE_TEST_CASE(get_c1_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_c1), end(db_data.public_request_c1), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.uuid},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_c1.size());
    BOOST_CHECK_EQUAL(cnt, 1);
}

BOOST_FIXTURE_TEST_CASE(get_c2_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_c2), end(db_data.public_request_c2), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.id},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_c2.size());
    BOOST_CHECK_EQUAL(cnt, 9);
}

BOOST_FIXTURE_TEST_CASE(get_c2_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_c2), end(db_data.public_request_c2), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.uuid},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_c2.size());
    BOOST_CHECK_EQUAL(cnt, 9);
}

BOOST_FIXTURE_TEST_CASE(get_d1_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_d1), end(db_data.public_request_d1), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.id},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_d1.size());
    BOOST_CHECK_EQUAL(cnt, 1);
}

BOOST_FIXTURE_TEST_CASE(get_d1_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_d1), end(db_data.public_request_d1), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.uuid},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_d1.size());
    BOOST_CHECK_EQUAL(cnt, 1);
}

BOOST_FIXTURE_TEST_CASE(get_d2_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_d2), end(db_data.public_request_d2), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.id},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_d2.size());
    BOOST_CHECK_EQUAL(cnt, 9);
}

BOOST_FIXTURE_TEST_CASE(get_d2_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_d2), end(db_data.public_request_d2), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.uuid},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_d2.size());
    BOOST_CHECK_EQUAL(cnt, 9);
}

BOOST_FIXTURE_TEST_CASE(get_k1_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_k1), end(db_data.public_request_k1), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.id},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_k1.size());
    BOOST_CHECK_EQUAL(cnt, 1);
}

BOOST_FIXTURE_TEST_CASE(get_k1_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_k1), end(db_data.public_request_k1), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.uuid},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_k1.size());
    BOOST_CHECK_EQUAL(cnt, 1);
}

BOOST_FIXTURE_TEST_CASE(get_k2_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_k2), end(db_data.public_request_k2), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.id},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_k2.size());
    BOOST_CHECK_EQUAL(cnt, 9);
}

BOOST_FIXTURE_TEST_CASE(get_k2_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_k2), end(db_data.public_request_k2), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.uuid},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_k2.size());
    BOOST_CHECK_EQUAL(cnt, 9);
}

BOOST_FIXTURE_TEST_CASE(get_n1_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_n1), end(db_data.public_request_n1), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.id},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_n1.size());
    BOOST_CHECK_EQUAL(cnt, 1);
}

BOOST_FIXTURE_TEST_CASE(get_n1_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_n1), end(db_data.public_request_n1), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.uuid},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_n1.size());
    BOOST_CHECK_EQUAL(cnt, 1);
}

BOOST_FIXTURE_TEST_CASE(get_n2_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_n2), end(db_data.public_request_n2), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.id},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_n2.size());
    BOOST_CHECK_EQUAL(cnt, 9);
}

BOOST_FIXTURE_TEST_CASE(get_n2_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    std::for_each(begin(db_data.public_request_n2), end(db_data.public_request_n2), [&](auto&& public_request)
    {
        ++cnt;
        auto wrapper = Wrapper::GetReply{&public_request.data};
        Fred::PublicRequest::Lib::get(
                ctx,
                Unwrapper::GetRequest{public_request.data.uuid},
                wrapper);
        BOOST_CHECK(wrapper.set_data_invoked());
    });
    BOOST_CHECK_EQUAL(cnt, db_data.public_request_n2.size());
    BOOST_CHECK_EQUAL(cnt, 9);
}

BOOST_FIXTURE_TEST_CASE(list_no_c_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{std::to_string(db_data.no_contact.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_no_c_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{to_string(get_raw_value_from(db_data.no_contact.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_no_c_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{db_data.no_contact.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c0_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{std::to_string(db_data.contact_0.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c0_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{to_string(get_raw_value_from(db_data.contact_0.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c0_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{db_data.contact_0.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c1_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_c1), end(db_data.public_request_c1)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{std::to_string(db_data.contact_1.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c1_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_c1), end(db_data.public_request_c1)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{to_string(get_raw_value_from(db_data.contact_1.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c1_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_c1), end(db_data.public_request_c1)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{db_data.contact_1.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c2_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_c2), end(db_data.public_request_c2)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{std::to_string(db_data.contact_2.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c2_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_c2), end(db_data.public_request_c2)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{to_string(get_raw_value_from(db_data.contact_2.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c2_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_c2), end(db_data.public_request_c2)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{db_data.contact_2.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_no_d_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{std::to_string(db_data.no_domain.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_no_d_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{to_string(get_raw_value_from(db_data.no_domain.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_no_d_by_fqdn, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{db_data.no_domain.data.fqdn}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_d0_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{std::to_string(db_data.domain_0.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_d0_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{to_string(get_raw_value_from(db_data.domain_0.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_d0_by_fqdn, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{db_data.domain_0.data.fqdn}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_d1_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_d1), end(db_data.public_request_d1)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{std::to_string(db_data.domain_1.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_d1_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_d1), end(db_data.public_request_d1)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{to_string(get_raw_value_from(db_data.domain_1.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_d1_by_fqdn, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_d1), end(db_data.public_request_d1)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{db_data.domain_1.data.fqdn}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_d2_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_d2), end(db_data.public_request_d2)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{std::to_string(db_data.domain_2.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_d2_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_d2), end(db_data.public_request_d2)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{to_string(get_raw_value_from(db_data.domain_2.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_d2_by_fqdn, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_d2), end(db_data.public_request_d2)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{db_data.domain_2.data.fqdn}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_no_k_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{std::to_string(db_data.no_keyset.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_no_k_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{to_string(get_raw_value_from(db_data.no_keyset.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_no_k_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{db_data.no_keyset.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k0_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{std::to_string(db_data.keyset_0.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k0_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{to_string(get_raw_value_from(db_data.keyset_0.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k0_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{db_data.keyset_0.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k1_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_k1), end(db_data.public_request_k1)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{std::to_string(db_data.keyset_1.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k1_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_k1), end(db_data.public_request_k1)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{to_string(get_raw_value_from(db_data.keyset_1.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k1_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_k1), end(db_data.public_request_k1)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{db_data.keyset_1.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k2_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_k2), end(db_data.public_request_k2)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{std::to_string(db_data.keyset_2.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k2_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_k2), end(db_data.public_request_k2)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{to_string(get_raw_value_from(db_data.keyset_2.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k2_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_k2), end(db_data.public_request_k2)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{db_data.keyset_2.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_no_n_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{std::to_string(db_data.no_nsset.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_no_n_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{to_string(get_raw_value_from(db_data.no_nsset.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_no_n_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{db_data.no_nsset.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n0_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{std::to_string(db_data.nsset_0.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n0_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{to_string(get_raw_value_from(db_data.nsset_0.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n0_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{db_data.nsset_0.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n1_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_n1), end(db_data.public_request_n1)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{std::to_string(db_data.nsset_1.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n1_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_n1), end(db_data.public_request_n1)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{to_string(get_raw_value_from(db_data.nsset_1.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n1_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_n1), end(db_data.public_request_n1)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{db_data.nsset_1.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n2_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_n2), end(db_data.public_request_n2)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{std::to_string(db_data.nsset_2.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n2_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_n2), end(db_data.public_request_n2)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{to_string(get_raw_value_from(db_data.nsset_2.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n2_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_n2), end(db_data.public_request_n2)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{db_data.nsset_2.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c_by_id_type_mismatch_d, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{std::to_string(db_data.contact_2.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c_by_uuid_type_mismatch_k, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{to_string(get_raw_value_from(db_data.contact_2.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c_by_handle_type_mismatch_n, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{db_data.contact_2.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_d_by_id_type_mismatch_c, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{std::to_string(db_data.domain_2.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_d_by_uuid_type_mismatch_k, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{to_string(get_raw_value_from(db_data.domain_2.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_d_by_fqdn_type_mismatch_n, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{db_data.domain_2.data.fqdn}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k_by_id_type_mismatch_c, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{std::to_string(db_data.keyset_2.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k_by_uuid_type_mismatch_d, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{to_string(get_raw_value_from(db_data.keyset_2.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k_by_handle_type_mismatch_n, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{db_data.keyset_2.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n_by_id_type_mismatch_c, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{std::to_string(db_data.nsset_2.data.id)}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n_by_uuid_type_mismatch_d, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::DomainId{to_string(get_raw_value_from(db_data.nsset_2.data.uuid))}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n_by_handle_type_mismatch_k, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{db_data.nsset_2.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_c_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_c), end(db_data.public_request_c)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::ContactId{db_data.object_c.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_k_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_k), end(db_data.public_request_k)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::KeysetId{db_data.object_k.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(list_n_by_handle, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::ListReply{{begin(db_data.public_request_n), end(db_data.public_request_n)}};
    Fred::PublicRequest::Lib::list(
            ctx,
            Unwrapper::ListRequest{Unwrapper::RegistryObjectReference{Fred::PublicRequest::Lib::NssetId{db_data.object_n.data.handle}}},
            wrapper);
    BOOST_CHECK(wrapper.set_public_requests_invoked());
    BOOST_CHECK(wrapper.finish_invoked());
}

BOOST_FIXTURE_TEST_CASE(invalidate_no_c_by_id, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::InvalidateReply{};
    Fred::PublicRequest::Lib::invalidate(
            ctx,
            Unwrapper::InvalidateRequest{db_data.no_public_request_c[0].data.id},
            wrapper);
    BOOST_CHECK(wrapper.set_exception_public_request_does_not_exist_invoked());
    BOOST_CHECK(!wrapper.set_exception_cannot_be_processed_invoked());
}

BOOST_FIXTURE_TEST_CASE(invalidate_no_c_by_uuid, Test::RwTxFixture<>)
{
    auto wrapper = Wrapper::InvalidateReply{};
    Fred::PublicRequest::Lib::invalidate(
            ctx,
            Unwrapper::InvalidateRequest{db_data.no_public_request_c[0].data.uuid},
            wrapper);
    BOOST_CHECK(wrapper.set_exception_public_request_does_not_exist_invoked());
    BOOST_CHECK(!wrapper.set_exception_cannot_be_processed_invoked());
}

BOOST_FIXTURE_TEST_CASE(invalidate_c1_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_c1), end(db_data.public_request_c1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(invalidated, 1);
}

BOOST_FIXTURE_TEST_CASE(invalidate_c1_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_c1), end(db_data.public_request_c1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.uuid}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(invalidated, 1);
}

BOOST_FIXTURE_TEST_CASE(invalidate_c2_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_c2), end(db_data.public_request_c2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(invalidated, 3);
}

BOOST_FIXTURE_TEST_CASE(invalidate_c2_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_c2), end(db_data.public_request_c2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.uuid}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(invalidated, 3);
}

BOOST_FIXTURE_TEST_CASE(invalidate_d1_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_d1), end(db_data.public_request_d1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(invalidated, 1);
}

BOOST_FIXTURE_TEST_CASE(invalidate_d1_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_d1), end(db_data.public_request_d1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.uuid}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(invalidated, 1);
}

BOOST_FIXTURE_TEST_CASE(invalidate_d2_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_d2), end(db_data.public_request_d2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(invalidated, 3);
}

BOOST_FIXTURE_TEST_CASE(invalidate_d2_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_d2), end(db_data.public_request_d2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.uuid}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(invalidated, 3);
}

BOOST_FIXTURE_TEST_CASE(invalidate_k1_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_k1), end(db_data.public_request_k1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(invalidated, 1);
}

BOOST_FIXTURE_TEST_CASE(invalidate_k1_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_k1), end(db_data.public_request_k1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.uuid}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(invalidated, 1);
}

BOOST_FIXTURE_TEST_CASE(invalidate_k2_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_k2), end(db_data.public_request_k2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(invalidated, 3);
}

BOOST_FIXTURE_TEST_CASE(invalidate_k2_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_k2), end(db_data.public_request_k2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.uuid}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(invalidated, 3);
}

BOOST_FIXTURE_TEST_CASE(invalidate_n1_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_n1), end(db_data.public_request_n1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(invalidated, 1);
}

BOOST_FIXTURE_TEST_CASE(invalidate_n1_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_n1), end(db_data.public_request_n1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.uuid}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(invalidated, 1);
}

BOOST_FIXTURE_TEST_CASE(invalidate_n2_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_n2), end(db_data.public_request_n2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(invalidated, 3);
}

BOOST_FIXTURE_TEST_CASE(invalidate_n2_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int invalidated = 0;
    std::for_each(begin(db_data.public_request_n2), end(db_data.public_request_n2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::InvalidateReply{};
        Fred::PublicRequest::Lib::invalidate(
                ctx,
                Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened);
        if (public_request.data.status == LibFred::PublicRequest::Status::opened)
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.uuid}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::invalidated);
            wrapper = Wrapper::InvalidateReply{};
            Fred::PublicRequest::Lib::invalidate(
                    ctx,
                    Unwrapper::InvalidateRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++invalidated;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(invalidated, 3);
}

BOOST_FIXTURE_TEST_CASE(resolve_c1_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_c1), end(db_data.public_request_c1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(resolved, 1);
}

BOOST_FIXTURE_TEST_CASE(resolve_c1_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_c1), end(db_data.public_request_c1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(resolved, 1);
}

BOOST_FIXTURE_TEST_CASE(resolve_c2_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_c2), end(db_data.public_request_c2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(resolved, 3);
}

BOOST_FIXTURE_TEST_CASE(resolve_c2_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_c2), end(db_data.public_request_c2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.uuid}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(resolved, 3);
}

BOOST_FIXTURE_TEST_CASE(resolve_d1_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_d1), end(db_data.public_request_d1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(resolved, 1);
}

BOOST_FIXTURE_TEST_CASE(resolve_d1_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_d1), end(db_data.public_request_d1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(resolved, 1);
}

BOOST_FIXTURE_TEST_CASE(resolve_d2_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_d2), end(db_data.public_request_d2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(resolved, 2);
}

BOOST_FIXTURE_TEST_CASE(resolve_d2_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_d2), end(db_data.public_request_d2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.uuid}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(resolved, 2);
}

BOOST_FIXTURE_TEST_CASE(resolve_k1_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_k1), end(db_data.public_request_k1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(resolved, 1);
}

BOOST_FIXTURE_TEST_CASE(resolve_k1_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_k1), end(db_data.public_request_k1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(resolved, 1);
}

BOOST_FIXTURE_TEST_CASE(resolve_k2_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_k2), end(db_data.public_request_k2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(resolved, 0);
}

BOOST_FIXTURE_TEST_CASE(resolve_k2_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_k2), end(db_data.public_request_k2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.uuid}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(resolved, 0);
}

BOOST_FIXTURE_TEST_CASE(resolve_n1_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_n1), end(db_data.public_request_n1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(resolved, 1);
}

BOOST_FIXTURE_TEST_CASE(resolve_n1_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_n1), end(db_data.public_request_n1), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 1);
    BOOST_CHECK_EQUAL(resolved, 1);
}

BOOST_FIXTURE_TEST_CASE(resolve_n2_by_id, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_n2), end(db_data.public_request_n2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.id}.exec(ctx);
            BOOST_CHECK_EQUAL(data.id, public_request.data.id);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.id}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(resolved, 3);
}

BOOST_FIXTURE_TEST_CASE(resolve_n2_by_uuid, Test::RwTxFixture<>)
{
    int cnt = 0;
    int resolved = 0;
    const auto type_traits = Fred::PublicRequest::Lib::get_type_traits(ctx);
    const auto is_resolvable = [&](const Test::PublicRequest& public_request)
    {
        const auto type_traits_iter = type_traits.find(public_request.data.type);
        return type_traits_iter == end(type_traits) || type_traits_iter->second.is_resolvable;
    };
    std::for_each(begin(db_data.public_request_n2), end(db_data.public_request_n2), [&](auto&& public_request)
    {
        auto wrapper = Wrapper::ResolveReply{};
        Fred::PublicRequest::Lib::resolve(
                ctx,
                Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                wrapper);
        BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
        BOOST_CHECK_EQUAL(wrapper.set_exception_cannot_be_processed_invoked(),
                          public_request.data.status != LibFred::PublicRequest::Status::opened ||
                          !is_resolvable(public_request));
        if (public_request.data.status == LibFred::PublicRequest::Status::opened &&
            is_resolvable(public_request))
        {
            const auto data = LibFred::PublicRequest::GetPublicRequest{public_request.data.uuid}.exec(ctx);
            BOOST_CHECK_EQUAL(data.uuid, public_request.data.uuid);
            BOOST_CHECK_EQUAL(data.resolve_request_id, std::to_string(cnt));
            BOOST_CHECK(data.status == LibFred::PublicRequest::Status::resolved);
            wrapper = Wrapper::ResolveReply{};
            Fred::PublicRequest::Lib::resolve(
                    ctx,
                    Unwrapper::ResolveRequest{public_request.data.uuid}.set_resolving_id(cnt),
                    wrapper);
            BOOST_CHECK(!wrapper.set_exception_public_request_does_not_exist_invoked());
            BOOST_CHECK(wrapper.set_exception_cannot_be_processed_invoked());
            ++resolved;
        }
        ++cnt;
    });
    BOOST_CHECK_EQUAL(cnt, 9);
    BOOST_CHECK_EQUAL(resolved, 3);
}

BOOST_AUTO_TEST_SUITE_END()//TestLib
