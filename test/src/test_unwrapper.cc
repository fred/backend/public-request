/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/unwrapper/common_types.hh"
#include "src/unwrapper/get_request.hh"
#include "src/unwrapper/invalidate_request.hh"
#include "src/unwrapper/list_request.hh"
#include "src/unwrapper/resolve_request.hh"

#include <boost/test/unit_test.hpp>

#include <exception>
#include <iostream>

namespace {

namespace FakeApi {

enum class RegistryObjectType
{
    contact,
    domain,
    keyset,
    nsset
};

std::ostream& operator<<(std::ostream& out, RegistryObjectType value)
{
    return out << [](RegistryObjectType value)
    {
        switch (value)
        {
            case RegistryObjectType::contact: return "contact";
            case RegistryObjectType::domain: return "domain";
            case RegistryObjectType::keyset: return "keyset";
            case RegistryObjectType::nsset: return "nsset";
        }
        return "unknown";
    }(value);
}

struct GetRequest
{
    std::string public_request_id;
};

struct RegistryObjectReference
{
    RegistryObjectType type;
    std::string value;
};

struct ListRequest
{
    RegistryObjectReference registry_object_reference;
};

struct InvalidateRequest
{
    std::string public_request_id;
    std::string resolving_id;
};

struct ResolveRequest
{
    std::string public_request_id;
    std::string resolving_id;
};

}//namespace {anonymous}::FakeApi

namespace Unwrapper {
namespace FakeRpc {

class GetRequest : public Fred::PublicRequest::Unwrapper::GetRequest<GetRequest>
{
public:
    const std::string& public_request_id() const
    {
        return data_.public_request_id;
    }
private:
    explicit GetRequest(const FakeApi::GetRequest& data)
        : data_{data}
    { }
    const FakeApi::GetRequest& data_;
    friend GetRequest make_unwrapper(const FakeApi::GetRequest& data)
    {
        return GetRequest{data};
    }
};
GetRequest make_unwrapper(const FakeApi::GetRequest& data);

class RegistryObjectReference : public Fred::PublicRequest::Unwrapper::RegistryObjectReference<RegistryObjectReference>
{
public:
    template <typename Visitor>
    decltype(auto) operator()(const Visitor& visitor) const
    {
        switch (data_.type)
        {
            case FakeApi::RegistryObjectType::contact :
                return visitor(Fred::PublicRequest::Lib::ContactId{data_.value});
            case FakeApi::RegistryObjectType::domain:
                return visitor(Fred::PublicRequest::Lib::DomainId{data_.value});
            case FakeApi::RegistryObjectType::keyset:
                return visitor(Fred::PublicRequest::Lib::KeysetId{data_.value});
            case FakeApi::RegistryObjectType::nsset:
                return visitor(Fred::PublicRequest::Lib::NssetId{data_.value});
            default:
                break;
        }
        struct UnknownRegistryObjectType : std::exception
        {
            const char* what() const noexcept override { return "Unknown object type"; }
        };
        throw UnknownRegistryObjectType{};
    }
private:
    explicit RegistryObjectReference(const FakeApi::RegistryObjectReference& data)
        : data_{data}
    { }
    const FakeApi::RegistryObjectReference& data_;
    friend RegistryObjectReference make_unwrapper(const FakeApi::RegistryObjectReference& data)
    {
        return RegistryObjectReference{data};
    }
};
RegistryObjectReference make_unwrapper(const FakeApi::RegistryObjectReference& data);

class CheckObjectId
{
public:
    explicit CheckObjectId(const FakeApi::RegistryObjectReference& data)
        : data_{data}
    { }
    void operator()(const Fred::PublicRequest::Lib::ContactId& id) const
    {
        BOOST_CHECK_EQUAL(data_.type, FakeApi::RegistryObjectType::contact);
        BOOST_CHECK_EQUAL(*id, data_.value);
    }
    void operator()(const Fred::PublicRequest::Lib::DomainId& id) const
    {
        BOOST_CHECK_EQUAL(data_.type, FakeApi::RegistryObjectType::domain);
        BOOST_CHECK_EQUAL(*id, data_.value);
    }
    void operator()(const Fred::PublicRequest::Lib::KeysetId& id) const
    {
        BOOST_CHECK_EQUAL(data_.type, FakeApi::RegistryObjectType::keyset);
        BOOST_CHECK_EQUAL(*id, data_.value);
    }
    void operator()(const Fred::PublicRequest::Lib::NssetId& id) const
    {
        BOOST_CHECK_EQUAL(data_.type, FakeApi::RegistryObjectType::nsset);
        BOOST_CHECK_EQUAL(*id, data_.value);
    }
private:
    const FakeApi::RegistryObjectReference& data_;
};

class ListRequest : public Fred::PublicRequest::Unwrapper::ListRequest<ListRequest>
{
public:
    RegistryObjectReference registry_object_reference() const
    {
        return make_unwrapper(data_.registry_object_reference);
    }
private:
    explicit ListRequest(const FakeApi::ListRequest& data)
        : data_{data}
    { }
    const FakeApi::ListRequest& data_;
    friend ListRequest make_unwrapper(const FakeApi::ListRequest& data)
    {
        return ListRequest{data};
    }
};
ListRequest make_unwrapper(const FakeApi::ListRequest& data);

class InvalidateRequest : public Fred::PublicRequest::Unwrapper::InvalidateRequest<InvalidateRequest>
{
public:
    Fred::PublicRequest::Lib::PublicRequestId public_request_id() const
    {
        return Fred::PublicRequest::Lib::PublicRequestId{data_.public_request_id};
    }
    Fred::PublicRequest::Lib::LogEntryId resolving_id() const
    {
        return Fred::PublicRequest::Lib::LogEntryId{data_.resolving_id};
    }
private:
    explicit InvalidateRequest(const FakeApi::InvalidateRequest& data)
        : data_{data}
    { }
    const FakeApi::InvalidateRequest& data_;
    friend InvalidateRequest make_unwrapper(const FakeApi::InvalidateRequest& data)
    {
        return InvalidateRequest{data};
    }
};
InvalidateRequest make_unwrapper(const FakeApi::InvalidateRequest&);

class ResolveRequest : public Fred::PublicRequest::Unwrapper::ResolveRequest<ResolveRequest>
{
public:
    Fred::PublicRequest::Lib::PublicRequestId public_request_id() const
    {
        return Fred::PublicRequest::Lib::PublicRequestId{data_.public_request_id};
    }
    Fred::PublicRequest::Lib::LogEntryId resolving_id() const
    {
        return Fred::PublicRequest::Lib::LogEntryId{data_.resolving_id};
    }
private:
    explicit ResolveRequest(const FakeApi::ResolveRequest& data)
        : data_{data}
    { }
    const FakeApi::ResolveRequest& data_;
    friend ResolveRequest make_unwrapper(const FakeApi::ResolveRequest& data)
    {
        return ResolveRequest{data};
    }
};
ResolveRequest make_unwrapper(const FakeApi::ResolveRequest&);

}//namespace {anonymous}::Unwrapper::FakeRpc
}//namespace {anonymous}::Unwrapper

template <template <typename> typename> struct HasUnwrapper;

template <>
struct HasUnwrapper<Fred::PublicRequest::Unwrapper::GetRequest>
{
    explicit HasUnwrapper()
        : api_data{"request id"},
          unwrapper_impl{Unwrapper::FakeRpc::make_unwrapper(api_data)},
          unwrapper{unwrapper_impl}
    { }
    FakeApi::GetRequest api_data;
    Unwrapper::FakeRpc::GetRequest unwrapper_impl;
    const Fred::PublicRequest::Unwrapper::GetRequest<Unwrapper::FakeRpc::GetRequest>& unwrapper;
};
using HasGetRequestUnwrapper = HasUnwrapper<Fred::PublicRequest::Unwrapper::GetRequest>;

template <>
struct HasUnwrapper<Fred::PublicRequest::Unwrapper::RegistryObjectReference>
{
    explicit HasUnwrapper(FakeApi::RegistryObjectType object_type, std::string registry_object_reference)
        : api_data{object_type, std::move(registry_object_reference)},
          unwrapper_impl{Unwrapper::FakeRpc::make_unwrapper(api_data)},
          unwrapper{unwrapper_impl}
    { }
    FakeApi::RegistryObjectReference api_data;
    Unwrapper::FakeRpc::RegistryObjectReference unwrapper_impl;
    const Fred::PublicRequest::Unwrapper::RegistryObjectReference<Unwrapper::FakeRpc::RegistryObjectReference>& unwrapper;
};
using HasObjectIdUnwrapper = HasUnwrapper<Fred::PublicRequest::Unwrapper::RegistryObjectReference>;

struct HasContactIdUnwrapper : HasObjectIdUnwrapper
{
    explicit HasContactIdUnwrapper()
        : HasObjectIdUnwrapper{FakeApi::RegistryObjectType::contact, "contact id"}
    { }
};

struct HasDomainIdUnwrapper : HasObjectIdUnwrapper
{
    explicit HasDomainIdUnwrapper()
        : HasObjectIdUnwrapper{FakeApi::RegistryObjectType::domain, "domain id"}
    { }
};

struct HasKeysetIdUnwrapper : HasObjectIdUnwrapper
{
    explicit HasKeysetIdUnwrapper()
        : HasObjectIdUnwrapper{FakeApi::RegistryObjectType::keyset, "keyset id"}
    { }
};

struct HasNssetIdUnwrapper : HasObjectIdUnwrapper
{
    explicit HasNssetIdUnwrapper()
        : HasObjectIdUnwrapper{FakeApi::RegistryObjectType::nsset, "nsset id"}
    { }
};

template <>
struct HasUnwrapper<Fred::PublicRequest::Unwrapper::ListRequest>
{
    explicit HasUnwrapper(FakeApi::RegistryObjectType object_type, std::string registry_object_reference)
        : api_data{{object_type, std::move(registry_object_reference)}},
          unwrapper_impl{Unwrapper::FakeRpc::make_unwrapper(api_data)},
          unwrapper{unwrapper_impl}
    { }
    FakeApi::ListRequest api_data;
    Unwrapper::FakeRpc::ListRequest unwrapper_impl;
    const Fred::PublicRequest::Unwrapper::ListRequest<Unwrapper::FakeRpc::ListRequest>& unwrapper;
};
using HasListRequestUnwrapper = HasUnwrapper<Fred::PublicRequest::Unwrapper::ListRequest>;

struct HasContactListRequestUnwrapper : HasListRequestUnwrapper
{
    explicit HasContactListRequestUnwrapper()
        : HasListRequestUnwrapper{FakeApi::RegistryObjectType::contact, "contact id"}
    { }
};

struct HasDomainListRequestUnwrapper : HasListRequestUnwrapper
{
    explicit HasDomainListRequestUnwrapper()
        : HasListRequestUnwrapper{FakeApi::RegistryObjectType::domain, "domain id"}
    { }
};

struct HasKeysetListRequestUnwrapper : HasListRequestUnwrapper
{
    explicit HasKeysetListRequestUnwrapper()
        : HasListRequestUnwrapper{FakeApi::RegistryObjectType::keyset, "keyset id"}
    { }
};

struct HasNssetListRequestUnwrapper : HasListRequestUnwrapper
{
    explicit HasNssetListRequestUnwrapper()
        : HasListRequestUnwrapper{FakeApi::RegistryObjectType::nsset, "nsset id"}
    { }
};

template <>
struct HasUnwrapper<Fred::PublicRequest::Unwrapper::InvalidateRequest>
{
    explicit HasUnwrapper()
        : api_invalidate_request{"request id", "invalidate id"},
          unwrapper_impl{Unwrapper::FakeRpc::make_unwrapper(api_invalidate_request)},
          unwrapper{unwrapper_impl}
    {
        BOOST_REQUIRE_NE(api_invalidate_request.public_request_id, api_invalidate_request.resolving_id);
    }
    FakeApi::InvalidateRequest api_invalidate_request;
    Unwrapper::FakeRpc::InvalidateRequest unwrapper_impl;
    const Fred::PublicRequest::Unwrapper::InvalidateRequest<Unwrapper::FakeRpc::InvalidateRequest>& unwrapper;
};
using HasInvalidateRequestUnwrapper = HasUnwrapper<Fred::PublicRequest::Unwrapper::InvalidateRequest>;

template <>
struct HasUnwrapper<Fred::PublicRequest::Unwrapper::ResolveRequest>
{
    explicit HasUnwrapper()
        : api_resolve_request{"request id", "resolve id"},
          unwrapper_impl{Unwrapper::FakeRpc::make_unwrapper(api_resolve_request)},
          unwrapper{unwrapper_impl}
    {
        BOOST_REQUIRE_NE(api_resolve_request.public_request_id, api_resolve_request.resolving_id);
    }
    FakeApi::ResolveRequest api_resolve_request;
    Unwrapper::FakeRpc::ResolveRequest unwrapper_impl;
    const Fred::PublicRequest::Unwrapper::ResolveRequest<Unwrapper::FakeRpc::ResolveRequest>& unwrapper;
};
using HasResolveRequestUnwrapper = HasUnwrapper<Fred::PublicRequest::Unwrapper::ResolveRequest>;

}//namespace {anonymous}

BOOST_AUTO_TEST_SUITE(TestUnwrapper)

BOOST_FIXTURE_TEST_CASE(get_request, HasGetRequestUnwrapper)
{
    BOOST_CHECK_EQUAL(unwrapper.public_request_id(), api_data.public_request_id);
}

BOOST_FIXTURE_TEST_CASE(contact_id, HasContactIdUnwrapper)
{
    unwrapper(Unwrapper::FakeRpc::CheckObjectId{api_data});
}

BOOST_FIXTURE_TEST_CASE(domain_id, HasDomainIdUnwrapper)
{
    unwrapper(Unwrapper::FakeRpc::CheckObjectId{api_data});
}

BOOST_FIXTURE_TEST_CASE(keyset_id, HasKeysetIdUnwrapper)
{
    unwrapper(Unwrapper::FakeRpc::CheckObjectId{api_data});
}

BOOST_FIXTURE_TEST_CASE(nsset_id, HasNssetIdUnwrapper)
{
    unwrapper(Unwrapper::FakeRpc::CheckObjectId{api_data});
}

BOOST_FIXTURE_TEST_CASE(contact_list_request, HasContactListRequestUnwrapper)
{
    unwrapper.registry_object_reference()(Unwrapper::FakeRpc::CheckObjectId{api_data.registry_object_reference});
}

BOOST_FIXTURE_TEST_CASE(domain_list_request, HasDomainListRequestUnwrapper)
{
    unwrapper.registry_object_reference()(Unwrapper::FakeRpc::CheckObjectId{api_data.registry_object_reference});
}

BOOST_FIXTURE_TEST_CASE(keyset_list_request, HasKeysetListRequestUnwrapper)
{
    unwrapper.registry_object_reference()(Unwrapper::FakeRpc::CheckObjectId{api_data.registry_object_reference});
}

BOOST_FIXTURE_TEST_CASE(nsset_list_request, HasNssetListRequestUnwrapper)
{
    unwrapper.registry_object_reference()(Unwrapper::FakeRpc::CheckObjectId{api_data.registry_object_reference});
}

BOOST_FIXTURE_TEST_CASE(invalidate_request, HasInvalidateRequestUnwrapper)
{
    BOOST_CHECK_EQUAL(*unwrapper.public_request_id(), api_invalidate_request.public_request_id);
    BOOST_CHECK_EQUAL(*unwrapper.resolving_id(), api_invalidate_request.resolving_id);
}

BOOST_FIXTURE_TEST_CASE(resolve_request, HasResolveRequestUnwrapper)
{
    BOOST_CHECK_EQUAL(*unwrapper.public_request_id(), api_resolve_request.public_request_id);
    BOOST_CHECK_EQUAL(*unwrapper.resolving_id(), api_resolve_request.resolving_id);
}

BOOST_AUTO_TEST_SUITE_END()//TestUnwrapper
