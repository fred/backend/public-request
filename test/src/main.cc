/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE TestPublicRequest

#include "test/setup/cfg.hh"
#include "test/setup/fixtures.hh"
#include "test/setup/has_fresh_database.hh"

#include "liblog/liblog.hh"

// dynamic library version
#include <boost/test/unit_test.hpp>

#include <utility>

namespace {

void database_setup()
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, "setup");
    auto tx = Test::make_rw_transaction();
    exec(tx, "WITH u1 AS "
             "("
                 "UPDATE enum_parameters SET val = '2' WHERE name = 'handle_registration_protection_period' "
                 "RETURNING id"
             ") "
             "UPDATE enum_parameters SET val = 'CZ' WHERE name = 'roid_suffix'");
    Test::CzZone{tx};
    Test::InitDomainNameCheckers{tx};
    Test::DbData::init(tx);
    commit(std::move(tx));
}

class GlobalFixture
{
public:
    GlobalFixture()
        : log_ctx{"GlobalFixture"},
          database_administrator_{
                []()
                {
                    return Test::Cfg::handle_command_line_args(database_setup);
                }()},
          create_db_template_{}
    {
        Test::HasFreshDatabase::set_restore_test_database_procedure(database_administrator_.get_restore_test_database_procedure());
        LIBLOG_INFO("tests start");
    }
    ~GlobalFixture()
    {
        try
        {
            LIBLOG_INFO("tests done");
            Test::HasFreshDatabase::clear_restore_test_database_procedure();
            Test::reset_connection();
        }
        catch (...) { }
    }
private:
    LibLog::SetContext log_ctx;
    Test::Cfg::DatabaseAdministrator database_administrator_;
    Test::CreateDbTemplate create_db_template_;
};

}//namespace {anonymous}

BOOST_GLOBAL_FIXTURE(GlobalFixture);
