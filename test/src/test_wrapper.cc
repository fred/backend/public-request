/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test/setup/fixtures.hh"

#include "src/wrapper/common_types.hh"
#include "src/wrapper/get_reply.hh"
#include "src/wrapper/get_types_reply.hh"
#include "src/wrapper/invalidate_reply.hh"
#include "src/wrapper/list_reply.hh"
#include "src/wrapper/public_request_type.hh"
#include "src/wrapper/resolve_reply.hh"

#include "libfred/public_request/public_request_data.hh"

#include <boost/test/unit_test.hpp>
#include <boost/uuid/string_generator.hpp>

#include <algorithm>
#include <chrono>
#include <exception>
#include <set>
#include <vector>
#include <utility>

namespace {

namespace FakeLib {

struct PublicRequest { };

struct PublicRequestType
{
    std::string name;
    bool is_resolvable;
    friend bool operator<(const PublicRequestType& lhs, const PublicRequestType& rhs) noexcept
    {
        return lhs.name < rhs.name;
    }
};

}//namespace {anonymous}::FakeLib

namespace FakeApi {

template <typename> constexpr const char* to_object_type_name() noexcept;

template <>
constexpr const char* to_object_type_name<Fred::PublicRequest::Lib::ContactId>() noexcept
{
    return "contact";
}

template <>
constexpr const char* to_object_type_name<Fred::PublicRequest::Lib::DomainId>() noexcept
{
    return "domain";
}

template <>
constexpr const char* to_object_type_name<Fred::PublicRequest::Lib::KeysetId>() noexcept
{
    return "keyset";
}

template <>
constexpr const char* to_object_type_name<Fred::PublicRequest::Lib::NssetId>() noexcept
{
    return "nsset";
}

template <typename RegistryObjectReference, typename ObjectData>
class ObjectDataCheck : public boost::static_visitor<void>
{
public:
    explicit ObjectDataCheck(const RegistryObjectReference& value)
        : uuid_{boost::uuids::string_generator{}(*value)}
    { }
    void operator()(const ObjectData& data) const
    {
        BOOST_CHECK_EQUAL(uuid_, data.uuid);
    }
    template <typename OtherObjectData>
    void operator()(const OtherObjectData&) const
    {
        BOOST_CHECK_MESSAGE(false, to_object_type_name<RegistryObjectReference>() << " expected");
    }
private:
    boost::uuids::uuid uuid_;
};

using ContactDataCheck = ObjectDataCheck<Fred::PublicRequest::Lib::ContactId,
                                         LibFred::PublicRequest::PublicRequestData::ContactDataLight>;

using DomainDataCheck = ObjectDataCheck<Fred::PublicRequest::Lib::DomainId,
                                        LibFred::PublicRequest::PublicRequestData::DomainDataLight>;

using KeysetDataCheck = ObjectDataCheck<Fred::PublicRequest::Lib::KeysetId,
                                        LibFred::PublicRequest::PublicRequestData::KeysetDataLight>;

using NssetDataCheck = ObjectDataCheck<Fred::PublicRequest::Lib::NssetId,
                                       LibFred::PublicRequest::PublicRequestData::NssetDataLight>;

class PublicRequest
{
public:
    explicit PublicRequest(const LibFred::PublicRequest::PublicRequestData& data)
        : data_{data},
          invoked_{}
    { }
    void set_public_request_id(const std::string& value)
    {
        BOOST_CHECK_EQUAL(data_.uuid, boost::uuids::string_generator{}(value));
        BOOST_CHECK(invoked_.public_request_id ^= true);
    }
    void set_numeric_id(std::uint64_t value)
    {
        BOOST_CHECK_EQUAL(data_.id, value);
        BOOST_CHECK(invoked_.numeric_id ^= true);
    }
    void set_registry_object_reference(Fred::PublicRequest::Lib::ContactId value)
    {
        boost::apply_visitor(ContactDataCheck{value}, data_.object_data);
        BOOST_CHECK(invoked_.contact_id ^= true);
    }
    void set_registry_object_reference(Fred::PublicRequest::Lib::DomainId value)
    {
        boost::apply_visitor(DomainDataCheck{value}, data_.object_data);
        BOOST_CHECK(invoked_.domain_id ^= true);
    }
    void set_registry_object_reference(Fred::PublicRequest::Lib::KeysetId value)
    {
        boost::apply_visitor(KeysetDataCheck{value}, data_.object_data);
        BOOST_CHECK(invoked_.keyset_id ^= true);
    }
    void set_registry_object_reference(Fred::PublicRequest::Lib::NssetId value)
    {
        boost::apply_visitor(NssetDataCheck{value}, data_.object_data);
        BOOST_CHECK(invoked_.nsset_id ^= true);
    }
    void set_type(std::string value)
    {
        BOOST_CHECK_EQUAL(data_.type, value);
        BOOST_CHECK(invoked_.type ^= true);
    }
    void set_registrar_id(std::string value)
    {
        BOOST_CHECK_EQUAL(data_.registrar_data->uuid, boost::uuids::string_generator{}(value));
        BOOST_CHECK(invoked_.registrar_id ^= true);
    }
    void set_response_email(std::string value)
    {
        BOOST_CHECK_EQUAL(data_.email_to_answer, value);
        BOOST_CHECK(invoked_.response_email ^= true);
    }
    void set_create_time(const LibFred::PublicRequest::PublicRequestData::TimePoint& value)
    {
        BOOST_CHECK_EQUAL(data_.create_time.time_since_epoch().count(), value.time_since_epoch().count());
        BOOST_CHECK(invoked_.create_time ^= true);
    }
    void set_creation_id(std::string value)
    {
        BOOST_CHECK_EQUAL(data_.create_request_id, value);
        BOOST_CHECK(invoked_.creation_id ^= true);
    }
    void set_resolve_time(const LibFred::PublicRequest::PublicRequestData::TimePoint& value)
    {
        BOOST_REQUIRE(data_.resolve_time != boost::none);
        BOOST_CHECK_EQUAL(data_.resolve_time->time_since_epoch().count(), value.time_since_epoch().count());
        BOOST_CHECK(invoked_.resolve_time ^= true);
    }
    void set_resolving_id(std::string value)
    {
        BOOST_CHECK_EQUAL(data_.resolve_request_id, value);
        BOOST_CHECK(invoked_.resolving_id ^= true);
    }
    void set_status(LibFred::PublicRequest::Status::Enum value)
    {
        BOOST_CHECK(data_.status == value);
        BOOST_CHECK(invoked_.status ^= true);
    }
    void set_on_status_action(LibFred::PublicRequest::OnStatusAction::Enum value)
    {
        BOOST_CHECK(data_.on_status_action == value);
        BOOST_CHECK(invoked_.on_status_action ^= true);
    }
    void check_all_contact_setters_used() const
    {
        this->all_setters_used();
        BOOST_CHECK(invoked_.contact_id);
        BOOST_CHECK(!invoked_.domain_id);
        BOOST_CHECK(!invoked_.keyset_id);
        BOOST_CHECK(!invoked_.nsset_id);
    }
    void check_all_domain_setters_used() const
    {
        this->all_setters_used();
        BOOST_CHECK(!invoked_.contact_id);
        BOOST_CHECK(invoked_.domain_id);
        BOOST_CHECK(!invoked_.keyset_id);
        BOOST_CHECK(!invoked_.nsset_id);
    }
    void check_all_keyset_setters_used() const
    {
        this->all_setters_used();
        BOOST_CHECK(!invoked_.contact_id);
        BOOST_CHECK(!invoked_.domain_id);
        BOOST_CHECK(invoked_.keyset_id);
        BOOST_CHECK(!invoked_.nsset_id);
    }
    void check_all_nsset_setters_used() const
    {
        this->all_setters_used();
        BOOST_CHECK(!invoked_.contact_id);
        BOOST_CHECK(!invoked_.domain_id);
        BOOST_CHECK(!invoked_.keyset_id);
        BOOST_CHECK(invoked_.nsset_id);
    }
private:
    void all_setters_used() const
    {
        BOOST_CHECK(invoked_.public_request_id);
        BOOST_CHECK(invoked_.numeric_id);
        BOOST_CHECK(invoked_.type);
        BOOST_CHECK(invoked_.registrar_id);
        BOOST_CHECK(invoked_.response_email);
        BOOST_CHECK(invoked_.create_time);
        BOOST_CHECK(invoked_.creation_id);
        BOOST_CHECK(invoked_.resolve_time);
        BOOST_CHECK(invoked_.resolving_id);
        BOOST_CHECK(invoked_.status);
        BOOST_CHECK(invoked_.on_status_action);
    }
    const LibFred::PublicRequest::PublicRequestData& data_;
    struct InvocationFlags
    {
        explicit InvocationFlags()
            : public_request_id{false},
              numeric_id{false},
              contact_id{false},
              domain_id{false},
              keyset_id{false},
              nsset_id{false},
              type{false},
              registrar_id{false},
              response_email{false},
              create_time{false},
              creation_id{false},
              resolve_time{false},
              resolving_id{false},
              status{false},
              on_status_action{false}
        { }
        bool public_request_id:1;
        bool numeric_id:1;
        bool contact_id:1;
        bool domain_id:1;
        bool keyset_id:1;
        bool nsset_id:1;
        bool type:1;
        bool registrar_id:1;
        bool response_email:1;
        bool create_time:1;
        bool creation_id:1;
        bool resolve_time:1;
        bool resolving_id:1;
        bool status:1;
        bool on_status_action:1;
    } invoked_;
};

class PublicRequestType
{
public:
    explicit PublicRequestType(const FakeLib::PublicRequestType& data)
        : data_{data},
          invoked_{}
    { }
    void set_name(std::string value)
    {
        BOOST_CHECK_EQUAL(value, data_.name);
        BOOST_CHECK(invoked_.name ^= true);
    }
    void set_is_resolvable(bool value)
    {
        BOOST_CHECK_EQUAL(value, data_.is_resolvable);
        BOOST_CHECK(invoked_.is_resolvable ^= true);
    }
    void check_all_setters_used() const
    {
        BOOST_CHECK(invoked_.name);
        BOOST_CHECK(invoked_.is_resolvable);
    }
private:
    const FakeLib::PublicRequestType& data_;
    struct InvocationFlags
    {
        explicit InvocationFlags()
            : name{false},
              is_resolvable{false}
        { }
        bool name:1;
        bool is_resolvable:1;
    } invoked_;
};

class GetTypesReply
{
public:
    explicit GetTypesReply(const std::set<FakeLib::PublicRequestType>& data)
        : data_{data},
          invoked_{}
    { }
    template <typename Size, typename IterB, typename IterE>
    void set_types(Size size, IterB&& begin, IterE&& end)
    {
        BOOST_CHECK_EQUAL(data_.size(), size);
        auto src_iter = std::begin(data_);
        std::for_each(std::forward<IterB>(begin), std::forward<IterE>(end), [&](auto&& type)
        {
            BOOST_CHECK_EQUAL(src_iter->name, type.name);
            BOOST_CHECK_EQUAL(src_iter->is_resolvable, type.is_resolvable);
            ++src_iter;
        });
        BOOST_CHECK(invoked_.types ^= true);
    }
    void check_all_setters_used() const
    {
        BOOST_CHECK(invoked_.types);
    }
private:
    const std::set<FakeLib::PublicRequestType>& data_;
    struct InvocationFlags
    {
        explicit InvocationFlags()
            : types{false}
        { }
        bool types:1;
    } invoked_;
};

class GetReply
{
public:
    explicit GetReply(const FakeLib::PublicRequest)
        : invoked_{}
    { }
    void set_data(FakeLib::PublicRequest)
    {
        BOOST_CHECK(invoked_.data ^= true);
        BOOST_CHECK(!invoked_.exception_public_request_does_not_exist);
    }
    void set_exception_public_request_does_not_exist()
    {
        BOOST_CHECK(!invoked_.data);
        BOOST_CHECK(invoked_.exception_public_request_does_not_exist ^= true);
    }
    void check_data_invoked() const
    {
        BOOST_CHECK(invoked_.data);
        BOOST_CHECK(!invoked_.exception_public_request_does_not_exist);
    }
    void check_exception_public_request_does_not_exist_invoked() const
    {
        BOOST_CHECK(!invoked_.data);
        BOOST_CHECK(invoked_.exception_public_request_does_not_exist);
    }
private:
    struct InvocationFlags
    {
        explicit InvocationFlags()
            : data{false},
              exception_public_request_does_not_exist{false}
        { }
        bool data:1;
        bool exception_public_request_does_not_exist:1;
    } invoked_;
};

class ListReply
{
public:
    explicit ListReply(const std::vector<FakeLib::PublicRequest>& data)
        : data_{data}
    { }
    template <typename Size, typename IterB, typename IterE>
    void set_public_requests(Size size, IterB&&, IterE&&)
    {
        BOOST_CHECK_EQUAL(size, data_.size());
        BOOST_CHECK(invoked_.public_requests ^= true);
    }
    void finish() &&
    {
        BOOST_CHECK(invoked_.finish ^= true);
    }
    void check_public_requests_invoked() const
    {
        BOOST_CHECK(invoked_.public_requests);
        BOOST_CHECK(!invoked_.finish);
    }
    void check_finish_invoked() const
    {
        BOOST_CHECK(invoked_.finish);
    }
private:
    const std::vector<FakeLib::PublicRequest>& data_;
    struct InvocationFlags
    {
        explicit InvocationFlags()
            : public_requests{false},
              finish{false}
        { }
        bool public_requests:1;
        bool finish:1;
    } invoked_;
};

class ResolveReply
{
public:
    explicit ResolveReply()
        : invoked_{}
    { }
    void set_exception_public_request_does_not_exist()
    {
        BOOST_CHECK(invoked_.exception_public_request_does_not_exist ^= true);
    }
    void set_exception_cannot_be_processed()
    {
        BOOST_CHECK(invoked_.exception_cannot_be_processed ^= true);
    }
    void check_exception_public_request_does_not_exist_invoked() const
    {
        BOOST_CHECK(invoked_.exception_public_request_does_not_exist);
        BOOST_CHECK(!invoked_.exception_cannot_be_processed);
    }
    void check_exception_cannot_be_processed() const
    {
        BOOST_CHECK(!invoked_.exception_public_request_does_not_exist);
        BOOST_CHECK(invoked_.exception_cannot_be_processed);
    }
private:
    struct InvocationFlags
    {
        explicit InvocationFlags()
            : exception_public_request_does_not_exist{false},
              exception_cannot_be_processed{false}
        { }
        bool exception_public_request_does_not_exist:1;
        bool exception_cannot_be_processed:1;
    } invoked_;
};

class InvalidateReply
{
public:
    explicit InvalidateReply()
        : invoked_{}
    { }
    void set_exception_public_request_does_not_exist()
    {
        BOOST_CHECK(invoked_.exception_public_request_does_not_exist ^= true);
    }
    void set_exception_cannot_be_processed()
    {
        BOOST_CHECK(invoked_.exception_cannot_be_processed ^= true);
    }
    void check_exception_public_request_does_not_exist_invoked() const
    {
        BOOST_CHECK(invoked_.exception_public_request_does_not_exist);
        BOOST_CHECK(!invoked_.exception_cannot_be_processed);
    }
    void check_exception_cannot_be_processed() const
    {
        BOOST_CHECK(!invoked_.exception_public_request_does_not_exist);
        BOOST_CHECK(invoked_.exception_cannot_be_processed);
    }
private:
    struct InvocationFlags
    {
        explicit InvocationFlags()
            : exception_public_request_does_not_exist{false},
              exception_cannot_be_processed{false}
        { }
        bool exception_public_request_does_not_exist:1;
        bool exception_cannot_be_processed:1;
    } invoked_;
};

}//namespace {anonymous}::FakeApi

namespace Wrapper {
namespace FakeRpc {

class PublicRequest : public Fred::PublicRequest::Wrapper::PublicRequest<PublicRequest>
{
public:
    void set_public_request_id(std::string value) const
    {
        data_.set_public_request_id(std::move(value));
    }
    void set_numeric_id(std::uint64_t value) const
    {
        data_.set_numeric_id(value);
    }
    void set_registry_object_reference(Fred::PublicRequest::Lib::ContactId value) const
    {
        data_.set_registry_object_reference(std::move(value));
    }
    void set_registry_object_reference(Fred::PublicRequest::Lib::DomainId value) const
    {
        data_.set_registry_object_reference(std::move(value));
    }
    void set_registry_object_reference(Fred::PublicRequest::Lib::KeysetId value) const
    {
        data_.set_registry_object_reference(std::move(value));
    }
    void set_registry_object_reference(Fred::PublicRequest::Lib::NssetId value) const
    {
        data_.set_registry_object_reference(std::move(value));
    }
    void set_type(std::string value) const
    {
        data_.set_type(std::move(value));
    }
    void set_registrar_id(std::string value) const
    {
        data_.set_registrar_id(std::move(value));
    }
    void set_response_email(std::string value) const
    {
        data_.set_response_email(std::move(value));
    }
    void set_create_time(const LibFred::PublicRequest::PublicRequestData::TimePoint& value) const
    {
        data_.set_create_time(value);
    }
    void set_creation_id(std::string value) const
    {
        data_.set_creation_id(std::move(value));
    }
    void set_resolve_time(const LibFred::PublicRequest::PublicRequestData::TimePoint& value) const
    {
        data_.set_resolve_time(value);
    }
    void set_resolving_id(std::string value) const
    {
        data_.set_resolving_id(std::move(value));
    }
    void set_status(LibFred::PublicRequest::Status::Enum value) const
    {
        data_.set_status(value);
    }
    void set_on_status_action(LibFred::PublicRequest::OnStatusAction::Enum value) const
    {
        data_.set_on_status_action(value);
    }
private:
    explicit PublicRequest(FakeApi::PublicRequest& data)
        : data_{data}
    { }
    FakeApi::PublicRequest& data_;
    friend PublicRequest make_wrapper(FakeApi::PublicRequest& data)
    {
        return PublicRequest{data};
    }
};
PublicRequest make_wrapper(FakeApi::PublicRequest& data);

class PublicRequestType : public Fred::PublicRequest::Wrapper::PublicRequestType<PublicRequestType>
{
public:
    void set_name(std::string value) const
    {
        data_.set_name(std::move(value));
    }
    void set_is_resolvable(bool value) const
    {
        data_.set_is_resolvable(value);
    }
private:
    explicit PublicRequestType(FakeApi::PublicRequestType& data)
        : data_{data}
    { }
    FakeApi::PublicRequestType& data_;
    friend PublicRequestType make_wrapper(FakeApi::PublicRequestType& data)
    {
        return PublicRequestType{data};
    }
};
PublicRequestType make_wrapper(FakeApi::PublicRequestType& data);

class GetTypesReply : public Fred::PublicRequest::Wrapper::GetTypesReply<GetTypesReply>
{
public:
    template <typename Size, typename IterB, typename IterE>
    void set_types(Size size, IterB&& begin, IterE&& end)
    {
        data_.set_types(size, std::forward<IterB>(begin), std::forward<IterE>(end));
    }
private:
    explicit GetTypesReply(FakeApi::GetTypesReply& data)
        : data_{data}
    { }
    FakeApi::GetTypesReply& data_;
    friend GetTypesReply make_wrapper(FakeApi::GetTypesReply& data)
    {
        return GetTypesReply{data};
    }
};
GetTypesReply make_wrapper(FakeApi::GetTypesReply& data);

class GetReply : public Fred::PublicRequest::Wrapper::GetReply<GetReply>
{
public:
    void set_data(FakeLib::PublicRequest data)
    {
        data_.set_data(std::move(data));
    }
    void set_exception_public_request_does_not_exist()
    {
        data_.set_exception_public_request_does_not_exist();
    }
private:
    explicit GetReply(FakeApi::GetReply& data)
        : data_{data}
    { }
    FakeApi::GetReply& data_;
    friend GetReply make_wrapper(FakeApi::GetReply& data)
    {
        return GetReply{data};
    }
};
GetReply make_wrapper(FakeApi::GetReply& data);

class ListReply : public Fred::PublicRequest::Wrapper::ListReply<ListReply>
{
public:
    using Iter = std::vector<FakeLib::PublicRequest>::const_iterator;
    void set_public_requests(std::size_t size, Iter begin, Iter end)
    {
        data_.set_public_requests(size, begin, end);
    }
    void finish() &&
    {
        std::move(data_).finish();
    }
private:
    explicit ListReply(FakeApi::ListReply& data)
        : data_{data}
    { }
    FakeApi::ListReply& data_;
    friend ListReply make_wrapper(FakeApi::ListReply& data)
    {
        return ListReply{data};
    }
};
ListReply make_wrapper(FakeApi::ListReply& data);

class ResolveReply : public Fred::PublicRequest::Wrapper::ResolveReply<ResolveReply>
{
public:
    void set_exception_public_request_does_not_exist()
    {
        data_.set_exception_public_request_does_not_exist();
    }
    void set_exception_cannot_be_processed()
    {
        data_.set_exception_cannot_be_processed();
    }
private:
    explicit ResolveReply(FakeApi::ResolveReply& data)
        : data_{data}
    { }
    FakeApi::ResolveReply& data_;
    friend ResolveReply make_wrapper(FakeApi::ResolveReply& data)
    {
        return ResolveReply{data};
    }
};
ResolveReply make_wrapper(FakeApi::ResolveReply& data);

class InvalidateReply : public Fred::PublicRequest::Wrapper::InvalidateReply<InvalidateReply>
{
public:
    void set_exception_public_request_does_not_exist()
    {
        data_.set_exception_public_request_does_not_exist();
    }
    void set_exception_cannot_be_processed()
    {
        data_.set_exception_cannot_be_processed();
    }
private:
    explicit InvalidateReply(FakeApi::InvalidateReply& data)
        : data_{data}
    { }
    FakeApi::InvalidateReply& data_;
    friend InvalidateReply make_wrapper(FakeApi::InvalidateReply& data)
    {
        return InvalidateReply{data};
    }
};
InvalidateReply make_wrapper(FakeApi::InvalidateReply& data);

}//namespace {anonymous}::Wrapper::FakeRpc
}//namespace {anonymous}::Wrapper

template <template <typename> typename> struct HasWrapper;

template <>
struct HasWrapper<Fred::PublicRequest::Wrapper::PublicRequest>
{
    template <typename ObjectData>
    explicit HasWrapper(ObjectData object_data)
        : src_data{
                42,
                boost::uuids::string_generator{}("9f7100ac-75a8-4c22-879d-a847c31f0000"),
                std::move(object_data),
                "public request type",
                LibFred::PublicRequest::PublicRequestData::RegistrarDataLight{
                        47,
                        boost::uuids::string_generator{}("9f7100ac-75a8-4c22-879d-a847c31f0005"),
                        "registrar handle"},
                std::chrono::system_clock::now() - std::chrono::seconds{10},
                "64",
                std::chrono::system_clock::now() - std::chrono::seconds{5},
                "123",
                LibFred::PublicRequest::Status::resolved,
                LibFred::PublicRequest::OnStatusAction::processed,
                "request@email.com"},
          dst_data{src_data},
          wrapper_impl{Wrapper::FakeRpc::make_wrapper(dst_data)},
          wrapper{wrapper_impl}
    { }
    LibFred::PublicRequest::PublicRequestData src_data;
    FakeApi::PublicRequest dst_data;
    Wrapper::FakeRpc::PublicRequest wrapper_impl;
    Fred::PublicRequest::Wrapper::PublicRequest<Wrapper::FakeRpc::PublicRequest>& wrapper;
};

struct HasContactPublicRequestWrapper : HasWrapper<Fred::PublicRequest::Wrapper::PublicRequest>
{
    explicit HasContactPublicRequestWrapper()
        : HasWrapper<Fred::PublicRequest::Wrapper::PublicRequest>{
                LibFred::PublicRequest::PublicRequestData::ContactDataLight{
                        43,
                        boost::uuids::string_generator{}("9f7100ac-75a8-4c22-879d-a847c31f0001"),
                        "contact handle"}}
    { }
};

struct HasDomainPublicRequestWrapper : HasWrapper<Fred::PublicRequest::Wrapper::PublicRequest>
{
    explicit HasDomainPublicRequestWrapper()
        : HasWrapper<Fred::PublicRequest::Wrapper::PublicRequest>{
                LibFred::PublicRequest::PublicRequestData::DomainDataLight{
                        44,
                        boost::uuids::string_generator{}("9f7100ac-75a8-4c22-879d-a847c31f0002"),
                        "domain handle"}}
    { }
};

struct HasKeysetPublicRequestWrapper : HasWrapper<Fred::PublicRequest::Wrapper::PublicRequest>
{
    explicit HasKeysetPublicRequestWrapper()
        : HasWrapper<Fred::PublicRequest::Wrapper::PublicRequest>{
                LibFred::PublicRequest::PublicRequestData::KeysetDataLight{
                        45,
                        boost::uuids::string_generator{}("9f7100ac-75a8-4c22-879d-a847c31f0003"),
                        "keyset handle"}}
    { }
};

struct HasNssetPublicRequestWrapper : HasWrapper<Fred::PublicRequest::Wrapper::PublicRequest>
{
    explicit HasNssetPublicRequestWrapper()
        : HasWrapper<Fred::PublicRequest::Wrapper::PublicRequest>{
                LibFred::PublicRequest::PublicRequestData::NssetDataLight{
                        46,
                        boost::uuids::string_generator{}("9f7100ac-75a8-4c22-879d-a847c31f0004"),
                        "nsset handle"}}
    { }
};

template <>
struct HasWrapper<Fred::PublicRequest::Wrapper::PublicRequestType>
{
    explicit HasWrapper(FakeLib::PublicRequestType data)
        : src_data{std::move(data)},
          dst_data{src_data},
          wrapper_impl{Wrapper::FakeRpc::make_wrapper(dst_data)},
          wrapper{wrapper_impl}
    { }
    FakeLib::PublicRequestType src_data;
    FakeApi::PublicRequestType dst_data;
    Wrapper::FakeRpc::PublicRequestType wrapper_impl;
    Fred::PublicRequest::Wrapper::PublicRequestType<Wrapper::FakeRpc::PublicRequestType>& wrapper;
};

struct HasResolvablePublicRequestTypeWrapper : HasWrapper<Fred::PublicRequest::Wrapper::PublicRequestType>
{
    explicit HasResolvablePublicRequestTypeWrapper()
        : HasWrapper<Fred::PublicRequest::Wrapper::PublicRequestType>{{"resolvable type", true}}
    { }
};

struct HasNonResolvablePublicRequestTypeWrapper : HasWrapper<Fred::PublicRequest::Wrapper::PublicRequestType>
{
    explicit HasNonResolvablePublicRequestTypeWrapper()
        : HasWrapper<Fred::PublicRequest::Wrapper::PublicRequestType>{{"non-resolvable type", false}}
    { }
};

template <>
struct HasWrapper<Fred::PublicRequest::Wrapper::GetTypesReply>
{
    explicit HasWrapper(std::set<FakeLib::PublicRequestType> types)
        : src_data{std::move(types)},
          dst_data{src_data},
          wrapper_impl{Wrapper::FakeRpc::make_wrapper(dst_data)},
          wrapper{wrapper_impl}
    { }
    std::set<FakeLib::PublicRequestType> src_data;
    FakeApi::GetTypesReply dst_data;
    Wrapper::FakeRpc::GetTypesReply wrapper_impl;
    Fred::PublicRequest::Wrapper::GetTypesReply<Wrapper::FakeRpc::GetTypesReply>& wrapper;
};

struct HasEmptyGetTypesReplyWrapper : HasWrapper<Fred::PublicRequest::Wrapper::GetTypesReply>
{
    explicit HasEmptyGetTypesReplyWrapper()
        : HasWrapper<Fred::PublicRequest::Wrapper::GetTypesReply>{{}}
    { }
};

struct HasGetTypesReplyWrapper : HasWrapper<Fred::PublicRequest::Wrapper::GetTypesReply>
{
    explicit HasGetTypesReplyWrapper()
        : HasWrapper<Fred::PublicRequest::Wrapper::GetTypesReply>{{
                {"resolvable type", true},
                {"non-resolvable type", false}}}
    { }
};

template <>
struct HasWrapper<Fred::PublicRequest::Wrapper::GetReply>
{
    explicit HasWrapper()
        : src_data{},
          dst_data{src_data},
          wrapper_impl{Wrapper::FakeRpc::make_wrapper(dst_data)},
          wrapper{wrapper_impl}
    { }
    explicit HasWrapper(FakeLib::PublicRequest public_request)
        : src_data{std::move(public_request)},
          dst_data{src_data},
          wrapper_impl{Wrapper::FakeRpc::make_wrapper(dst_data)},
          wrapper{wrapper_impl}
    { }
    FakeLib::PublicRequest src_data;
    FakeApi::GetReply dst_data;
    Wrapper::FakeRpc::GetReply wrapper_impl;
    Fred::PublicRequest::Wrapper::GetReply<Wrapper::FakeRpc::GetReply>& wrapper;
};

struct HasGetReplyWrapper : HasWrapper<Fred::PublicRequest::Wrapper::GetReply>
{
    explicit HasGetReplyWrapper()
        : HasWrapper<Fred::PublicRequest::Wrapper::GetReply>{{}}
    { }
};

struct HasExceptionGetReply : HasWrapper<Fred::PublicRequest::Wrapper::GetReply>
{
    explicit HasExceptionGetReply()
        : HasWrapper<Fred::PublicRequest::Wrapper::GetReply>{}
    { }
};

template <>
struct HasWrapper<Fred::PublicRequest::Wrapper::ListReply>
{
    explicit HasWrapper(std::vector<FakeLib::PublicRequest> public_requests)
        : src_data{std::move(public_requests)},
          dst_data{src_data},
          wrapper_impl{Wrapper::FakeRpc::make_wrapper(dst_data)},
          wrapper{wrapper_impl}
    { }
    std::vector<FakeLib::PublicRequest> src_data;
    FakeApi::ListReply dst_data;
    Wrapper::FakeRpc::ListReply wrapper_impl;
    Fred::PublicRequest::Wrapper::ListReply<Wrapper::FakeRpc::ListReply>& wrapper;
};

struct HasListReplyWrapper : HasWrapper<Fred::PublicRequest::Wrapper::ListReply>
{
    explicit HasListReplyWrapper()
        : HasWrapper<Fred::PublicRequest::Wrapper::ListReply>{{{}, {}, {}}}
    { }
};

struct HasEmptyListReplyWrapper : HasWrapper<Fred::PublicRequest::Wrapper::ListReply>
{
    explicit HasEmptyListReplyWrapper()
        : HasWrapper<Fred::PublicRequest::Wrapper::ListReply>{{}}
    { }
};

template <>
struct HasWrapper<Fred::PublicRequest::Wrapper::ResolveReply>
{
    explicit HasWrapper()
        : dst_data{},
          wrapper_impl{Wrapper::FakeRpc::make_wrapper(dst_data)},
          wrapper{wrapper_impl}
    { }
    FakeApi::ResolveReply dst_data;
    Wrapper::FakeRpc::ResolveReply wrapper_impl;
    Fred::PublicRequest::Wrapper::ResolveReply<Wrapper::FakeRpc::ResolveReply>& wrapper;
};
using HasResolveReplyWrapper = HasWrapper<Fred::PublicRequest::Wrapper::ResolveReply>;

template <>
struct HasWrapper<Fred::PublicRequest::Wrapper::InvalidateReply>
{
    explicit HasWrapper()
        : dst_data{},
          wrapper_impl{Wrapper::FakeRpc::make_wrapper(dst_data)},
          wrapper{wrapper_impl}
    { }
    FakeApi::InvalidateReply dst_data;
    Wrapper::FakeRpc::InvalidateReply wrapper_impl;
    Fred::PublicRequest::Wrapper::InvalidateReply<Wrapper::FakeRpc::InvalidateReply>& wrapper;
};
using HasInvalidateReplyWrapper = HasWrapper<Fred::PublicRequest::Wrapper::InvalidateReply>;

}//namespace {anonymous}

BOOST_AUTO_TEST_SUITE(TestWrapper)

BOOST_FIXTURE_TEST_CASE(contact_public_request, HasContactPublicRequestWrapper)
{
    wrapper(src_data);
    dst_data.check_all_contact_setters_used();
}

BOOST_FIXTURE_TEST_CASE(domain_public_request, HasDomainPublicRequestWrapper)
{
    wrapper(src_data);
    dst_data.check_all_domain_setters_used();
}

BOOST_FIXTURE_TEST_CASE(keyset_public_request, HasKeysetPublicRequestWrapper)
{
    wrapper(src_data);
    dst_data.check_all_keyset_setters_used();
}

BOOST_FIXTURE_TEST_CASE(nsset_public_request, HasNssetPublicRequestWrapper)
{
    wrapper(src_data);
    dst_data.check_all_nsset_setters_used();
}

BOOST_FIXTURE_TEST_CASE(non_resolvable_public_request_type, HasNonResolvablePublicRequestTypeWrapper)
{
    wrapper.set_name(src_data.name);
    wrapper.set_is_resolvable(src_data.is_resolvable);
    dst_data.check_all_setters_used();
}

BOOST_FIXTURE_TEST_CASE(resolvable_public_request_type, HasResolvablePublicRequestTypeWrapper)
{
    wrapper.set_name(src_data.name);
    wrapper.set_is_resolvable(src_data.is_resolvable);
    dst_data.check_all_setters_used();
}

BOOST_FIXTURE_TEST_CASE(empty_get_types_reply, HasEmptyGetTypesReplyWrapper)
{
    wrapper.set_types(src_data.size(), begin(src_data), end(src_data));
    dst_data.check_all_setters_used();
}

BOOST_FIXTURE_TEST_CASE(get_types_reply, HasGetTypesReplyWrapper)
{
    wrapper.set_types(src_data.size(), begin(src_data), end(src_data));
    dst_data.check_all_setters_used();
}

BOOST_FIXTURE_TEST_CASE(get_reply, HasGetReplyWrapper)
{
    wrapper.set_data(src_data);
    dst_data.check_data_invoked();
}

BOOST_FIXTURE_TEST_CASE(get_reply_exception, HasGetReplyWrapper)
{
    wrapper.set_exception_public_request_does_not_exist();
    dst_data.check_exception_public_request_does_not_exist_invoked();
}

BOOST_FIXTURE_TEST_CASE(list_reply, HasListReplyWrapper)
{
    wrapper.set_public_requests(src_data.size(), begin(src_data), end(src_data));
    dst_data.check_public_requests_invoked();
    std::move(wrapper).finish();
    dst_data.check_finish_invoked();
}

BOOST_FIXTURE_TEST_CASE(empty_list_reply, HasEmptyListReplyWrapper)
{
    wrapper.set_public_requests(src_data.size(), begin(src_data), end(src_data));
    dst_data.check_public_requests_invoked();
    std::move(wrapper).finish();
    dst_data.check_finish_invoked();
}

BOOST_FIXTURE_TEST_CASE(resolve_reply_exception_1, HasResolveReplyWrapper)
{
    wrapper.set_exception_public_request_does_not_exist();
    dst_data.check_exception_public_request_does_not_exist_invoked();
}

BOOST_FIXTURE_TEST_CASE(resolve_reply_exception_2, HasResolveReplyWrapper)
{
    wrapper.set_exception_cannot_be_processed();
    dst_data.check_exception_cannot_be_processed();
}

BOOST_FIXTURE_TEST_CASE(invalidate_reply_exception_1, HasInvalidateReplyWrapper)
{
    wrapper.set_exception_public_request_does_not_exist();
    dst_data.check_exception_public_request_does_not_exist_invoked();
}

BOOST_FIXTURE_TEST_CASE(invalidate_reply_exception_2, HasInvalidateReplyWrapper)
{
    wrapper.set_exception_cannot_be_processed();
    dst_data.check_exception_cannot_be_processed();
}

BOOST_AUTO_TEST_SUITE_END()//TestWrapper
