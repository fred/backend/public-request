/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef OPERATION_CONTEXT_HH_25CE49C9E55E525599EC572426972FEE//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define OPERATION_CONTEXT_HH_25CE49C9E55E525599EC572426972FEE

#include "libfred/opcontext.hh"

#include "libpg/pg_ro_transaction.hh"
#include "libpg/pg_rw_transaction.hh"

#include <memory>
#include <utility>

namespace Test {

template <typename Transaction>
class OperationContext
{
public:
    OperationContext();
    explicit OperationContext(Transaction tx);
    ~OperationContext();
    operator const LibFred::OperationContext&() const;
    operator const Transaction&() const;
    operator const LibPg::PgTransaction&() const;
    operator const LibPg::PgConnection&() const;
private:
    struct TransactionContext
    {
        explicit TransactionContext(Transaction tx);
        Transaction tx;
        LibFred::OperationContext ctx;
    };
    std::unique_ptr<TransactionContext> ptr_;
    template <typename T>
    friend void rollback(OperationContext<T>&&);
    template <typename T>
    friend LibPg::PgConnection commit(OperationContext<T>&&);
};

template <typename T>
void rollback(OperationContext<T>&&);
template <typename T>
LibPg::PgConnection commit(OperationContext<T>&&);

using RoOperationContext = OperationContext<LibPg::PgRoTransaction>;
using RwOperationContext = OperationContext<LibPg::PgRwTransaction>;

extern template class OperationContext<LibPg::PgRoTransaction>;
extern template class OperationContext<LibPg::PgRwTransaction>;

extern template LibPg::PgConnection commit(RoOperationContext&&);
extern template LibPg::PgConnection commit(RwOperationContext&&);

extern template void rollback(RoOperationContext&&);
extern template void rollback(RwOperationContext&&);

void reset_connection();
bool is_rollbacked();

}//namespace Test

#endif//OPERATION_CONTEXT_HH_25CE49C9E55E525599EC572426972FEE
