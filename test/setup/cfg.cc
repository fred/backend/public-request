/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "test/setup/cfg.hh"
#include "test/setup/arguments.hh"
#include "test/setup/fixtures.hh"
#include "test/setup/run_in_background.hh"
#include "test/setup/test_tree_list.hh"

#include "src/util/install_pg_log_layer.hh"

#include "config.h"

#include "liblog/liblog.hh"
#include "liblog/log_config.hh"
#include "liblog/log.hh"

#include "libpg/pg_connection.hh"

#include <boost/program_options.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/test/unit_test_parameters.hpp>

#include <grpc/support/log.h>

#include <cstdlib>
#include <syslog.h>
#include <unistd.h>

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <random>
#include <sstream>
#include <thread>
#include <unordered_map>
#include <utility>
#include <vector>

#ifndef DEFAULT_CONFIG_FILE
#define DEFAULT_CONFIG_FILE "test-public-request.conf"
#endif

namespace Test {
namespace Cfg {

namespace {

template <typename E>
[[noreturn]] void raise(std::string msg)
{
    struct ExceptionImpl : E
    {
        explicit ExceptionImpl(std::string msg) : msg{std::move(msg)} { }
        const char* what() const noexcept override { return msg.c_str(); }
        std::string msg;
    };
    throw ExceptionImpl{std::move(msg)};
}

//critical/error/warning/info/debug/trace
decltype(auto) make_severity(const std::string& str)
{
    static const std::unordered_map<std::string, LibLog::Level> str2level =
    {
        { "critical", LibLog::Level::critical },
        { "error", LibLog::Level::error },
        { "warning", LibLog::Level::warning },
        { "info", LibLog::Level::info },
        { "debug", LibLog::Level::debug },
        { "trace", LibLog::Level::trace }
    };
    const auto level_itr = str2level.find(str);
    if (level_itr != str2level.end())
    {
        return level_itr->second;
    }
    raise<Exception>("\"" + str + "\" not a valid severity");
}

constexpr int fake_argc = -1;
constexpr const char* const fake_argv[] = { nullptr };

class ScopeExit
{
public:
    ScopeExit(std::function<void()> on_exit)
        : on_exit_{std::move(on_exit)}
    { }
    ~ScopeExit()
    {
        try
        {
            on_exit_();
        }
        catch (...) { }
    }
private:
    std::function<void()> on_exit_;
};

static constexpr unsigned terminal_width = 120;

decltype(auto) make_generic_options(Options::Generic& generic)
{
    boost::program_options::options_description generic_options{"Generic options", terminal_width};
    const auto set_config_file_name = [&](std::string file_name)
    {
        generic.config_file_name = std::move(file_name);
    };
    const auto set_parallel = [&](int value)
    {
        if (value < 1)
        {
            class ParallelOutOfRange : public boost::program_options::invalid_option_value
            {
            public:
                explicit ParallelOutOfRange(int value)
                    : boost::program_options::invalid_option_value{std::to_string(value)},
                      msg_{std::string{this->boost::program_options::invalid_option_value::what()} +
                           ", must be greater then zero"}
                {
                    this->set_option_name("parallel");
                    this->set_prefix(1);
                }
                const char* what() const noexcept override { return msg_.c_str(); }
            private:
                std::string msg_;
            };
            throw ParallelOutOfRange{value};
        }
        generic.parallel = value;
    };
    const auto set_setup = [&](bool value) { generic.invoke_database_setup = value; };
    generic_options.add_options()
        ("help,h", "display help message")
        ("version,v", "display server version information")
        ("parallel,p", boost::program_options::value<int>()->default_value(1)->notifier(set_parallel), "run tests in parallel")
        ("setup,s", boost::program_options::value<bool>()->default_value(true)->notifier(set_setup), "setup database on start")
        ("config,c",
#ifdef DEFAULT_CONFIG_FILE
         boost::program_options::value<std::string>()->default_value(DEFAULT_CONFIG_FILE)->notifier(set_config_file_name),
#else
         boost::program_options::value<std::string>()->notifier(set_config_file_name),
#endif
         "name of the configuration file.");
    return generic_options;
}

decltype(auto) make_database_options(Options::Database& dsn)
{
    boost::program_options::options_description database_options{"Database regular user access options", terminal_width};
    const auto set_host = [&](const std::string& host) { dsn.host = host; };
    const auto set_host_addr = [&](const std::string& ip_address) { dsn.host_addr = boost::asio::ip::address::from_string(ip_address); };
    const auto set_port = [&](::in_port_t port) { dsn.port = port; };
    const auto set_user = [&](const std::string& user) { dsn.user = user; };
    const auto set_dbname = [&](const std::string& dbname) { dsn.dbname = dbname; };
    const auto set_password = [&](const std::string& password) { dsn.password = password; };
    const auto set_connect_timeout = [&](std::chrono::seconds::rep seconds) { dsn.connect_timeout = std::chrono::seconds{seconds}; };
    const auto set_application_name = [&](const std::string& name) { dsn.application_name = name; };
    database_options.add_options()
        ("database.host", boost::program_options::value<std::string>()->notifier(set_host), "Name of host to connect to.")
        ("database.host_addr", boost::program_options::value<std::string>()->notifier(set_host_addr), "Numeric IP address of host to connect to.")
        ("database.port", boost::program_options::value<::in_port_t>()->notifier(set_port), "Port number to connect to at the server host.")
        ("database.user", boost::program_options::value<std::string>()->notifier(set_user), "PostgreSQL user name to connect as.")
        ("database.dbname", boost::program_options::value<std::string>()->notifier(set_dbname), "The database name. Defaults to be the same as the user name.")
        ("database.password", boost::program_options::value<std::string>()->notifier(set_password), "Password to be used if the server demands password authentication.")
        ("database.connect_timeout",
         boost::program_options::value<std::chrono::seconds::rep>()->notifier(set_connect_timeout),
         "Maximum wait for connection, in seconds. Zero or not specified means wait indefinitely. "
         "It is not recommended to use a timeout of less than 2 seconds.")
        ("database.application_name",
         boost::program_options::value<std::string>()->notifier(set_application_name),
         "Specifies a value for the application_name configuration parameter.");
    return database_options;
}

decltype(auto) make_admin_database_options(Options::AdminDatabase& dsn)
{
    boost::program_options::options_description database_options{"Database privileged user access options", terminal_width};
    const auto set_host = [&](const std::string& host) { dsn.host = host; };
    const auto set_host_addr = [&](const std::string& ip_address) { dsn.host_addr = boost::asio::ip::address::from_string(ip_address); };
    const auto set_port = [&](::in_port_t port) { dsn.port = port; };
    const auto set_user = [&](const std::string& user) { dsn.user = user; };
    const auto set_dbname = [&](const std::string& dbname) { dsn.dbname = dbname; };
    const auto set_password = [&](const std::string& password) { dsn.password = password; };
    const auto set_connect_timeout = [&](std::chrono::seconds::rep seconds) { dsn.connect_timeout = std::chrono::seconds{seconds}; };
    const auto set_application_name = [&](const std::string& name) { dsn.application_name = name; };
    database_options.add_options()
        ("admin_database.host", boost::program_options::value<std::string>()->notifier(set_host), "Name of host to connect to.")
        ("admin_database.host_addr", boost::program_options::value<std::string>()->notifier(set_host_addr), "Numeric IP address of host to connect to.")
        ("admin_database.port", boost::program_options::value<::in_port_t>()->notifier(set_port), "Port number to connect to at the server host.")
        ("admin_database.user", boost::program_options::value<std::string>()->notifier(set_user), "PostgreSQL user name to connect as.")
        ("admin_database.dbname", boost::program_options::value<std::string>()->notifier(set_dbname), "The database name. Defaults to be the same as the user name.")
        ("admin_database.password", boost::program_options::value<std::string>()->notifier(set_password), "Password to be used if the server demands password authentication.")
        ("admin_database.connect_timeout",
         boost::program_options::value<std::chrono::seconds::rep>()->notifier(set_connect_timeout),
         "Maximum wait for connection, in seconds. Zero or not specified means wait indefinitely. "
         "It is not recommended to use a timeout of less than 2 seconds.")
        ("admin_database.application_name",
         boost::program_options::value<std::string>()->notifier(set_application_name),
         "Specifies a value for the application_name configuration parameter.");
    return database_options;
}

struct CommonLogOptions
{
    enum class Device
    {
        console,
        file,
        syslog
    };
    static decltype(auto) make_device(const std::string& device)
    {
        static const std::unordered_map<std::string, Device> str2device =
            {
                { "console", Device::console },
                { "file", Device::file },
                { "syslog", Device::syslog }
            };
        const auto device_itr = str2device.find(device);
        if (device_itr != str2device.end())
        {
            return device_itr->second;
        }
        raise<Exception>("\"" + device + "\" is not valid device");
    }
    std::set<Device> devices;
    boost::optional<LibLog::Level> min_severity;
    boost::optional<bool> log_grpc_library_events;
    void set_devices(const std::vector<std::string>& str_devices)
    {
        std::for_each(
            str_devices.begin(),
            str_devices.end(),
            [&](const std::string& str_device)
            {
                devices.insert(make_device(str_device));
            });
    }
    void set_min_severity(const std::string& severity)
    {
        if (min_severity != boost::none)
        {
            raise<Exception>("severity can be defined at most once");
        }
        min_severity = make_severity(severity);
    }
    void set_log_grpc_library_events(bool value)
    {
        log_grpc_library_events = value;
    }
};

decltype(auto) make_log_options(CommonLogOptions& common_log_options)
{
    boost::program_options::options_description options_description{"Logging options", terminal_width};
    const auto set_devices = [&](const std::vector<std::string>& devices) { common_log_options.set_devices(devices); };
    const auto set_min_severity = [&](const std::string& severity) { common_log_options.set_min_severity(severity); };
    const auto set_log_grpc_library_events = [&](bool to_log) { common_log_options.set_log_grpc_library_events(to_log); };
    options_description.add_options()
        ("log.device", boost::program_options::value<std::vector<std::string>>()->multitoken()->notifier(set_devices), "where to log (console/file/syslog)")
        ("log.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace")
        ("log.grpc_library",
         boost::program_options::value<bool>()->zero_tokens()->notifier(set_log_grpc_library_events),
         "to log gRPC library events");
    return options_description;
}

//stderr/stdout
decltype(auto) make_output_stream(const std::string& str)
{
    static const std::unordered_map<std::string, LibLog::Sink::ConsoleSinkConfig::OutputStream> str2output_stream =
        {
            { "stderr", LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr },
            { "stdout", LibLog::Sink::ConsoleSinkConfig::OutputStream::stdout }
        };
    const auto output_stream_itr = str2output_stream.find(str);
    if (output_stream_itr != str2output_stream.end())
    {
        return output_stream_itr->second;
    }
    raise<Exception>("\"" + str + "\" not a valid output stream");
}

//always/auto/never
decltype(auto) make_color_mode(const std::string& str)
{
    static const std::unordered_map<std::string, LibLog::ColorMode> str2color_mode =
        {
            { "always", LibLog::ColorMode::always },
            { "auto", LibLog::ColorMode::automatic },
            { "never", LibLog::ColorMode::never }
        };
    const auto color_mode_itr = str2color_mode.find(str);
    if (color_mode_itr != str2color_mode.end())
    {
        return color_mode_itr->second;
    }
    raise<Exception>("\"" + str + "\" not a valid color mode");
}

struct ConsoleLogOptions
{
    boost::optional<LibLog::Level> min_severity;
    boost::optional<LibLog::Sink::ConsoleSinkConfig::OutputStream> output_stream;
    boost::optional<LibLog::ColorMode> color_mode;
    void set_min_severity(const std::string& value)
    {
        if (min_severity != boost::none)
        {
            raise<Exception>("severity can be defined at most once");
        }
        min_severity = make_severity(value);
    }
    void set_output_stream(const std::string& value)
    {
        if (output_stream != boost::none)
        {
            raise<Exception>("severity can be defined at most once");
        }
        output_stream = make_output_stream(value);
    }
    void set_color_mode(const std::string& value)
    {
        if (color_mode != boost::none)
        {
            raise<Exception>("color mode can be defined at most once");
        }
        color_mode = make_color_mode(value);
    }
};

decltype(auto) make_log_console_options(ConsoleLogOptions& console_log_options)
{
    boost::program_options::options_description options_description{"Logging on console options", terminal_width};
    const auto set_min_severity = [&](const std::string& severity) { console_log_options.set_min_severity(severity); };
    const auto set_output_stream = [&](const std::string& output_stream) { console_log_options.set_output_stream(output_stream); };
    const auto set_color_mode = [&](const std::string& color_mode) { console_log_options.set_color_mode(color_mode); };
    options_description.add_options()
        ("log.console.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace")
        ("log.console.output_stream",
         boost::program_options::value<std::string>()->notifier(set_output_stream),
         "where to log (stderr/stdout), default is stderr")
        ("log.console.color_mode",
         boost::program_options::value<std::string>()->notifier(set_color_mode),
         "how to colorize output (always/auto/never), default is never");
    return options_description;
}

struct FileLogOptions
{
    boost::optional<std::string> file_name;
    boost::optional<LibLog::Level> min_severity;
    void set_file_name(const std::string& name)
    {
        if (file_name != boost::none)
        {
            raise<Exception>("file name can be defined at most once");
        }
        file_name = name;
    }
    void set_min_severity(const std::string& severity)
    {
        if (min_severity != boost::none)
        {
            raise<Exception>("severity can be defined at most once");
        }
        min_severity = make_severity(severity);
    }
};

decltype(auto) make_log_file_options(FileLogOptions& file_log_options)
{
    boost::program_options::options_description options_description{"Logging into file options", terminal_width};
    const auto set_file_name = [&](const std::string& file_name) { file_log_options.set_file_name(file_name); };
    const auto set_min_severity = [&](const std::string& severity) { file_log_options.set_min_severity(severity); };
    options_description.add_options()
        ("log.file.file_name", boost::program_options::value<std::string>()->notifier(set_file_name), "what file to log into")
        ("log.file.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace");
    return options_description;
}

struct SyslogLogOptions
{
    boost::optional<std::string> ident;
    boost::optional<int> facility_local_offset;
    boost::optional<LibLog::Level> min_severity;
    void set_ident(const std::string& value)
    {
        if (ident != boost::none)
        {
            raise<Exception>("ident can be defined at most once");
        }
        ident = value;
    }
    void set_facility(int value)
    {
        if ((value < 0) || (7 < value))
        {
            raise<Exception>("facility out of range [0, 7]");
        }
        if (facility_local_offset != boost::none)
        {
            raise<Exception>("facility can be defined at most once");
        }
        facility_local_offset = value;
    }
    void set_min_severity(const std::string& severity)
    {
        if (min_severity != boost::none)
        {
            raise<Exception>("severity can be defined at most once");
        }
        min_severity = make_severity(severity);
    }
};

decltype(auto) make_log_syslog_options(SyslogLogOptions& syslog_log_options)
{
    boost::program_options::options_description options_description{"Logging into syslog options", terminal_width};
    const auto set_ident = [&](const std::string& ident) { syslog_log_options.set_ident(ident); };
    const auto set_facility = [&](int facility) { syslog_log_options.set_facility(facility); };
    const auto set_min_severity = [&](const std::string& severity) { syslog_log_options.set_min_severity(severity); };
    options_description.add_options()
        ("log.syslog.ident", boost::program_options::value<std::string>()->notifier(set_ident), "what ident to log with (default is empty string)")
        ("log.syslog.facility",
         boost::program_options::value<int>()->notifier(set_facility),
         "what LOG_LOCALx facility to log with (x in range 0..7, default means facility LOG_USER)")
        ("log.syslog.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace");
    return options_description;
}

void collect_log_options(
    const CommonLogOptions& common_log_options,
    const ConsoleLogOptions& console_log_options,
    const FileLogOptions& file_log_options,
    const SyslogLogOptions& syslog_log_options,
    Options::Log& log_options)
{
    log_options.log_grpc_library_events = (common_log_options.log_grpc_library_events != boost::none) && *common_log_options.log_grpc_library_events;

    static constexpr auto default_log_min_severity = LibLog::Level::critical;
    const auto common_log_min_severity = common_log_options.min_severity != boost::none ? *common_log_options.min_severity
                                                                                        : default_log_min_severity;
    std::for_each(
        common_log_options.devices.begin(),
        common_log_options.devices.end(),
        [&](CommonLogOptions::Device device)
        {
            switch (device)
            {
            case CommonLogOptions::Device::console:
            {
                Options::Log::Console console;
                console.min_severity = console_log_options.min_severity != boost::none ? *console_log_options.min_severity
                                                                                       : common_log_min_severity;
                static constexpr auto default_output_stream = LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr;
                console.output_stream = console_log_options.output_stream != boost::none ? *console_log_options.output_stream
                                                                                         : default_output_stream;
                static constexpr auto default_color_mode = LibLog::ColorMode::never;
                console.color_mode = console_log_options.color_mode != boost::none ? *console_log_options.color_mode
                                                                                   : default_color_mode;
                log_options.console = console;
                return;
            }
            case CommonLogOptions::Device::file:
            {
                if (file_log_options.file_name == boost::none)
                {
                    raise<MissingOption>("missing option: 'log.file.file_name'");
                }
                Options::Log::File log_file;
                log_file.file_name = *file_log_options.file_name;
                log_file.min_severity = file_log_options.min_severity != boost::none ? *file_log_options.min_severity
                                                                                     : common_log_min_severity;
                log_options.file = log_file;
                return;
            }
            case CommonLogOptions::Device::syslog:
            {
                Options::Log::Syslog syslog;
                static const std::string default_ident = "";
                syslog.ident = syslog_log_options.ident != boost::none ? *syslog_log_options.ident
                                                                       : default_ident;
                static constexpr int default_facility = LOG_USER;
                static const auto make_facility = [](int offset)
                {
                    switch (offset)
                    {
                        case 0: return LOG_LOCAL0;
                        case 1: return LOG_LOCAL1;
                        case 2: return LOG_LOCAL2;
                        case 3: return LOG_LOCAL3;
                        case 4: return LOG_LOCAL4;
                        case 5: return LOG_LOCAL5;
                        case 6: return LOG_LOCAL6;
                        case 7: return LOG_LOCAL7;
                    }
                    return default_facility;
                };
                syslog.facility = syslog_log_options.facility_local_offset != boost::none ? make_facility(*syslog_log_options.facility_local_offset)
                                                                                          : default_facility;
                syslog.min_severity = syslog_log_options.min_severity != boost::none ? *syslog_log_options.min_severity
                                                                                     : common_log_min_severity;
                static constexpr int default_options = LOG_CONS | LOG_ODELAY;
                syslog.options = default_options;
                log_options.syslog = syslog;
                return;
            }
            }
            raise<Exception>("Invalid device");
        });
}

LibPg::Dsn* admin_connection_dsn = nullptr;
LibPg::Dsn* user_connection_dsn = nullptr;

template <typename Src, typename Dst>
void copy_if_present(const boost::optional<Src>& src, LibStrong::Optional<Dst>& dst)
{
    if (src != boost::none)
    {
        dst = Dst{*src};
    }
}

template <typename Tag>
decltype(auto) make_dsn(const Options::DatabaseOptions<Tag>& option)
{
    LibPg::Dsn result;
    copy_if_present(option.host, result.host);
    copy_if_present(option.host_addr, result.host_addr);
    copy_if_present(option.port, result.port);
    copy_if_present(option.user, result.user);
    copy_if_present(option.dbname, result.db_name);
    copy_if_present(option.password, result.password);
    copy_if_present(option.connect_timeout, result.connect_timeout);
    return result;
}

const LibPg::Dsn& set_admin_connection_dsn(const Options::AdminDatabase& cfg)
{
    if (admin_connection_dsn == nullptr)
    {
        admin_connection_dsn = new LibPg::Dsn{make_dsn(cfg)};
    }
    return *admin_connection_dsn;
}

LibPg::Dsn get_admin_connection_dsn(std::string database_name)
{
    auto dsn = *admin_connection_dsn;
    dsn.db_name = LibPg::Dsn::DbName{std::move(database_name)};
    return dsn;
}

LibPg::PgConnection get_admin_connection(std::string database_name)
{
    return LibPg::PgConnection{get_admin_connection_dsn(std::move(database_name))};
}

const LibPg::Dsn& set_user_connection_dsn(const Options::Database& cfg)
{
    if (user_connection_dsn == nullptr)
    {
        user_connection_dsn = new LibPg::Dsn{make_dsn(cfg)};
    }
    return *user_connection_dsn;
}

std::string to_optional_string(const boost::optional<std::string>& value)
{
    return value == boost::none ? std::string{}
                                : *value;
}

std::string to_optional_string(const boost::optional<in_port_t>& value)
{
    return value == boost::none ? std::string{}
                                : std::to_string(*value);
}

std::string to_optional_string(const boost::optional<std::chrono::seconds>& value)
{
    return value == boost::none ? std::string{}
                                : std::to_string(value->count());
}

Util::ArgumentsFactory& push_back(Util::ArgumentsFactory& args, const Options::Database& cfg)
{
    return args.push_back_non_empty("--database.name=", to_optional_string(cfg.dbname))
               .push_back_non_empty("--database.host=", to_optional_string(cfg.host))
               .push_back_non_empty("--database.password=", to_optional_string(cfg.password))
               .push_back_non_empty("--database.port=", to_optional_string(cfg.port))
               .push_back_non_empty("--database.timeout=", to_optional_string(cfg.connect_timeout))
               .push_back_non_empty("--database.user=", to_optional_string(cfg.user));
}

Util::ArgumentsFactory& push_back(Util::ArgumentsFactory& args, const Options::AdminDatabase& cfg)
{
    return args.push_back_non_empty("--admin_database.name=", to_optional_string(cfg.dbname))
               .push_back_non_empty("--admin_database.host=", to_optional_string(cfg.host))
               .push_back_non_empty("--admin_database.password=", to_optional_string(cfg.password))
               .push_back_non_empty("--admin_database.port=", to_optional_string(cfg.port))
               .push_back_non_empty("--admin_database.timeout=", to_optional_string(cfg.connect_timeout))
               .push_back_non_empty("--admin_database.user=", to_optional_string(cfg.user));
}

std::string to_optional_string(LibLog::Level value)
{
    switch (value)
    {
        case LibLog::Level::critical: return "critical";
        case LibLog::Level::error: return "error";
        case LibLog::Level::warning: return "warning";
        case LibLog::Level::info: return "info";
        case LibLog::Level::debug: return "debug";
        case LibLog::Level::trace: return "trace";
    }
    return "";
}

std::string to_optional_string(LibLog::Sink::ConsoleSinkConfig::OutputStream value)
{
    switch (value)
    {
        case LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr: return "stderr";
        case LibLog::Sink::ConsoleSinkConfig::OutputStream::stdout: return "stdout";
    }
    return "";
}

std::string to_optional_string(LibLog::ColorMode value)
{
    switch (value)
    {
        case LibLog::ColorMode::always: return "always";
        case LibLog::ColorMode::automatic: return "auto";
        case LibLog::ColorMode::never: return "never";
    }
    return "";
}

Util::ArgumentsFactory& push_back(Util::ArgumentsFactory& args, const Options::Log& cfg)
{
    if (cfg.console != boost::none)
    {
        args.push_back("--log.device=console")
            .push_back_non_empty("--log.console.min_severity=", to_optional_string(cfg.console->min_severity))
            .push_back_non_empty("--log.console.output_stream=", to_optional_string(cfg.console->output_stream))
            .push_back_non_empty("--log.console.color_mode=", to_optional_string(cfg.console->color_mode));
    }
    if (cfg.file != boost::none)
    {
        args.push_back("--log.device=file")
            .push_back_non_empty("--log.file.file_name=", cfg.file->file_name)
            .push_back_non_empty("--log.file.min_severity=", to_optional_string(cfg.file->min_severity));
    }
    if (cfg.syslog != boost::none)
    {
        args.push_back("--log.device=syslog")
            .push_back_non_empty("--log.syslog.min_severity=", to_optional_string(cfg.syslog->min_severity))
            .push_back_non_empty("--log.syslog.facility=", std::to_string(cfg.syslog->facility));
    }
    if (cfg.log_grpc_library_events)
    {
        args.push_back("--log.grpc_library");
    }
    return args;
}

void copy_database(const LibPg::PgConnection& conn, const std::string& source_db, const std::string& target_db)
{
    exec(conn, "CREATE DATABASE \"" + target_db + "\" WITH TEMPLATE \"" + source_db + "\"");
}

void drop_database_if_exists(const LibPg::PgConnection& conn, const std::string& db)
{
    exec(conn, "DROP DATABASE IF EXISTS \"" + db + "\"");
}

std::vector<std::string>& push_back(std::vector<std::string>& args, std::string argv)
{
    args.push_back(std::move(argv));
    return args;
}

std::vector<std::string>& push_back_non_empty(
        std::vector<std::string>& args,
        const char* option_name,
        std::string argv)
{
    if (!argv.empty())
    {
        push_back(args, option_name).push_back(std::move(argv));
    }
    return args;
}

std::vector<std::string>& push_back_non_empty(
        std::vector<std::string>& args,
        const char* option_name,
        boost::optional<std::string> argv)
{
    return push_back_non_empty(
            args,
            option_name,
            argv == boost::none ? std::string{}
                                : *std::move(argv));
}

struct ProcessResult
{
    std::string stdout;
    std::string stderr;
    Util::RunInBackground::ExitStatus exit_status;
};

struct PidLess
{
    bool operator()(Util::RunInBackground::ProcessId lhs, Util::RunInBackground::ProcessId rhs) const noexcept { return lhs.value < rhs.value; }
};

using ProcessesData = std::map<Util::RunInBackground::ProcessId, ProcessResult, PidLess>;

void show_process_result(const ProcessesData::value_type& result)
{
    if (result.second.exit_status.exited())
    {
        if (result.second.exit_status.get_exit_status() == EXIT_SUCCESS)
        {
            std::cout << "process " << result.first.value << " exited successfully" << std::endl;
        }
        else
        {
            std::cout << "process " << result.first.value << " failed with status " << result.second.exit_status.get_exit_status() << std::endl;
        }
    }
    else if (result.second.exit_status.signaled())
    {
        std::cout << "process " << result.first.value << " terminated by signal " << result.second.exit_status.get_term_sig() << std::endl;
    }
    else
    {
        std::cout << "process " << result.first.value << " finished under strange circumstances" << std::endl;
    }
    if (!result.second.stdout.empty())
    {
        std::cout << result.second.stdout << std::flush;
    }
    if (!result.second.stderr.empty())
    {
        std::cerr << result.second.stderr;
    }
}

std::string to_string(boost::unit_test::log_level level)
{
    switch (level)
    {
        case boost::unit_test::log_level::log_successful_tests:
            return "success";
        case boost::unit_test::log_level::log_test_units:
            return "unit_scope";
        case boost::unit_test::log_level::log_messages:
            return "message";
        case boost::unit_test::log_level::log_warnings:
            return "warning";
        case boost::unit_test::log_level::log_all_errors:
            return "error";
        case boost::unit_test::log_level::log_cpp_exception_errors:
            return "cpp_exception";
        case boost::unit_test::log_level::log_system_errors:
            return "system_error";
        case boost::unit_test::log_level::log_fatal_errors:
            return "fatal_error";
        case boost::unit_test::log_level::log_nothing:
            return "nothing";
        case boost::unit_test::log_level::invalid_log_level:
            break;
    }
    return "";
}

std::string get_log_level()
{
    if (boost::unit_test::runtime_config::has(boost::unit_test::runtime_config::btrt_log_level))
    {
        return to_string(boost::unit_test::runtime_config::get<boost::unit_test::log_level>(boost::unit_test::runtime_config::btrt_log_level));
    }
    return "";
}

static constexpr auto to_set_log_ctx = true;

void clone_database(
        std::string src_database,
        std::vector<std::string>::const_iterator dst_database_begin,
        std::vector<std::string>::const_iterator dst_database_end,
        bool set_log_ctx = !to_set_log_ctx)
{
    const auto log_ctx =
            set_log_ctx ? std::unique_ptr<LibLog::SetContext>{new LibLog::SetContext{"clone"}}
                        : std::unique_ptr<LibLog::SetContext>{nullptr};
    auto threads = std::list<std::thread>{};
    if (dst_database_end != dst_database_begin)
    {
        const auto conn = get_admin_connection(src_database);
        while (true)
        {
            const auto number_of_databases = dst_database_end - dst_database_begin;
            if (number_of_databases == 0)
            {
                break;
            }
            copy_database(conn, src_database, *dst_database_begin);
            if (number_of_databases == 1)
            {
                break;
            }
            auto dst_database_middle = dst_database_begin + 1 + ((number_of_databases - 1) / 2);
            threads.emplace_back(
                    clone_database,
                    *dst_database_begin,
                    dst_database_begin + 1,
                    dst_database_middle,
                    to_set_log_ctx);
            dst_database_begin = dst_database_middle;
        }
    }
    std::for_each(begin(threads), end(threads), [](auto&& thread) { thread.join(); });
}

void run_parallel(
        const Options::Generic& generic_cfg,
        const Options::AdminDatabase& admin_database_cfg,
        const Options::Database& database_cfg,
        const Options::Log& log_cfg,
        const std::string& test_database_name,
        const Util::TestTreeList& tests)
{
    const auto processes = static_cast<int>(tests.size()) < generic_cfg.parallel
            ? static_cast<int>(tests.size())
            : generic_cfg.parallel;
    std::cout << "run parallel in " << processes << " processes" << std::endl;
    const auto master_source_db = database_cfg.dbname != boost::none ? *database_cfg.dbname
                                                                     : *database_cfg.user;
    const auto make_process_db_name = [&](int process_id)
    {
        return master_source_db + "-" + std::to_string(process_id);
    };
    const auto make_process_log_file = [&](int process_id)
    {
        if (log_cfg.file == boost::none)
        {
            return std::string{};
        }
        return log_cfg.file->file_name + "." + std::to_string(process_id);
    };
    const auto create_databases = [&]()
    {
        auto databases = std::vector<std::string>{};
        databases.reserve(processes);
        for (int process_id = 0; process_id < processes; ++process_id)
        {
            databases.push_back(make_process_db_name(process_id));
        }
        try
        {
            clone_database(test_database_name, begin(databases), end(databases));
        }
        catch (const std::exception& e)
        {
            std::cerr << "clone_database failed: " << e.what() << std::endl;
        }
        catch (...)
        {
            std::cerr << "clone_database failed" << std::endl;
        }
    };
    const auto drop_databases = [&]()
    {
        const auto drop_database = [&](int process_id)
        {
            const auto log_ctx =
                    0 < process_id ? std::unique_ptr<LibLog::SetContext>{new LibLog::SetContext{"drop"}}
                                   : std::unique_ptr<LibLog::SetContext>{nullptr};
            const auto admin_db_conn = get_admin_connection(test_database_name);
            const auto process_db_name = make_process_db_name(process_id);
            try
            {
                drop_database_if_exists(admin_db_conn, process_db_name);
            }
            catch (const std::exception& e)
            {
                std::cerr << "DROP DATABASE failed: " << e.what() << std::endl;
            }
            catch (...)
            {
                std::cerr << "DROP DATABASE failed" << std::endl;
            }
        };
        auto threads = std::list<std::thread>{};
        for (int process_id = 1; process_id < processes; ++process_id)
        {
            threads.emplace_back(drop_database, process_id);
        }
        if (0 < processes)
        {
            drop_database(0);
        }
        std::for_each(begin(threads), end(threads), [](auto&& thread) { thread.join(); });
    };
    create_databases();
    ScopeExit do_on_exit{drop_databases};
    Util::RunInBackground running_tests;
    ProcessesData tests_results;
    const auto on_stdout = [&](Util::RunInBackground::ProcessId pid, const char* data, std::ptrdiff_t bytes)
    {
        if (data == nullptr)
        {
            return;
        }
        const auto result_iter = tests_results.find(pid);
        if (result_iter != end(tests_results))
        {
            result_iter->second.stdout.append(data, bytes);
        }
    };
    const auto on_stderr = [&](Util::RunInBackground::ProcessId pid, const char* data, std::ptrdiff_t bytes)
    {
        if (data == nullptr)
        {
            return;
        }
        const auto result_iter = tests_results.find(pid);
        if (result_iter != end(tests_results))
        {
            result_iter->second.stderr.append(data, bytes);
        }
    };
    const auto on_exit = [&](Util::RunInBackground::ProcessId pid, Util::RunInBackground::ExitStatus exit_status)
    {
        const auto result_iter = tests_results.find(pid);
        if (result_iter != end(tests_results))
        {
            result_iter->second.exit_status = exit_status;
        }
    };
    {
        const auto cmd = boost::unit_test::framework::master_test_suite().argv[0];
        Util::RunInBackground::Consumer event_consumer{on_stdout, on_stderr, on_exit};
        ProcessResult process_data{std::string{}, std::string{}, Util::RunInBackground::ExitStatus{-1}};
        for (int process_id = 0; process_id < processes; ++process_id)
        {
            std::vector<std::string> args{};
            const auto get_test_idx = [&](int process_id) { return (tests.size() * process_id) / processes; };
            for (auto idx = get_test_idx(process_id); idx < get_test_idx(process_id + 1); ++idx)
            {
                push_back_non_empty(args, "-t", tests[idx]);
            }
            push_back_non_empty(args, "-l", get_log_level());
            push_back(args, "--");
            push_back_non_empty(args, "-c", generic_cfg.config_file_name);
            push_back_non_empty(args, "-s", "no");
            push_back_non_empty(args, "--database.dbname", make_process_db_name(process_id));
            push_back_non_empty(args, "--log.file_name", make_process_log_file(process_id));
            const auto pid = running_tests(cmd, begin(args), end(args), event_consumer);
            tests_results.insert(std::make_pair(pid, process_data));
        }
    }
    running_tests.wait();
    std::for_each(begin(tests_results), end(tests_results), show_process_result);
}

}//namespace Test::Cfg::{anonymous}

Options::Options(
        int argc,
        const char* const* argv,
        std::function<void(const std::string&)> on_unrecognized)
{
    CommonLogOptions common_log_options;
    ConsoleLogOptions console_log_options;
    FileLogOptions file_log_options;
    SyslogLogOptions syslog_log_options;
    const auto generic_options = make_generic_options(generic);
    const auto database_options = make_database_options(database);
    const auto admin_database_options = make_admin_database_options(admin_database);
    const auto log_options = make_log_options(common_log_options);
    const auto log_console_options = make_log_console_options(console_log_options);
    const auto log_file_options = make_log_file_options(file_log_options);
    const auto log_syslog_options = make_log_syslog_options(syslog_log_options);

    boost::program_options::options_description command_line_options{"test-public-request options", terminal_width};
    command_line_options
        .add(generic_options)
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(database_options)
        .add(admin_database_options);
    boost::program_options::options_description config_file_options;
    config_file_options
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(database_options)
        .add(admin_database_options);

    boost::program_options::variables_map variables_map;
    const auto has_option = [&](const char* option) { return 0 < variables_map.count(option); };
    const auto unregistered = [&]()
    {
        try
        {
            auto parsed = boost::program_options::command_line_parser{argc, argv}
                    .options(command_line_options)
                    .allow_unregistered()
                    .run();
            boost::program_options::store(parsed, variables_map);
            boost::program_options::notify(variables_map);
            return boost::program_options::collect_unrecognized(
                    parsed.options,
                    boost::program_options::include_positional);
        }
        catch (const boost::program_options::unknown_option& unknown_option)
        {
            std::ostringstream out;
            out << unknown_option.what() << "\n\n" << command_line_options;
            raise<UnknownOption>(out.str());
        }
    }();
    boost::program_options::notify(variables_map);

    if (has_option("help"))
    {
        std::ostringstream out;
        out << command_line_options;
        raise<AllDone>(out.str());
    }
    if (has_option("version"))
    {
        std::ostringstream out;
        out << "Server Version: " << PACKAGE_VERSION << std::endl;
        out << "PublicRequest API Version: " << API_PUBLIC_REQUEST_VERSION << std::endl;
        out << "Diagnostics API Version: " << API_DIAGNOSTICS_VERSION << std::endl;
        out << "LibDiagnostics Version: " << LIBDIAGNOSTICS_VERSION << std::endl;
        out << "LibFred Version: " << LIBFRED_VERSION << std::endl;
        out << "LibLog Version: " << LIBLOG_VERSION << std::endl;
        out << "LibPg Version: " << LIBPG_VERSION << std::endl;
        out << "LibStrong Version: " << LIBSTRONG_VERSION << std::endl;
        raise<AllDone>(out.str());
    }
    const bool config_file_name_presents = generic.config_file_name != boost::none;
    if (config_file_name_presents)
    {
        std::ifstream config_file{*generic.config_file_name};
        if (!config_file)
        {
            raise<Exception>("can not open config file \"" + *generic.config_file_name + "\"");
        }
        try
        {
            boost::program_options::store(
                    boost::program_options::parse_config_file(
                            config_file,
                            config_file_options,
                            true),//allow unregistered
                    variables_map);
        }
        catch (const boost::program_options::unknown_option& unknown_option)
        {
            std::ostringstream out;
            out << unknown_option.what() << "\n\n" << command_line_options;
            raise<UnknownOption>(out.str());
        }
        boost::program_options::notify(variables_map);
    }

    collect_log_options(
        common_log_options,
        console_log_options,
        file_log_options,
        syslog_log_options,
        log);

    const auto required = [&](const char* variable)
    {
        if (variables_map.count(variable) == 0)
        {
            raise<MissingOption>("missing option: '" + std::string{variable} + "'");
        }
    };
}

const Options& Options::get()
{
    return init(fake_argc, fake_argv, nullptr);
}

const Options& Options::init(
        int argc,
        const char* const* argv,
        std::function<void(const std::string&)> on_unrecognized)
{
    static const Options* singleton_ptr = nullptr;
    const bool init_requested = (argc != fake_argc) || (argv != fake_argv);
    const bool first_run = singleton_ptr == nullptr;
    if (first_run)
    {
        if (!init_requested)
        {
            raise<Exception>("First call of Cfg::Options::init must contain valid arguments");
        }
        static const Options singleton{argc, argv, on_unrecognized};
        singleton_ptr = &singleton;
    }
    else if (init_requested)
    {
        raise<Exception>("Only first call of Cfg::Options::init can contain valid arguments");
    }
    return *singleton_ptr;
}

DatabaseAdministrator::DatabaseAdministrator(
        std::string test_database_name,
        std::function<void()> database_setup_procedure)
    : origin_{},
      test_{},
      test_database_name_{std::move(test_database_name)}
{
    if (database_setup_procedure != nullptr)
    {
        origin_.backup(test_database_name_, test_database_name_ + "-orig");
        LIBLOG_TRACE("setup fred database start");
        database_setup_procedure();
        LIBLOG_TRACE("setup fred database done");
    }
    else
    {
        auto tx = Test::make_rw_transaction();
        DbData::load(tx);
        commit(std::move(tx));
    }
}

DatabaseAdministrator::~DatabaseAdministrator()
{
}

std::function<void()> DatabaseAdministrator::get_restore_test_database_procedure()
{
    test_.backup(test_database_name_, test_database_name_ + "-bak");
    return [&]() { test_.recover(); };
}

DatabaseAdministrator::BackupDatabase::~BackupDatabase()
{
    if (this->has_backup())
    {
        try
        {
            this->reset();
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("reset failure: {}", e.what());
        }
        catch (...) { }
    }
}

void DatabaseAdministrator::BackupDatabase::backup(std::string origin, std::string backup)
{
    origin_ = std::move(origin);
    backup_ = std::move(backup);
    LIBLOG_TRACE("backup {} into {}", origin_, backup_);
    const auto admin_db_conn = get_admin_connection(origin_);
    drop_database_if_exists(admin_db_conn, backup_);
    copy_database(admin_db_conn, origin_, backup_);
}

bool DatabaseAdministrator::BackupDatabase::has_backup() const noexcept
{
    return !origin_.empty() && !backup_.empty();
}

void DatabaseAdministrator::BackupDatabase::recover()
{
    LIBLOG_TRACE("recover {} into {}", backup_, origin_);
    auto admin_db_conn = get_admin_connection(backup_);
    drop_database_if_exists(admin_db_conn, origin_);
    copy_database(admin_db_conn, backup_, origin_);
}

void DatabaseAdministrator::BackupDatabase::reset()
{
    auto admin_db_conn = get_admin_connection(backup_);
    LIBLOG_TRACE("reset {} into {}", backup_, origin_);
    drop_database_if_exists(admin_db_conn, origin_);
    copy_database(admin_db_conn, backup_, origin_);
    admin_db_conn = get_admin_connection(origin_);
    drop_database_if_exists(admin_db_conn, backup_);
    origin_.clear();
    backup_.clear();
}

void disable_grpc_library_logging()
{
    static const auto dummy_log_func = [](::gpr_log_func_args*) { };
    ::gpr_set_log_function(dummy_log_func);
    static constexpr auto do_not_log = static_cast<::gpr_log_severity>(GPR_LOG_SEVERITY_ERROR + 1);
    ::gpr_set_log_verbosity(do_not_log);
}

void grpc_log(::gpr_log_func_args* log_args)
{
    try
    {
        switch (log_args->severity)
        {
            case GPR_LOG_SEVERITY_DEBUG:
                LIBLOG_DEBUG("{} at {}:{}", log_args->message, log_args->file, log_args->line);
                return;
            case GPR_LOG_SEVERITY_INFO:
                LIBLOG_INFO("{} at {}:{}", log_args->message, log_args->file, log_args->line);
                return;
            case GPR_LOG_SEVERITY_ERROR:
                LIBLOG_ERROR("{} at {}:{}", log_args->message, log_args->file, log_args->line);
                return;
        }
        LIBLOG_WARNING("{} at {}:{}", log_args->message, log_args->file, log_args->line);
    } catch (...)
    { }
}

constexpr auto no_log_grpc_severity = static_cast<::gpr_log_severity>(GPR_LOG_SEVERITY_ERROR + 1);

constexpr ::gpr_log_severity make_grpc_log_severity(LibLog::Level min_severity)
{
    switch (min_severity)
    {
        case LibLog::Level::critical:
            return no_log_grpc_severity;
        case LibLog::Level::error:
            return GPR_LOG_SEVERITY_ERROR;
        case LibLog::Level::warning:
        case LibLog::Level::info:
            return GPR_LOG_SEVERITY_INFO;
        case LibLog::Level::debug:
        case LibLog::Level::trace:
            return GPR_LOG_SEVERITY_DEBUG;
    }
    return no_log_grpc_severity;
}

decltype(auto) get_min_severity(const Cfg::Options::Log& log_options)
{
    boost::optional<LibLog::Level> min_severity;
    const auto update_min_severity = [&](LibLog::Level severity)
    {
        if ((min_severity == boost::none) ||
            (severity < *min_severity))
        {
            min_severity = severity;
        }
    };
    if (log_options.console != boost::none)
    {
        update_min_severity(log_options.console->min_severity);
    }
    if (log_options.file != boost::none)
    {
        update_min_severity(log_options.file->min_severity);
    }
    if (log_options.syslog != boost::none)
    {
        update_min_severity(log_options.syslog->min_severity);
    }
    return min_severity;
}

void set_grpc_library_logging(const Cfg::Options::Log& log_options)
{
    if (!log_options.log_grpc_library_events)
    {
        LIBLOG_INFO("do not log gRPC library");
        disable_grpc_library_logging();
        return;
    }
    const auto min_severity = get_min_severity(log_options);
    if (min_severity == boost::none)
    {
        disable_grpc_library_logging();
        return;
    }
    ::gpr_set_log_function(grpc_log);
    ::gpr_set_log_verbosity(make_grpc_log_severity(*min_severity));
    LIBLOG_INFO("log gRPC library");
}

decltype(auto) make_sink_config(const Cfg::Options::Log::Console& log_options)
{
    LibLog::Sink::ConsoleSinkConfig sink_config;
    sink_config.set_level(log_options.min_severity);
    sink_config.set_output_stream(log_options.output_stream);
    sink_config.set_color_mode(log_options.color_mode);
    return sink_config;
}

decltype(auto) make_sink_config(const Cfg::Options::Log::File& log_options)
{
    LibLog::Sink::FileSinkConfig sink_config{log_options.file_name};
    sink_config.set_level(log_options.min_severity);
    return sink_config;
}

decltype(auto) make_sink_config(const Cfg::Options::Log::Syslog& log_options)
{
    LibLog::Sink::SyslogSinkConfig sink_config;
    sink_config.set_syslog_facility(log_options.facility);
    sink_config.set_level(log_options.min_severity);
    sink_config.set_syslog_ident(log_options.ident);
    sink_config.set_syslog_options(log_options.options);
    return sink_config;
}

void set_logging(const Options::Log& log_options)
{
    bool has_device = false;
    LibLog::LogConfig log_config;
    if (log_options.console != boost::none)
    {
        try
        {
            log_config.add_sink_config(make_sink_config(*log_options.console));
            has_device = true;
        }
        catch (...) { }
    }
    if (log_options.file != boost::none)
    {
        try
        {
            log_config.add_sink_config(make_sink_config(*log_options.file));
            has_device = true;
        }
        catch (...) { }
    }
    if (log_options.syslog != boost::none)
    {
        try
        {
            log_config.add_sink_config(make_sink_config(*log_options.syslog));
            has_device = true;
        }
        catch (...) { }
    }
    if (has_device)
    {
        try
        {
            Fred::PublicRequest::Util::install_pg_log_layer();
            LibLog::Log::start<LibLog::ThreadMode::multi_threaded>(log_config);
            set_grpc_library_logging(log_options);
            return;
        }
        catch (const std::exception& e)
        {
            std::cerr << "Setting up logging failure: " << e.what() << std::endl;
            ::exit(EXIT_FAILURE);
        }
        catch (...)
        {
            std::cerr << "Setting up logging failure: Unexpected exception caught" << std::endl;
            ::exit(EXIT_FAILURE);
        }
    }
    try
    {
        disable_grpc_library_logging();
    }
    catch (...)
    {
        std::cerr << "unable to disable gRPC library logging" << std::endl;
    }
    std::cout << "no logging is configured!?" << std::endl;
}

}//namespace Test::Cfg
}//namespace Test

using namespace Test::Cfg;

DatabaseAdministrator Test::Cfg::handle_command_line_args(
        std::function<void()> database_setup_procedure)
{
    try
    {
        auto unrecognized = std::vector<std::string>{};
        auto option_name = std::string{};
        const auto& cfg = Options::init(
                boost::unit_test::framework::master_test_suite().argc,
                boost::unit_test::framework::master_test_suite().argv,
                [&](const std::string& item)
                {
                    if (option_name.empty())
                    {
                        option_name = item;
                    }
                    else
                    {
                        unrecognized.push_back("--" + option_name + "=" + item);
                        option_name.clear();
                    }
                });
        set_logging(cfg.log);
        {
            set_admin_connection_dsn(cfg.admin_database);
            set_user_connection_dsn(cfg.database);
            const auto test_database_name = cfg.database.dbname != boost::none ? *cfg.database.dbname
                                                                               : *cfg.database.user;
            auto db_administrator = cfg.generic.invoke_database_setup
                    ? DatabaseAdministrator{test_database_name, std::move(database_setup_procedure)}
                    : DatabaseAdministrator{test_database_name};
            if (cfg.generic.parallel <= 1)
            {
                return db_administrator;
            }
            const auto tests = []()
            {
                auto tests = Util::get_test_tree();
                std::random_device rd;
                std::mt19937 rnd_gen{rd()};
                std::shuffle(begin(tests), end(tests), rnd_gen);
                return tests;
            }();
            if (tests.size() <= 1)
            {
                return db_administrator;
            }
            run_parallel(
                    cfg.generic,
                    cfg.admin_database,
                    cfg.database,
                    cfg.log,
                    test_database_name,
                    tests);
        }
        ::_exit(EXIT_SUCCESS);
    }
    catch (const AllDone& all_done)
    {
        std::cout << all_done.what() << std::endl;
        ::exit(EXIT_SUCCESS);
    }
    catch (const UnknownOption& unknown_option)
    {
        std::cerr << unknown_option.what() << std::endl;
        ::exit(EXIT_FAILURE);
    }
    catch (const MissingOption& missing_option)
    {
        std::cerr << missing_option.what() << std::endl;
        ::exit(EXIT_FAILURE);
    }
    catch (const Exception& e)
    {
        std::cerr << "Configuration problem: " << e.what() << std::endl;
        ::exit(EXIT_FAILURE);
    }
    catch (const std::exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        ::exit(EXIT_FAILURE);
    }
    catch (...)
    {
        std::cerr << "Unexpected exception caught" << std::endl;
        ::exit(EXIT_FAILURE);
    }
}

LibPg::PgConnection Test::Cfg::get_admin_connection()
{
    return LibPg::PgConnection{*admin_connection_dsn};
}

LibPg::PgConnection Test::Cfg::get_user_connection()
{
    return LibPg::PgConnection{*user_connection_dsn};
}
