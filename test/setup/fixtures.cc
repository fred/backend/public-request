/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test/setup/fixtures.hh"
#include "test/setup/cfg.hh"

#include "src/lib/get_type_traits.hh"

#include "libfred/public_request/create_public_request.hh"
#include "libfred/public_request/get_public_request.hh"
#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/registrable_object/domain/domain_name.hh"
#include "libfred/registrable_object/domain/info_domain.hh"
#include "libfred/registrable_object/keyset/info_keyset.hh"
#include "libfred/registrable_object/nsset/info_nsset.hh"
#include "libfred/registrar/info_registrar.hh"
#include "libfred/zone/create_zone.hh"
#include "libfred/zone/exceptions.hh"
#include "libfred/zone/info_zone.hh"

#include "liblog/liblog.hh"

#include <boost/test/framework.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include <stdexcept>

namespace Test {

namespace {

std::string get_original_db_name()
{
    return Cfg::Options::get().database.dbname != boost::none ? *Cfg::Options::get().database.dbname
                                                              : *Cfg::Options::get().database.user;
}

std::string get_db_template_name()
{
    return get_original_db_name() + "-tt";//Test Template
}

constexpr unsigned max_postgresql_database_name_length = 63;

void check_dbname_length(const std::string& db_name)
{
    if (max_postgresql_database_name_length < db_name.length())
    {
        throw std::runtime_error{
                "db_name.length(): " + std::to_string(db_name.length()) + " is greater than "
                "max_postgresql_database_name_length: " + std::to_string(max_postgresql_database_name_length) + " "
                "db_name: " + db_name};
    }
}

void drop_db(const std::string& db_name, const LibPg::PgConnection& conn)
{
    check_dbname_length(db_name);
    exec(conn, "DROP DATABASE IF EXISTS \""+ db_name +"\"");
}

void copy_db(const std::string& src_name, const std::string& dst_name, const LibPg::PgConnection& conn)
{
    check_dbname_length(src_name);
    check_dbname_length(dst_name);
    drop_db(dst_name, conn);
    exec(conn, "CREATE DATABASE \"" + dst_name + "\" TEMPLATE \"" + src_name + "\"");
}

std::string get_unit_test_path(const boost::unit_test::test_unit& tu,
                               const std::string& delimiter = "/")
{
    using namespace boost::unit_test;
    const bool is_orphan = (tu.p_parent_id < MIN_TEST_SUITE_ID) || (tu.p_parent_id == INV_TEST_UNIT_ID);
    if (is_orphan)
    {
        return static_cast<std::string>(tu.p_name);
    }
    return get_unit_test_path(framework::get<test_suite>(tu.p_parent_id), delimiter) +
           delimiter + static_cast<std::string>(tu.p_name);
}

struct GetNonEnumZone : boost::static_visitor<LibFred::Zone::NonEnumZone>
{
    LibFred::Zone::NonEnumZone operator()(const LibFred::Zone::NonEnumZone& value) const
    {
        return value;
    }
    template <typename T>
    LibFred::Zone::NonEnumZone operator()(const T&) const
    {
        throw std::runtime_error{"not a non-enum zone"};
    }
};

auto get_non_enum_zone(const LibPg::PgRwTransaction& tx, const char* zone, bool idn_enabled)
{
    try
    {
        return boost::apply_visitor(GetNonEnumZone{}, LibFred::Zone::InfoZone{zone}.exec(tx));
    }
    catch (const LibFred::Zone::NonExistentZone&) { }
    catch (const LibFred::Zone::InfoZoneException& e)
    {
        LIBLOG_INFO("InfoZone failed: {}", e.what());
    }
    catch (const std::exception& e)
    {
        LIBLOG_ERROR("InfoZone failed: {}", e.what());
        throw;
    }
    catch (...)
    {
        LIBLOG_ERROR("InfoZone failed by an unexpected exception");
        throw;
    }
    static constexpr int expiration_period_min_in_months = 12;
    static constexpr int expiration_period_max_in_months = 120;
    LibFred::Zone::CreateZone{zone, expiration_period_min_in_months, expiration_period_max_in_months}
            .exec(tx);
    if (!idn_enabled)
    {
        LibFred::Domain::set_domain_name_validation_config_into_database(tx, zone, { "dncheck_no_consecutive_hyphens" });
    }
    return boost::apply_visitor(GetNonEnumZone{}, LibFred::Zone::InfoZone{zone}.exec(tx));
}

std::string make_fqdn(const char* subdomain, std::size_t subdomain_length, const char* zone)
{
    auto fqdn = std::string{};
    if (subdomain_length == 0)
    {
        fqdn = zone;
        return fqdn;
    }
    const auto zone_length = std::strlen(zone);
    if (subdomain[subdomain_length - 1] == '.')
    {
        fqdn.reserve(subdomain_length + zone_length);
        fqdn.append(subdomain, subdomain_length);
    }
    else
    {
        fqdn.reserve(subdomain_length + 1 + zone_length);
        fqdn.append(subdomain, subdomain_length);
        fqdn.append(1, '.');
    }
    fqdn.append(zone, zone_length);
    return fqdn;
}

std::string make_fqdn(const char* subdomain, const char* zone)
{
    return make_fqdn(subdomain, std::strlen(subdomain), zone);
}

std::string make_fqdn(const std::string& subdomain, const char* zone)
{
    return make_fqdn(subdomain.c_str(), subdomain.length(), zone);
}

std::string make_fqdn(unsigned long long subdomain, const char* zone)
{
    auto fqdn = std::string{};
    if (subdomain == 0)
    {
        fqdn = zone;
        return fqdn;
    }
    const auto number_of_digits = [](unsigned long long value)
    {
        unsigned result = 1;
        while (10 <= value)
        {
            ++result;
            value /= 10;
        }
        return result;
    }(subdomain);
    const auto zone_length = std::strlen(zone);
    fqdn.reserve(2 * number_of_digits + zone_length);
    while (0 < subdomain)
    {
        const char level[2] = {char('0' + (subdomain % 10)), '.'};
        fqdn.append(level, 2);
        subdomain /= 10;
    }
    fqdn.append(zone, zone_length);
    return fqdn;
}

decltype(auto) make_registrar(const LibPg::PgRwTransaction& tx, LibFred::CreateRegistrar& create)
{
    for (int cnt = 0; cnt < 2; ++cnt)
    {
        try
        {
            exec(tx, "SAVEPOINT make_registrar");
            const auto registrar_id = create.exec(tx);
            exec(tx, "RELEASE SAVEPOINT make_registrar");
            return LibFred::InfoRegistrarById{registrar_id}.exec(tx).info_registrar_data;
        }
        catch (const LibFred::CreateRegistrar::Exception& e)
        {
            exec(tx, "ROLLBACK TO SAVEPOINT make_registrar");
            if (e.is_set_invalid_registrar_handle())
            {
                return LibFred::InfoRegistrarByHandle{e.get_invalid_registrar_handle()}.exec(tx).info_registrar_data;
            }
            if (!e.is_set_invalid_registrar_varsymb())
            {
                LIBLOG_WARNING("unable to create registrar: {}", e.what());
                throw;
            }
            LIBLOG_INFO("unable to create registrar: try to change variable symbol");
            create.set_variable_symbol("no symbol");
        }
        catch (...)
        {
            exec(tx, "ROLLBACK TO SAVEPOINT make_registrar");
            LIBLOG_WARNING("unable to create registrar");
            throw;
        }
    }
    LIBLOG_WARNING("unable to create registrar");
    struct NoRegistrarHasBeenMade : std::exception
    {
        const char* what() const noexcept override { return "no registrar has been made"; }
    };
    throw NoRegistrarHasBeenMade{};
}

std::string get_version(int index)
{
   if (index < 0)
   {
       throw std::runtime_error{"negative index is not allowed"};
   }
   static constexpr char number_of_letters = 'Z' - 'A';
   if (index <= number_of_letters)
   {
       return std::string(1, 'A' + index);
   }
   return std::to_string(index - number_of_letters);
}

static constexpr auto is_system_registrar = true;

void set_registrar(LibFred::CreateRegistrar& op, bool system, int index)
{
    const auto version = get_version(index);
    if (system)
   {
       op.set_name(version + " System Registrar")
         .set_organization(version + " System Registrar Ltd")
         .set_email("system-registrar-" + version + "@nic.com")
         .set_url("https://system-registrar-" + version + ".nic.com");
   }
   else
   {
       op.set_name(version + " Registrar")
         .set_organization(version + " Registrar Gmbh.")
         .set_email("registrar-" + version + "@registrar.com")
         .set_url("https://registrar-" + version + ".registrar.com");
   }
   op.set_street({"Street 1 - " + version,
                  "Street 2 - " + version,
                  "Street 3 - " + version})
     .set_city("City " + version)
     .set_stateorprovince("State Or Province " + version)
     .set_postalcode("143 21")
     .set_country("CZ")
     .set_telephone("+420.441207848")
     .set_fax("+420.361971091")
     .set_ico("1234" + version)
     .set_dic("5678" + version)
     .set_variable_symbol("VS-" + std::to_string(2 * index + (system ? 1 : 0)))
     .set_payment_memo_regex(version)
     .set_vat_payer(index % 2 != 0);
}

void set_contact(LibFred::CreateContact& op, int index)
{
    const auto version = get_version(index);
    op.set_name(version + " Contact")
      .set_organization(version + " Contact Gmbh.")
      .set_email("contact-" + version + "@contact.com");
}

decltype(auto) make_contact(const LibPg::PgRwTransaction& tx, LibFred::CreateContact& create)
{
    try
    {
        exec(tx, "SAVEPOINT make_contact");
        const auto contact_id = create.exec(tx).create_object_result.object_id;
        exec(tx, "RELEASE SAVEPOINT make_contact");
        auto info_op = LibFred::InfoContactById{contact_id};
        return info_op.exec(tx).info_contact_data;
    }
    catch (const LibFred::CreateContact::Exception& e)
    {
        if (e.is_set_invalid_contact_handle())
        {
            exec(tx, "ROLLBACK TO SAVEPOINT make_contact");
            auto info_op = LibFred::InfoContactByHandle{e.get_invalid_contact_handle()};
            return info_op.exec(tx).info_contact_data;
        }
        throw;
    }
}

void set_domain(LibFred::CreateDomain&, int)
{
}

decltype(auto) make_domain(const LibPg::PgRwTransaction& tx, LibFred::CreateDomain& create)
{
    try
    {
        exec(tx, "SAVEPOINT make_domain");
        const auto domain_id = create.exec(tx).create_object_result.object_id;
        exec(tx, "RELEASE SAVEPOINT make_domain");
        auto info_op = LibFred::InfoDomainById{domain_id};
        return info_op.exec(tx).info_domain_data;
    }
    catch (const LibFred::CreateObject::Exception& e)
    {
        if (e.is_set_invalid_object_handle())
        {
            exec(tx, "ROLLBACK TO SAVEPOINT make_domain");
            auto info_op = LibFred::InfoDomainByFqdn{e.get_invalid_object_handle()};
            return info_op.exec(tx).info_domain_data;
        }
        throw;
    }
}

void set_keyset(LibFred::CreateKeyset&, int)
{
}

decltype(auto) make_keyset(const LibPg::PgRwTransaction& tx, LibFred::CreateKeyset& create)
{
    try
    {
        exec(tx, "SAVEPOINT make_keyset");
        const auto keyset_id = create.exec(tx).create_object_result.object_id;
        exec(tx, "RELEASE SAVEPOINT make_keyset");
        auto info_op = LibFred::InfoKeysetById{keyset_id};
        return info_op.exec(tx).info_keyset_data;
    }
    catch (const LibFred::CreateObject::Exception& e)
    {
        if (e.is_set_invalid_object_handle())
        {
            exec(tx, "ROLLBACK TO SAVEPOINT make_keyset");
            auto info_op = LibFred::InfoKeysetByHandle{e.get_invalid_object_handle()};
            return info_op.exec(tx).info_keyset_data;
        }
        throw;
    }
}

void set_nsset(LibFred::CreateNsset&, int)
{
}

decltype(auto) make_nsset(const LibPg::PgRwTransaction& tx, LibFred::CreateNsset& create)
{
    try
    {
        exec(tx, "SAVEPOINT make_nsset");
        const auto nsset_id = create.exec(tx).create_object_result.object_id;
        exec(tx, "RELEASE SAVEPOINT make_nsset");
        auto info_op = LibFred::InfoNssetById{nsset_id};
        return info_op.exec(tx).info_nsset_data;
    }
    catch (const LibFred::CreateNsset::Exception& e)
    {
        if (e.is_set_invalid_nsset_handle())
        {
            exec(tx, "ROLLBACK TO SAVEPOINT make_nsset");
            auto info_op = LibFred::InfoNssetByHandle{e.get_invalid_nsset_handle()};
            return info_op.exec(tx).info_nsset_data;
        }
        throw;
    }
}

LibFred::PublicRequest::PublicRequestData make_public_request(
        const LibPg::PgRwTransaction& tx,
        const std::string& type,
        unsigned long long object_id,
        LibFred::Object_Type::Enum object_type,
        const boost::optional<Test::Registrar>& registrar,
        const std::string& email_to_answer,
        LibFred::PublicRequest::Status::Enum status,
        LibFred::PublicRequest::OnStatusAction::Enum on_status_action,
        std::chrono::seconds create_time_shift,
        const boost::optional<unsigned long long>& create_request_id,
        const boost::optional<std::chrono::seconds>& resolve_time_shift,
        const boost::optional<unsigned long long>& resolve_request_id)
{
    {
        auto public_requests = [&]()
        {
            using StatusFilter = LibFred::PublicRequest::Filter<LibFred::PublicRequest::Status::Enum>;
            const auto status_filter = StatusFilter{{status}};
            using OnStatusActionFilter = LibFred::PublicRequest::Filter<LibFred::PublicRequest::OnStatusAction::Enum>;
            const auto on_status_action_filter = OnStatusActionFilter{{on_status_action}};
            using TypeFilter = LibFred::PublicRequest::Filter<std::string>;
            const auto type_filter = TypeFilter{{type}};
            switch (object_type)
            {
                case LibFred::Object_Type::contact:
                    return LibFred::PublicRequest::GetPublicRequestsOfContact{
                            object_id,
                            status_filter,
                            on_status_action_filter,
                            type_filter}.exec(tx);
                case LibFred::Object_Type::domain:
                    return LibFred::PublicRequest::GetPublicRequestsOfDomain{
                            object_id,
                            status_filter,
                            on_status_action_filter,
                            type_filter}.exec(tx);
                case LibFred::Object_Type::keyset:
                    return LibFred::PublicRequest::GetPublicRequestsOfKeyset{
                            object_id,
                            status_filter,
                            on_status_action_filter,
                            type_filter}.exec(tx);
                case LibFred::Object_Type::nsset:
                    return LibFred::PublicRequest::GetPublicRequestsOfNsset{
                            object_id,
                            status_filter,
                            on_status_action_filter,
                            type_filter}.exec(tx);
            }
            struct UnknownRegistryObjectType : std::exception
            {
                const char* what() const noexcept override { return "unknown object type"; }
            };
            throw UnknownRegistryObjectType{};
        }();
        if (!public_requests.empty())
        {
            return std::move(public_requests.front());
        }
    }
    class MinimalPublicRequest : public LibFred::PublicRequestTypeIface
    {
    public:
        explicit MinimalPublicRequest(const std::string& type)
            : type_{type}
        { }
    private:
        std::string get_public_request_type() const override
        {
            return type_;
        }
        PublicRequestTypes get_public_request_types_to_cancel_on_create() const override
        {
            return {};
        }
        PublicRequestTypes get_public_request_types_to_cancel_on_update(
                LibFred::PublicRequest::Status::Enum,
                LibFred::PublicRequest::Status::Enum) const override
        {
            return {};
        }
        const std::string& type_;
    };
    const auto public_request = MinimalPublicRequest{type};
    const auto ctx = LibFred::OperationContext{tx};
    const auto locked_object = LibFred::PublicRequestsOfObjectLockGuardByObjectId{ctx, object_id};
    const auto op = LibFred::CreatePublicRequest{
            Optional<std::string>{},
            email_to_answer.empty() ? Optional<std::string>{} : email_to_answer,
            registrar == boost::none ? Optional<LibFred::RegistrarId>{} : registrar->data.id};
    const auto public_request_id = op.exec(
            locked_object,
            public_request,
            create_request_id == boost::none ? Optional<unsigned long long>()
                                             : Optional<unsigned long long>(*create_request_id));
    if (status != LibFred::PublicRequest::Status::opened ||
        on_status_action != public_request.get_on_status_action(LibFred::PublicRequest::Status::opened) ||
        create_time_shift != std::chrono::seconds{0} ||
        resolve_time_shift != boost::none ||
        resolve_request_id != boost::none)
    {
        const LibPg::PgResultTuples dbres = exec(
                tx,
                "UPDATE public_request "
                   "SET status = eprs.id, "
                       "on_status_action = $3::ENUM_ON_STATUS_ACTION_TYPE, "
                       "create_time = create_time + ($4::TEXT || 'SECONDS')::INTERVAL, "
                       "resolve_time = NOW() + ($5::TEXT || 'SECONDS')::INTERVAL, "
                       "resolve_request_id = $6::BIGINT "
                  "FROM enum_public_request_status eprs "
                 "WHERE public_request.id = $1::BIGINT AND "
                       "eprs.name = $2::TEXT "
             "RETURNING 0",
                LibPg::make_fixed_size_query_arguments(
                        public_request_id,
                        Conversion::Enums::to_db_handle(status),
                        Conversion::Enums::to_db_handle(on_status_action),
                        create_time_shift.count(),
                        resolve_time_shift == boost::none ? boost::none
                                                          : boost::make_optional(resolve_time_shift->count()),
                        resolve_request_id
                ));
        if (dbres.size() != LibPg::RowIndex{1})
        {
            struct UnexpectedNumberOfRows : std::exception
            {
                const char* what() const noexcept override { return "unexpected number of rows"; }
            };
            throw UnexpectedNumberOfRows{};
        }
    }
    auto data = LibFred::PublicRequest::GetPublicRequest{public_request_id}.exec(tx);
    LIBLOG_DEBUG("public_request.id = {}", data.id);
    return data;
}

std::string get_public_request_type(const LibPg::PgRwTransaction& tx)
{
    static const auto types = Fred::PublicRequest::Lib::get_type_traits(tx);
    static auto top = begin(types);
    if (top == end(types))
    {
        top = begin(types);
        if (top == end(types))
        {
            return std::string{};
        }
    }
    return (top++)->first;
}

std::array<PublicRequest, 9> make_public_requests(
        const LibPg::PgRwTransaction& tx,
        unsigned long long object_id,
        LibFred::Object_Type::Enum object_type,
        const boost::optional<Registrar>& registrar)
{
    return {
            PublicRequest{
                    tx,
                    get_public_request_type(tx),
                    object_id,
                    object_type,
                    registrar,
                    "",
                    LibFred::PublicRequest::Status::opened,
                    LibFred::PublicRequest::OnStatusAction::scheduled,
                    std::chrono::seconds{0},
                    boost::none,
                    boost::none,
                    boost::none
            },
            PublicRequest{
                    tx,
                    get_public_request_type(tx),
                    object_id,
                    object_type,
                    registrar,
                    "",
                    LibFred::PublicRequest::Status::opened,
                    LibFred::PublicRequest::OnStatusAction::processed,
                    std::chrono::seconds{-100},
                    2,
                    boost::none,
                    boost::none
            },
            PublicRequest{
                    tx,
                    get_public_request_type(tx),
                    object_id,
                    object_type,
                    registrar,
                    "no-reply@email.cz",
                    LibFred::PublicRequest::Status::opened,
                    LibFred::PublicRequest::OnStatusAction::failed,
                    std::chrono::seconds{-200},
                    boost::none,
                    boost::none,
                    boost::none
            },
            PublicRequest{
                    tx,
                    get_public_request_type(tx),
                    object_id,
                    object_type,
                    registrar,
                    "",
                    LibFred::PublicRequest::Status::resolved,
                    LibFred::PublicRequest::OnStatusAction::scheduled,
                    std::chrono::seconds{-300},
                    3,
                    std::chrono::seconds{-250},
                    boost::none
            },
            PublicRequest{
                    tx,
                    get_public_request_type(tx),
                    object_id,
                    object_type,
                    registrar,
                    "",
                    LibFred::PublicRequest::Status::resolved,
                    LibFred::PublicRequest::OnStatusAction::processed,
                    std::chrono::seconds{-400},
                    boost::none,
                    std::chrono::seconds{-350},
                    4
            },
            PublicRequest{
                    tx,
                    get_public_request_type(tx),
                    object_id,
                    object_type,
                    registrar,
                    "no-reply@email.cz",
                    LibFred::PublicRequest::Status::resolved,
                    LibFred::PublicRequest::OnStatusAction::failed,
                    std::chrono::seconds{-500},
                    5,
                    std::chrono::seconds{-450},
                    6
            },
            PublicRequest{
                    tx,
                    get_public_request_type(tx),
                    object_id,
                    object_type,
                    registrar,
                    "",
                    LibFred::PublicRequest::Status::invalidated,
                    LibFred::PublicRequest::OnStatusAction::scheduled,
                    std::chrono::seconds{0},
                    boost::none,
                    boost::none,
                    boost::none
            },
            PublicRequest{
                    tx,
                    get_public_request_type(tx),
                    object_id,
                    object_type,
                    registrar,
                    "",
                    LibFred::PublicRequest::Status::invalidated,
                    LibFred::PublicRequest::OnStatusAction::processed,
                    std::chrono::seconds{-600},
                    7,
                    std::chrono::seconds{-550},
                    8
            },
            PublicRequest{
                    tx,
                    get_public_request_type(tx),
                    object_id,
                    object_type,
                    registrar,
                    "no-reply@email.cz",
                    LibFred::PublicRequest::Status::invalidated,
                    LibFred::PublicRequest::OnStatusAction::failed,
                    std::chrono::seconds{-700},
                    boost::none,
                    std::chrono::seconds{-650},
                    9
            }};
}

boost::uuids::uuid make_uuid()
{
    static boost::uuids::uuid::value_type data = 0;
    return boost::uuids::uuid{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, data++}};
}

unsigned long long make_id()
{
    static unsigned long long id = 2'000'000'000;
    return id++;
}

}//namespace Test::{anonymous}

CreateDbTemplate::CreateDbTemplate()
{
    check_dbname_length(get_original_db_name());
    check_dbname_length(get_db_template_name());
    copy_db(get_original_db_name(), get_db_template_name(), Cfg::get_admin_connection());
}

CreateDbTemplate::~CreateDbTemplate()
{
    // restore original db for the last time
    copy_db(get_db_template_name(), get_original_db_name(), Cfg::get_admin_connection());
    drop_db(get_db_template_name(), Cfg::get_admin_connection());
}

Zone::Zone(const LibPg::PgRwTransaction& tx, const char* zone, bool idn_enabled)
    : data{get_non_enum_zone(tx, zone, idn_enabled)}
{ }

CzZone::CzZone(const LibPg::PgRwTransaction& tx)
    : Zone{tx, fqdn()}
{ }

const char* CzZone::fqdn() noexcept { return "cz"; }

std::string CzZone::fqdn(const char* subdomain)
{
    return make_fqdn(subdomain, fqdn());
}

std::string CzZone::fqdn(const std::string& subdomain)
{
    return make_fqdn(subdomain, fqdn());
}

InitDomainNameCheckers::InitDomainNameCheckers(const LibPg::PgRwTransaction& tx)
{
    exec(tx, "SAVEPOINT init_domain_name_checkers");
    try
    {
        LibFred::Domain::insert_domain_name_checker_name_into_database(tx, "dncheck_no_consecutive_hyphens", "forbid consecutive hyphens");
        exec(tx, "RELEASE SAVEPOINT init_domain_name_checkers");
    }
    catch (...)
    {
        exec(tx, "ROLLBACK TO SAVEPOINT init_domain_name_checkers");
    }
    exec(tx, "SAVEPOINT init_domain_name_checkers");
    try
    {
        LibFred::Domain::insert_domain_name_checker_name_into_database(tx, "dncheck_single_digit_labels_only", "enforces single digit labels (for enum domains)");
        exec(tx, "RELEASE SAVEPOINT init_domain_name_checkers");
    }
    catch (...)
    {
        exec(tx, "ROLLBACK TO SAVEPOINT init_domain_name_checkers");
    }
}

Registrar::Registrar(const LibPg::PgRwTransaction& tx, LibFred::CreateRegistrar create)
    : data{make_registrar(tx, create)}
{ }

Registrar::Registrar(LibFred::InfoRegistrarData data)
    : data{std::move(data)}
{ }

Contact::Contact(const LibPg::PgRwTransaction& tx, LibFred::CreateContact create)
    : data{make_contact(tx, create)}
{ }

Contact::Contact(LibFred::InfoContactData data)
    : data{std::move(data)}
{ }

Domain::Domain(const LibPg::PgRwTransaction& tx, LibFred::CreateDomain create)
    : data{make_domain(tx, create)}
{ }

Domain::Domain(LibFred::InfoDomainData data)
    : data{std::move(data)}
{ }

Keyset::Keyset(const LibPg::PgRwTransaction& tx, LibFred::CreateKeyset create)
    : data{make_keyset(tx, create)}
{ }

Keyset::Keyset(LibFred::InfoKeysetData data)
    : data{std::move(data)}
{ }

Nsset::Nsset(const LibPg::PgRwTransaction& tx, LibFred::CreateNsset create)
    : data{make_nsset(tx, create)}
{ }

Nsset::Nsset(LibFred::InfoNssetData data)
    : data{std::move(data)}
{ }

PublicRequest::PublicRequest(
        const LibPg::PgRwTransaction& tx,
        const std::string& type,
        unsigned long long object_id,
        LibFred::Object_Type::Enum object_type,
        const boost::optional<Test::Registrar>& registrar,
        const std::string& email_to_answer,
        LibFred::PublicRequest::Status::Enum status,
        LibFred::PublicRequest::OnStatusAction::Enum on_status_action,
        std::chrono::seconds create_time_shift,
        const boost::optional<unsigned long long>& create_request_id,
        const boost::optional<std::chrono::seconds>& resolve_time_shift,
        const boost::optional<unsigned long long>& resolve_request_id)
    : data{make_public_request(
            tx,
            type,
            object_id,
            object_type,
            registrar,
            email_to_answer,
            status,
            on_status_action,
            create_time_shift,
            create_request_id,
            resolve_time_shift,
            resolve_request_id)}
{ }

PublicRequest::PublicRequest(LibFred::PublicRequest::PublicRequestData data)
    : data{std::move(data)}
{ }

DbData::DbData(const LibPg::PgRwTransaction& tx)
    : no_registrar{make_no_registrar({make_id(), make_uuid(), "REG-TEST-NO"})},
      registrar{tx, Setter::registrar(
            LibFred::CreateRegistrar{"REG-TEST",
                    "-", "-", {"-"}, "-", "-", "-", "-", "-", "-"})},
      no_contact{make_no_contact({make_id(), make_uuid(), "CONTACT-NO"})},
      contact_0{tx, Setter::contact(LibFred::CreateContact{"CONTACT-0", registrar.data.handle}, 0)},
      contact_1{tx, Setter::contact(LibFred::CreateContact{"CONTACT-1", registrar.data.handle}, 1)},
      contact_2{tx, Setter::contact(LibFred::CreateContact{"CONTACT-2", registrar.data.handle}, 2)},
      no_domain{make_no_domain({make_id(), make_uuid(), "domain-no.cz"})},
      domain_0{tx, Setter::domain(LibFred::CreateDomain{"domain-0.cz", registrar.data.handle, contact_0.data.handle}, 0)},
      domain_1{tx, Setter::domain(LibFred::CreateDomain{"domain-1.cz", registrar.data.handle, contact_0.data.handle}, 1)},
      domain_2{tx, Setter::domain(LibFred::CreateDomain{"domain-2.cz", registrar.data.handle, contact_0.data.handle}, 2)},
      no_keyset{make_no_keyset({make_id(), make_uuid(), "KEYSET-NO"})},
      keyset_0{tx, Setter::keyset(LibFred::CreateKeyset{"KEYSET-0", registrar.data.handle}, 0)},
      keyset_1{tx, Setter::keyset(LibFred::CreateKeyset{"KEYSET-1", registrar.data.handle}, 1)},
      keyset_2{tx, Setter::keyset(LibFred::CreateKeyset{"KEYSET-2", registrar.data.handle}, 2)},
      no_nsset{make_no_nsset({make_id(), make_uuid(), "NSSET-NO"})},
      nsset_0{tx, Setter::nsset(LibFred::CreateNsset{"NSSET-0", registrar.data.handle}, 0)},
      nsset_1{tx, Setter::nsset(LibFred::CreateNsset{"NSSET-1", registrar.data.handle}, 1)},
      nsset_2{tx, Setter::nsset(LibFred::CreateNsset{"NSSET-2", registrar.data.handle}, 2)},
      object_c{tx, Setter::contact(LibFred::CreateContact{"OBJECT", registrar.data.handle}, 3)},
      object_k{tx, Setter::keyset(LibFred::CreateKeyset{"OBJECT", registrar.data.handle}, 3)},
      object_n{tx, Setter::nsset(LibFred::CreateNsset{"OBJECT", registrar.data.handle}, 3)},
      no_public_request_c{make_no_public_request(no_contact, no_registrar)},
      public_request_c1{{
            PublicRequest{
                    tx,
                    "authinfo_auto_pif",
                    contact_1.data.id,
                    LibFred::Object_Type::contact,
                    registrar,
                    "no-reply@email.cz",
                    LibFred::PublicRequest::Status::opened,
                    LibFred::PublicRequest::OnStatusAction::scheduled,
                    std::chrono::seconds{0},
                    1,
                    boost::none,
                    boost::none
            }}},
      public_request_c2{make_public_requests(
            tx,
            contact_2.data.id,
            LibFred::Object_Type::contact,
            registrar)},
      no_public_request_d{make_no_public_request(no_domain, no_registrar)},
      public_request_d1{{
            PublicRequest{
                    tx,
                    "personalinfo_auto_pif",
                    domain_1.data.id,
                    LibFred::Object_Type::domain,
                    registrar,
                    "no-reply@email.cz",
                    LibFred::PublicRequest::Status::opened,
                    LibFred::PublicRequest::OnStatusAction::scheduled,
                    std::chrono::seconds{0},
                    1,
                    boost::none,
                    boost::none
            }}},
      public_request_d2{make_public_requests(
            tx,
            domain_2.data.id,
            LibFred::Object_Type::domain,
            registrar)},
      no_public_request_k{make_no_public_request(no_keyset, no_registrar)},
      public_request_k1{{
            PublicRequest{
                    tx,
                    "authinfo_government_pif",
                    keyset_1.data.id,
                    LibFred::Object_Type::keyset,
                    registrar,
                    "no-reply@email.cz",
                    LibFred::PublicRequest::Status::opened,
                    LibFred::PublicRequest::OnStatusAction::scheduled,
                    std::chrono::seconds{0},
                    1,
                    boost::none,
                    boost::none
            }}},
      public_request_k2{make_public_requests(
            tx,
            keyset_2.data.id,
            LibFred::Object_Type::keyset,
            registrar)},
      no_public_request_n{make_no_public_request(no_nsset, no_registrar)},
      public_request_n1{{
            PublicRequest{
                    tx,
                    "block_changes_post_pif",
                    nsset_1.data.id,
                    LibFred::Object_Type::nsset,
                    registrar,
                    "no-reply@email.cz",
                    LibFred::PublicRequest::Status::opened,
                    LibFred::PublicRequest::OnStatusAction::scheduled,
                    std::chrono::seconds{0},
                    1,
                    boost::none,
                    boost::none
            }}},
      public_request_n2{make_public_requests(
            tx,
            nsset_2.data.id,
            LibFred::Object_Type::nsset,
            registrar)},
      public_request_c{{
            PublicRequest{
                    tx,
                    "personalinfo_auto_pif",
                    object_c.data.id,
                    LibFred::Object_Type::contact,
                    registrar,
                    "no-reply@email.cz",
                    LibFred::PublicRequest::Status::opened,
                    LibFred::PublicRequest::OnStatusAction::scheduled,
                    std::chrono::seconds{0},
                    1,
                    boost::none,
                    boost::none
            }}},
      public_request_k{{
            PublicRequest{
                    tx,
                    "block_changes_post_pif",
                    object_k.data.id,
                    LibFred::Object_Type::keyset,
                    registrar,
                    "no-reply@email.cz",
                    LibFred::PublicRequest::Status::opened,
                    LibFred::PublicRequest::OnStatusAction::scheduled,
                    std::chrono::seconds{0},
                    1,
                    boost::none,
                    boost::none
            }}},
      public_request_n{{
            PublicRequest{
                    tx,
                    "unblock_changes_email_pif",
                    object_n.data.id,
                    LibFred::Object_Type::nsset,
                    registrar,
                    "no-reply@email.cz",
                    LibFred::PublicRequest::Status::opened,
                    LibFred::PublicRequest::OnStatusAction::scheduled,
                    std::chrono::seconds{0},
                    1,
                    boost::none,
                    boost::none
            }}}
{ }

const DbData* db_data_ptr = nullptr;

void DbData::init(const LibPg::PgRwTransaction& tx)
{
    if (db_data_ptr != nullptr)
    {
        struct PrerequisitesNotMet : std::exception
        {
            const char* what() const noexcept override { return "can only be called once at most"; }
        };
        throw PrerequisitesNotMet{};
    }
    db_data_ptr = new DbData{tx};
}

void DbData::load(const LibPg::PgRwTransaction& tx)
{
    init(tx);
}

const DbData& DbData::get()
{
    if (db_data_ptr == nullptr)
    {
        struct PrerequisitesNotMet : std::exception
        {
            const char* what() const noexcept override { return "must be initialized first"; }
        };
        throw PrerequisitesNotMet{};
    }
    return *db_data_ptr;
}

HasDbData::HasDbData()
    : db_data{DbData::get()}
{ }

}//namespace Test

using namespace Test;

template <typename Level>
LibPg::PgRoTransaction Test::make_ro_transaction(
        LibPg::PgTransaction::IsolationLevel<Level> level)
{
    return LibPg::PgRoTransaction{Cfg::get_user_connection(), level};
}

LibPg::PgRwTransaction Test::make_rw_transaction()
{
    return LibPg::PgRwTransaction{Cfg::get_user_connection()};
}

template LibPg::PgRoTransaction Test::make_ro_transaction<>(
        LibPg::PgTransaction::IsolationLevel<SessionDefault>);
template LibPg::PgRoTransaction Test::make_ro_transaction<SerializableDeferrable>(
        LibPg::PgTransaction::IsolationLevel<SerializableDeferrable>);

std::string Test::testcase_name()
{
    return get_unit_test_path(boost::unit_test::framework::current_test_case());
}

Registrar Test::make_no_registrar(const LibFred::PublicRequest::PublicRequestData::RegistrarDataLight& data)
{
    LibFred::InfoRegistrarData registrar;
    registrar.id = data.id;
    registrar.handle = data.handle;
    return Registrar{std::move(registrar)};
}

Contact Test::make_no_contact(const LibFred::PublicRequest::PublicRequestData::ContactDataLight& data)
{
    LibFred::InfoContactData contact;
    contact.id = data.id;
    contact.uuid = Util::make_strong<LibFred::RegistrableObject::Contact::ContactUuid>(data.uuid);
    contact.handle = data.handle;
    return Contact{std::move(contact)};
}

Domain Test::make_no_domain(const LibFred::PublicRequest::PublicRequestData::DomainDataLight& data)
{
    LibFred::InfoDomainData domain;
    domain.id = data.id;
    domain.uuid = Util::make_strong<LibFred::RegistrableObject::Domain::DomainUuid>(data.uuid);
    domain.fqdn = data.fqdn;
    return Domain{std::move(domain)};
}

Keyset Test::make_no_keyset(const LibFred::PublicRequest::PublicRequestData::KeysetDataLight& data)
{
    LibFred::InfoKeysetData keyset;
    keyset.id = data.id;
    keyset.uuid = Util::make_strong<LibFred::RegistrableObject::Keyset::KeysetUuid>(data.uuid);
    keyset.handle = data.handle;
    return Keyset{std::move(keyset)};
}

Nsset Test::make_no_nsset(const LibFred::PublicRequest::PublicRequestData::NssetDataLight& data)
{
    LibFred::InfoNssetData nsset;
    nsset.id = data.id;
    nsset.uuid = Util::make_strong<LibFred::RegistrableObject::Nsset::NssetUuid>(data.uuid);
    nsset.handle = data.handle;
    return Nsset{std::move(nsset)};
}

PublicRequest Test::make_no_public_request(const Contact& contact, const boost::optional<Registrar>& registrar)
{
    LibFred::PublicRequest::PublicRequestData public_request;
    public_request.id = make_id();
    public_request.uuid = make_uuid();
    public_request.object_data = LibFred::PublicRequest::PublicRequestData::ContactDataLight{
            contact.data.id,
            get_raw_value_from(contact.data.uuid),
            contact.data.handle};
    if (registrar == boost::none)
    {
        public_request.registrar_data = boost::none;    
    }
    else
    {
        public_request.registrar_data = LibFred::PublicRequest::PublicRequestData::RegistrarDataLight{
            registrar->data.id,
            {},
            registrar->data.handle};
    }
    return PublicRequest{std::move(public_request)};
}

PublicRequest Test::make_no_public_request(const Domain& domain, const boost::optional<Registrar>& registrar)
{
    LibFred::PublicRequest::PublicRequestData public_request;
    public_request.id = make_id();
    public_request.uuid = make_uuid();
    public_request.object_data = LibFred::PublicRequest::PublicRequestData::DomainDataLight{
            domain.data.id,
            get_raw_value_from(domain.data.uuid),
            domain.data.fqdn};
    if (registrar == boost::none)
    {
        public_request.registrar_data = boost::none;    
    }
    else
    {
        public_request.registrar_data = LibFred::PublicRequest::PublicRequestData::RegistrarDataLight{
            registrar->data.id,
            {},
            registrar->data.handle};
    }
    return PublicRequest{std::move(public_request)};
}

PublicRequest Test::make_no_public_request(const Keyset& keyset, const boost::optional<Registrar>& registrar)
{
    LibFred::PublicRequest::PublicRequestData public_request;
    public_request.id = make_id();
    public_request.uuid = make_uuid();
    public_request.object_data = LibFred::PublicRequest::PublicRequestData::KeysetDataLight{
            keyset.data.id,
            get_raw_value_from(keyset.data.uuid),
            keyset.data.handle};
    if (registrar == boost::none)
    {
        public_request.registrar_data = boost::none;    
    }
    else
    {
        public_request.registrar_data = LibFred::PublicRequest::PublicRequestData::RegistrarDataLight{
            registrar->data.id,
            {},
            registrar->data.handle};
    }
    return PublicRequest{std::move(public_request)};
}

PublicRequest Test::make_no_public_request(const Nsset& nsset, const boost::optional<Registrar>& registrar)
{
    LibFred::PublicRequest::PublicRequestData public_request;
    public_request.id = make_id();
    public_request.uuid = make_uuid();
    public_request.object_data = LibFred::PublicRequest::PublicRequestData::NssetDataLight{
            nsset.data.id,
            get_raw_value_from(nsset.data.uuid),
            nsset.data.handle};
    if (registrar == boost::none)
    {
        public_request.registrar_data = boost::none;    
    }
    else
    {
        public_request.registrar_data = LibFred::PublicRequest::PublicRequestData::RegistrarDataLight{
            registrar->data.id,
            {},
            registrar->data.handle};
    }
    return PublicRequest{std::move(public_request)};
}

using namespace Test::Setter;

LibFred::CreateRegistrar Test::Setter::registrar(LibFred::CreateRegistrar create, int index)
{
    set_registrar(create, !is_system_registrar, index);
    return create;
}

LibFred::CreateContact Test::Setter::contact(LibFred::CreateContact create, int index)
{
    set_contact(create, index);
    return create;
}

LibFred::CreateDomain Test::Setter::domain(LibFred::CreateDomain create, int index)
{
    set_domain(create, index);
    return create;
}

LibFred::CreateKeyset Test::Setter::keyset(LibFred::CreateKeyset create, int index)
{
    set_keyset(create, index);
    return create;
}

LibFred::CreateNsset Test::Setter::nsset(LibFred::CreateNsset create, int index)
{
    set_nsset(create, index);
    return create;
}
