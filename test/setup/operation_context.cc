/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "test/setup/operation_context.hh"
#include "test/setup/cfg.hh"
#include "test/setup/has_fresh_database.hh"

#include "liblog/liblog.hh"

#include <exception>
#include <utility>

namespace Test {

namespace {

std::unique_ptr<LibPg::PgConnection> connection_ptr = nullptr;

LibPg::PgConnection get_connection()
{
    if (connection_ptr.get() == nullptr)
    {
        connection_ptr = std::make_unique<LibPg::PgConnection>(Cfg::get_user_connection());
    }
    else
    {
        LIBLOG_DEBUG("new connection establishment saved");
    }
    auto connection = LibPg::PgConnection{std::move(*(connection_ptr.get()))};
    connection_ptr.reset();
    return connection;
}

void store_connection(LibPg::PgConnection connection)
{
    connection_ptr = std::make_unique<LibPg::PgConnection>(std::move(connection));
}

}//namespace Test::{anonymous}

template <typename Transaction>
OperationContext<Transaction>::TransactionContext::TransactionContext(Transaction tx)
    : tx{std::move(tx)},
      ctx{this->tx}
{ }

template <typename Transaction>
OperationContext<Transaction>::OperationContext()
    : OperationContext{Transaction{get_connection()}}
{ }

template <typename Transaction>
OperationContext<Transaction>::OperationContext(Transaction tx)
    : ptr_{std::make_unique<TransactionContext>(std::move(tx))}
{ }

template <typename Transaction>
OperationContext<Transaction>::~OperationContext()
{
    if (ptr_.get() != nullptr)
    {
        try
        {
            store_connection(rollback(std::move(ptr_->tx)));
            ptr_.reset();
        }
        catch (const std::exception& e)
        {
            try { LIBLOG_ERROR("destroying of operation context failed: {}", e.what()); } catch (...) { }
        }
        catch (...)
        {
            try { LIBLOG_ERROR("destroying of operation context failed by an unknown exception"); } catch (...) { }
        }
    }
}

template <typename Transaction>
OperationContext<Transaction>::operator const LibFred::OperationContext& () const
{
    return static_cast<const LibFred::OperationContext&>(ptr_->ctx);
}

template <typename Transaction>
OperationContext<Transaction>::operator const Transaction& () const
{
    return static_cast<const Transaction&>(ptr_->tx);
}

template <typename Transaction>
OperationContext<Transaction>::operator const LibPg::PgTransaction& () const
{
    return static_cast<const LibPg::PgTransaction&>(ptr_->tx);
}

template class OperationContext<LibPg::PgRoTransaction>;
template class OperationContext<LibPg::PgRwTransaction>;

}//namespace Test

template <typename Transaction>
LibPg::PgConnection Test::commit(OperationContext<Transaction>&& ctx)
{
    auto connection = commit(std::move(ctx.ptr_->tx));
    ctx.ptr_.reset();
    return connection;
}

template <typename Transaction>
void Test::rollback(OperationContext<Transaction>&& ctx)
{
    store_connection(rollback(std::move(ctx.ptr_->tx)));
    ctx.ptr_.reset();
}

template LibPg::PgConnection Test::commit(RoOperationContext&&);
template LibPg::PgConnection Test::commit(RwOperationContext&&);

template void Test::rollback(RoOperationContext&&);
template void Test::rollback(RwOperationContext&&);

void Test::reset_connection()
{
    Test::connection_ptr.reset();
}

bool Test::is_rollbacked()
{
    return connection_ptr.get() != nullptr;
}
