/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef FIXTURES_HH_98D905D0B566DAD1693B8BD1DA9E17E4//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define FIXTURES_HH_98D905D0B566DAD1693B8BD1DA9E17E4

#include "test/setup/has_fresh_database.hh"
#include "test/setup/operation_context.hh"

#include "libfred/public_request/public_request_data.hh"
#include "libfred/public_request/public_request_on_status_action.hh"
#include "libfred/public_request/public_request_status.hh"
#include "libfred/registrable_object/contact/create_contact.hh"
#include "libfred/registrable_object/contact/info_contact_data.hh"
#include "libfred/registrable_object/domain/create_domain.hh"
#include "libfred/registrable_object/domain/info_domain_data.hh"
#include "libfred/registrable_object/keyset/create_keyset.hh"
#include "libfred/registrable_object/keyset/info_keyset_data.hh"
#include "libfred/registrable_object/nsset/create_nsset.hh"
#include "libfred/registrable_object/nsset/info_nsset_data.hh"
#include "libfred/registrar/create_registrar.hh"
#include "libfred/registrar/info_registrar_data.hh"
#include "libfred/zone/info_zone_data.hh"

#include "liblog/liblog.hh"

#include "libpg/pg_ro_transaction.hh"
#include "libpg/pg_rw_transaction.hh"

#include <boost/optional.hpp>

#include <array>
#include <string>

namespace Test {

struct CreateDbTemplate
{
    explicit CreateDbTemplate();
    ~CreateDbTemplate();
};

std::string testcase_name();

template <typename ...Fs>
struct LogContext : LibLog::SetContext, Fs...
{
    LogContext() : LibLog::SetContext{Test::testcase_name()}, Fs{}... { }
};

struct HasRoOperationContext
{
    RoOperationContext ctx;
};

struct HasRwOperationContext
{
    RwOperationContext ctx;
};

class DbData;

struct HasDbData
{
    explicit HasDbData();
    const DbData& db_data;
};

template <typename ...SubFixtures>
struct RoTxFixture
    : LibLog::SetContext,
      HasFreshDatabase,
      HasRoOperationContext,
      HasDbData,
      SubFixtures...
{
    RoTxFixture()
        : LibLog::SetContext{testcase_name()},
          HasFreshDatabase{},
          HasRoOperationContext{},
          HasDbData{},
          SubFixtures{ctx}...
    { }
};

template <typename ...SubFixtures>
struct RwTxFixture
    : LibLog::SetContext,
      HasFreshDatabase,
      HasRwOperationContext,
      HasDbData,
      SubFixtures...
{
    RwTxFixture()
        : LibLog::SetContext{testcase_name()},
          HasFreshDatabase{},
          HasRwOperationContext{},
          HasDbData{},
          SubFixtures{ctx}...
    { }
};

using SessionDefault = LibPg::PgTransaction::SessionDefault;
using SerializableDeferrable = LibPg::PgTransaction::Serializable<LibPg::PgTransaction::Deferrable>;

template <typename Level = SessionDefault>
LibPg::PgRoTransaction make_ro_transaction(LibPg::PgTransaction::IsolationLevel<Level> level = LibPg::PgTransaction::session_default);
LibPg::PgRwTransaction make_rw_transaction();

extern template LibPg::PgRoTransaction make_ro_transaction<>(LibPg::PgTransaction::IsolationLevel<SessionDefault>);
extern template LibPg::PgRoTransaction make_ro_transaction<SerializableDeferrable>(LibPg::PgTransaction::IsolationLevel<SerializableDeferrable>);

struct Zone
{
    explicit Zone(
            const LibPg::PgRwTransaction& tx,
            const char* zone,
            bool idn_enabled = false);
    LibFred::Zone::NonEnumZone data;
};

struct CzZone : Zone
{
    explicit CzZone(const LibPg::PgRwTransaction& tx);
    static const char* fqdn() noexcept;
    static std::string fqdn(const char* subdomain);
    static std::string fqdn(const std::string& subdomain);
};

struct InitDomainNameCheckers
{
    explicit InitDomainNameCheckers(const LibPg::PgRwTransaction& tx);
};

struct Registrar
{
    explicit Registrar(const LibPg::PgRwTransaction& tx, LibFred::CreateRegistrar create);
    explicit Registrar(LibFred::InfoRegistrarData data);
    LibFred::InfoRegistrarData data;
    friend Registrar make_no_registrar(const LibFred::PublicRequest::PublicRequestData::RegistrarDataLight&);
};

Registrar make_no_registrar(const LibFred::PublicRequest::PublicRequestData::RegistrarDataLight&);

struct Contact
{
    explicit Contact(const LibPg::PgRwTransaction& tx, LibFred::CreateContact create);
    explicit Contact(LibFred::InfoContactData data);
    LibFred::InfoContactData data;
    friend Contact make_no_contact(const LibFred::PublicRequest::PublicRequestData::ContactDataLight&);
};

Contact make_no_contact(const LibFred::PublicRequest::PublicRequestData::ContactDataLight&);

struct Domain
{
    explicit Domain(const LibPg::PgRwTransaction& tx, LibFred::CreateDomain create);
    explicit Domain(LibFred::InfoDomainData data);
    LibFred::InfoDomainData data;
    friend Domain make_no_domain(const LibFred::PublicRequest::PublicRequestData::DomainDataLight&);
};

Domain make_no_domain(const LibFred::PublicRequest::PublicRequestData::DomainDataLight&);

struct Keyset
{
    explicit Keyset(const LibPg::PgRwTransaction& tx, LibFred::CreateKeyset create);
    explicit Keyset(LibFred::InfoKeysetData data);
    LibFred::InfoKeysetData data;
    friend Keyset make_no_keyset(const LibFred::PublicRequest::PublicRequestData::KeysetDataLight&);
};

Keyset make_no_keyset(const LibFred::PublicRequest::PublicRequestData::KeysetDataLight&);

struct Nsset
{
    explicit Nsset(const LibPg::PgRwTransaction& tx, LibFred::CreateNsset create);
    explicit Nsset(LibFred::InfoNssetData data);
    LibFred::InfoNssetData data;
    friend Nsset make_no_nsset(const LibFred::PublicRequest::PublicRequestData::NssetDataLight&);
};

Nsset make_no_nsset(const LibFred::PublicRequest::PublicRequestData::NssetDataLight&);

struct PublicRequest
{
    explicit PublicRequest(
            const LibPg::PgRwTransaction& tx,
            const std::string& type,
            unsigned long long object_id,
            LibFred::Object_Type::Enum object_type,
            const boost::optional<Registrar>& registrar,
            const std::string& email_to_answer,
            LibFred::PublicRequest::Status::Enum status,
            LibFred::PublicRequest::OnStatusAction::Enum on_status_action,
            std::chrono::seconds create_time_shift,
            const boost::optional<unsigned long long>& create_request_id,
            const boost::optional<std::chrono::seconds>& resolve_time_shift,
            const boost::optional<unsigned long long>& resolve_request_id);
    explicit PublicRequest(LibFred::PublicRequest::PublicRequestData data);
    LibFred::PublicRequest::PublicRequestData data;
    friend PublicRequest make_no_public_request(const Contact&, const boost::optional<Registrar>&);
    friend PublicRequest make_no_public_request(const Domain&, const boost::optional<Registrar>&);
    friend PublicRequest make_no_public_request(const Keyset&, const boost::optional<Registrar>&);
    friend PublicRequest make_no_public_request(const Nsset&, const boost::optional<Registrar>&);
};

PublicRequest make_no_public_request(const Contact&, const boost::optional<Registrar>&);
PublicRequest make_no_public_request(const Domain&, const boost::optional<Registrar>&);
PublicRequest make_no_public_request(const Keyset&, const boost::optional<Registrar>&);
PublicRequest make_no_public_request(const Nsset&, const boost::optional<Registrar>&);

namespace Setter {

LibFred::CreateRegistrar registrar(LibFred::CreateRegistrar create, int index = 0);
LibFred::CreateContact contact(LibFred::CreateContact create, int index = 0);
LibFred::CreateDomain domain(LibFred::CreateDomain create, int index = 0);
LibFred::CreateKeyset keyset(LibFred::CreateKeyset create, int index = 0);
LibFred::CreateNsset nsset(LibFred::CreateNsset create, int index = 0);

}//namespace Test::Setter

class DbData
{
public:
    static void init(const LibPg::PgRwTransaction& tx);
    static void load(const LibPg::PgRwTransaction& tx);
    static const DbData& get();
    Registrar no_registrar;
    Registrar registrar;
    Contact no_contact;
    Contact contact_0;
    Contact contact_1;
    Contact contact_2;
    Domain no_domain;
    Domain domain_0;
    Domain domain_1;
    Domain domain_2;
    Keyset no_keyset;
    Keyset keyset_0;
    Keyset keyset_1;
    Keyset keyset_2;
    Nsset no_nsset;
    Nsset nsset_0;
    Nsset nsset_1;
    Nsset nsset_2;
    Contact object_c;
    Keyset object_k;
    Nsset object_n;
    std::array<PublicRequest, 1> no_public_request_c;
    std::array<PublicRequest, 1> public_request_c1;
    std::array<PublicRequest, 9> public_request_c2;
    std::array<PublicRequest, 1> no_public_request_d;
    std::array<PublicRequest, 1> public_request_d1;
    std::array<PublicRequest, 9> public_request_d2;
    std::array<PublicRequest, 1> no_public_request_k;
    std::array<PublicRequest, 1> public_request_k1;
    std::array<PublicRequest, 9> public_request_k2;
    std::array<PublicRequest, 1> no_public_request_n;
    std::array<PublicRequest, 1> public_request_n1;
    std::array<PublicRequest, 9> public_request_n2;
    std::array<PublicRequest, 1> public_request_c;
    std::array<PublicRequest, 1> public_request_k;
    std::array<PublicRequest, 1> public_request_n;
private:
    explicit DbData(const LibPg::PgRwTransaction& tx);
};

}//namespace Test

#endif//FIXTURES_HH_98D905D0B566DAD1693B8BD1DA9E17E4
