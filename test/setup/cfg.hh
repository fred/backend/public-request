/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CFG_HH_BDE921F63E94E6B0D406E82C0560822A//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CFG_HH_BDE921F63E94E6B0D406E82C0560822A

/*
 * config file example:

#Logging options
[log]
device       = file
device       = console
grpc_library = yes

#Logging into file options
[log.file]
file_name    = public_request.log
min_severity = debug

#Logging into console options
[log.console]
min_severity = trace

#Regular user database access options
[database]
host     = localhost
port     = 11112
dbname   = fred
user     = fred
password = password
application_name = test-public-request

#Privileged user database access options
[admin_database]
host     = localhost
port     = 11112
dbname   = fred
user     = postgres
password = password
application_name = admin:test-public-request
 */

#include "liblog/level.hh"
#include "liblog/sink/console_sink_config.hh"

#include "libpg/pg_connection.hh"

#include <boost/asio/ip/address.hpp>
#include <boost/optional.hpp>

#include <chrono>
#include <exception>
#include <functional>
#include <string>

namespace Test {
namespace Cfg {

struct AllDone : std::exception { };

struct Exception : std::exception { };

struct UnknownOption : Exception { };

struct MissingOption : Exception { };

class Options
{
public:
    Options() = delete;
    Options(const Options&) = delete;
    Options(Options&&) = delete;
    Options& operator=(const Options&) = delete;
    Options& operator=(Options&&) = delete;

    static const Options& get();
    static const Options& init(
            int argc,
            const char* const* argv,
            std::function<void(const std::string&)> on_unrecognized);

    struct Generic
    {
        boost::optional<std::string> config_file_name;
        int parallel;
        bool invoke_database_setup;
    } generic;
    template <typename>
    struct DatabaseOptions
    {
        boost::optional<std::string> host;
        boost::optional<boost::asio::ip::address> host_addr;
        boost::optional<::in_port_t> port;
        boost::optional<std::string> user;
        boost::optional<std::string> dbname;
        boost::optional<std::string> password;
        boost::optional<std::chrono::seconds> connect_timeout;
        boost::optional<std::string> application_name;
    };
    using Database = DatabaseOptions<struct RegularUser_>;
    Database database;
    using AdminDatabase = DatabaseOptions<struct PrivilegedUser_>;
    AdminDatabase admin_database;
    struct Log
    {
        struct Console
        {
            LibLog::Level min_severity;
            LibLog::Sink::ConsoleSinkConfig::OutputStream output_stream;
            LibLog::ColorMode color_mode;
        };
        struct File
        {
            std::string file_name;
            LibLog::Level min_severity;
        };
        struct Syslog
        {
            std::string ident;
            LibLog::Level min_severity;
            int options;
            int facility;
        };
        boost::optional<Console> console;
        boost::optional<File> file;
        boost::optional<Syslog> syslog;
        bool log_grpc_library_events;
    } log;
private:
    Options(int argc,
            const char* const* argv,
            std::function<void(const std::string&)> on_unrecognized);
};

class DatabaseAdministrator
{
public:
    explicit DatabaseAdministrator(
            std::string test_database_name,
            std::function<void()> database_setup_procedure = nullptr);
    DatabaseAdministrator(DatabaseAdministrator&&) = default;
    DatabaseAdministrator(const DatabaseAdministrator&) = delete;
    ~DatabaseAdministrator();
    DatabaseAdministrator& operator=(DatabaseAdministrator&&) = default;
    DatabaseAdministrator& operator=(const DatabaseAdministrator&) = delete;
    std::function<void()> get_restore_test_database_procedure();
private:
    class BackupDatabase
    {
    public:
        BackupDatabase() = default;
        BackupDatabase(BackupDatabase&&) = default;
        BackupDatabase(const BackupDatabase&) = delete;
        ~BackupDatabase();
        BackupDatabase& operator=(BackupDatabase&&) = default;
        BackupDatabase& operator=(const BackupDatabase&) = delete;
        void backup(std::string origin, std::string backup);
        bool has_backup() const noexcept;
        void recover();
        void reset();
    private:
        std::string origin_;
        std::string backup_;
    };
    BackupDatabase origin_;
    BackupDatabase test_;
    std::string test_database_name_;
};

DatabaseAdministrator handle_command_line_args(
        std::function<void()> database_setup_procedure = []() { });

LibPg::PgConnection get_admin_connection();
LibPg::PgConnection get_user_connection();

}//namespace Test::Cfg
}//namespace Test

#endif//CFG_HH_BDE921F63E94E6B0D406E82C0560822A
