/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ARGUMENTS_HH_888B8533D1675A8ACD3FD06DE42D856F//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define ARGUMENTS_HH_888B8533D1675A8ACD3FD06DE42D856F

#include <cstddef>
#include <string>
#include <vector>

namespace Test {
namespace Util {

class ArgumentsFactory;

class Arguments
{
public:
    explicit Arguments(
            const char* cmd,
            const char* const* arg_begin,
            const char* const* arg_end);
    explicit Arguments(
            const char* cmd,
            std::vector<std::string>::const_iterator arg_begin,
            std::vector<std::string>::const_iterator arg_end);
    ~Arguments();
    Arguments(Arguments&&) = default;
    Arguments& operator=(Arguments&&) = default;
    std::size_t count() const noexcept;
    char** data() noexcept;
private:
    explicit Arguments() = default;
    std::vector<char*> argv_;
    friend class ArgumentsFactory;
};

class ArgumentsFactory
{
public:
    explicit ArgumentsFactory(std::ptrdiff_t reserved_capacity = 0);
    ArgumentsFactory& push_back(const char* argument);
    ArgumentsFactory& push_back(const std::string& argument);
    ArgumentsFactory& push_back_non_empty(const char* option_name, const std::string& argument);
    Arguments finish() &&;
private:
    Arguments args_;
};

}//namespace Test::Util
}//namespace Test

#endif//ARGUMENTS_HH_888B8533D1675A8ACD3FD06DE42D856F
