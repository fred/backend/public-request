find_package(Boost 1.53.0
    COMPONENTS
    unit_test_framework
    REQUIRED)

add_library(public-request-test-setup STATIC
    setup/arguments.cc
    setup/cfg.cc
    setup/fixtures.cc
    setup/has_fresh_database.cc
    setup/operation_context.cc
    setup/run_in_background.cc
    setup/test_tree_list.cc)
add_library(PublicRequestTestSetup::library ALIAS public-request-test-setup)

target_include_directories(public-request-test-setup
    PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}/..
    PRIVATE
        ${CMAKE_CURRENT_BINARY_DIR}/..)

target_link_libraries(public-request-test-setup PUBLIC
    Boost::unit_test_framework
    Boost::program_options
    Fred::library
    LibLog::library
    LibPg::library
    gRPC::grpc++
PRIVATE
    FredPublicRequestCore::library)

set_target_properties(public-request-test-setup PROPERTIES
    VERSION ${PROJECT_VERSION}
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/lib"
    OUTPUT_NAME "public-request-test-setup")

add_executable(test-public-request
    src/main.cc
    src/test_lib.cc
    src/test_unwrapper.cc
    src/test_wrapper.cc)

set_target_properties(test-public-request PROPERTIES
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/bin")

target_link_libraries(test-public-request PRIVATE
    Fred::library
    FredPublicRequestCore::library
    Boost::unit_test_framework
    PublicRequestTestSetup::library)
include(GNUInstallDirs)

install(TARGETS test-public-request DESTINATION ${CMAKE_INSTALL_BINDIR})
