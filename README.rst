======================================
Backend for public requests management 
======================================

Public requests management server based on gRPC API for `FRED <https://fred.nic.cz/>`_ registry.
