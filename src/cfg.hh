/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CFG_HH_7814767E8B9D8F5CD208BE639BDD884E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CFG_HH_7814767E8B9D8F5CD208BE639BDD884E

/*
 * config file example:

#Logging options
[log]
device       = file
device       = console
grpc_library = yes

#Logging into file options
[log.file]
file_name = public_request.log
min_severity = debug

#Logging into console options
[log.console]
min_severity = trace

#Database options
[database]
host    = localhost
port    = 11112
dbname  = fred
user     = fred
password = password

#Server options
[public_request]
listen  = localhost:50051
service = PublicRequestInfo
service = PublicRequestProcessing
service = Diagnostics

 */

#include "liblog/level.hh"
#include "liblog/sink/console_sink_config.hh"

#include <boost/asio/ip/address.hpp>
#include <boost/optional.hpp>

#include <chrono>
#include <set>
#include <exception>
#include <string>

namespace Cfg {

struct AllDone : std::exception { };

struct Exception : std::exception { };

struct UnknownOption : Exception { };

struct MissingOption : Exception { };

class Options
{
public:
    Options() = delete;
    Options(const Options&) = delete;
    Options(Options&&) = delete;
    Options& operator=(const Options&) = delete;
    Options& operator=(Options&&) = delete;

    static const Options& get();
    static const Options& init(int argc, const char* const* argv);

    boost::optional<std::string> config_file_name;
    struct Database
    {
        boost::optional<std::string> host;
        boost::optional<boost::asio::ip::address> host_addr;
        boost::optional<::in_port_t> port;
        boost::optional<std::string> user;
        boost::optional<std::string> dbname;
        boost::optional<std::string> password;
        boost::optional<std::chrono::seconds> connect_timeout;
        boost::optional<std::string> application_name;
    } database;
    struct Log
    {
        struct Console
        {
            LibLog::Level min_severity;
            LibLog::Sink::ConsoleSinkConfig::OutputStream output_stream;
            LibLog::ColorMode color_mode;
        };
        struct File
        {
            std::string file_name;
            LibLog::Level min_severity;
        };
        struct Syslog
        {
            std::string ident;
            LibLog::Level min_severity;
            int options;
            int facility;
        };
        boost::optional<Console> console;
        boost::optional<File> file;
        boost::optional<Syslog> syslog;
        bool log_grpc_library_events;
    } log;
    struct Server
    {
        std::string listen_on;
        enum class Service
        {
            public_request_info,
            public_request_processing,
            diagnostics
        };
        std::set<Service> services;
    } public_request;
private:
    Options(int argc, const char* const* argv);
};

void handle_cli_args(int argc, char** argv);

}//namespace Cfg

#endif//CFG_HH_7814767E8B9D8F5CD208BE639BDD884E
