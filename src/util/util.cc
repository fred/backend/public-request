/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/util/util.hh"
#include "src/util/install_pg_log_layer.hh"

#include "src/cfg.hh"

#include "liblog/liblog.hh"

#include <algorithm>
#include <chrono>
#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Util {

namespace {

template <typename Src, typename Dst>
void copy_if_present(const boost::optional<Src>& src, LibStrong::Optional<Dst>& dst)
{
    if (src != boost::none)
    {
        dst = Dst{*src};
    }
}

decltype(auto) make_dsn(const Cfg::Options::Database& option)
{
    LibPg::Dsn result;
    copy_if_present(option.host, result.host);
    copy_if_present(option.host_addr, result.host_addr);
    copy_if_present(option.port, result.port);
    copy_if_present(option.user, result.user);
    copy_if_present(option.dbname, result.db_name);
    copy_if_present(option.password, result.password);
    copy_if_present(option.connect_timeout, result.connect_timeout);
    return result;
}

LibPg::PgConnection get_connection()
{
    install_pg_log_layer();
    static const auto dsn = make_dsn(Cfg::Options::get().database);
    return LibPg::PgConnection{dsn};
}

}//namespace Fred::Identity::Util::{anonymous}

}//namespace Fred::PublicRequest::Util
}//namespace Fred::PublicRequest
}//namespace Fred

using namespace Fred::PublicRequest::Util;

template <typename Level>
LibPg::PgRoTransaction Fred::PublicRequest::Util::make_ro_transaction(
        LibPg::PgTransaction::IsolationLevel<Level> level)
{
    return LibPg::PgRoTransaction{get_connection(), level};
}

LibPg::PgRwTransaction Fred::PublicRequest::Util::make_rw_transaction()
{
    return LibPg::PgRwTransaction{get_connection()};
}

template LibPg::PgRoTransaction Fred::PublicRequest::Util::make_ro_transaction<>(
        LibPg::PgTransaction::IsolationLevel<SessionDefault>);
template LibPg::PgRoTransaction Fred::PublicRequest::Util::make_ro_transaction<SerializableDeferrable>(
        LibPg::PgTransaction::IsolationLevel<SerializableDeferrable>);
