/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INSTALL_PG_LOG_LAYER_HH_F20BF43B7468D3F9B20AD909078108A4//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define INSTALL_PG_LOG_LAYER_HH_F20BF43B7468D3F9B20AD909078108A4

namespace Fred {
namespace PublicRequest {
namespace Util {

void install_pg_log_layer();

}//namespace Fred::PublicRequest::Util
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//INSTALL_PG_LOG_LAYER_HH_F20BF43B7468D3F9B20AD909078108A4
