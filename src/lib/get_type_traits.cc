/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/lib/get_type_traits.hh"

#include "libfred/public_request/get_public_request_types.hh"

#include <algorithm>

using namespace Fred::PublicRequest::Lib;

std::map<std::string, PublicRequestTypeTraits> Fred::PublicRequest::Lib::get_type_traits(
        const LibPg::PgTransaction& tx)
{
    const auto types = LibFred::PublicRequest::GetPublicRequestTypes{}.exec(tx);
    std::map<std::string, PublicRequestTypeTraits> result;
    std::for_each(begin(types), end(types), [&](auto&& type_name)
    {
        result.insert({type_name, PublicRequestTypeTraits{type_name}});
    });
    return result;
}
