/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/lib/invalidate.hh"
#include "src/lib/exceptions.hh"
#include "src/lib/get.hh"

#include "libfred/public_request/update_public_request.hh"

#include <cstdlib>
#include <utility>

namespace {

template <typename TrueFnc, typename FalseFnc>
decltype(auto) run_if_is_integer(const std::string& str, TrueFnc&& true_fnc, FalseFnc&& false_fnc)
{
    char* str_end = nullptr;
    const auto number = std::strtoull(str.c_str(), &str_end, 10);
    const bool is_integer = !str.empty() &&
                            ((str.c_str() + str.length()) == str_end);
    return is_integer ? std::forward<TrueFnc>(true_fnc)(number)
                      : std::forward<FalseFnc>(false_fnc)();
}

struct MissingPublicRequest : Fred::PublicRequest::Lib::PublicRequestDoesNotExist
{
    const char* what() const noexcept override { return "public request does not exist"; }
};

struct UnexpectedException : Fred::PublicRequest::Lib::CannotBeProcessed
{
    const char* what() const noexcept override { return "unexpected exception"; }
};

class FakePublicRequestForInvalidating : private LibFred::PublicRequestTypeIface
{
public:
    FakePublicRequestForInvalidating(std::string type) : type_{std::move(type)} { }
    ~FakePublicRequestForInvalidating() { }
    auto& iface() const
    {
        return static_cast<const PublicRequestTypeIface&>(*this);
    }
private:
    std::string get_public_request_type() const override { return type_; }
    PublicRequestTypes get_public_request_types_to_cancel_on_create() const override
    {
        struct InvalidUsage : Fred::PublicRequest::Lib::CannotBeProcessed
        {
            const char* what() const noexcept override
            {
                return "get_public_request_types_to_cancel_on_create method should never be called";
            }
        };
        throw InvalidUsage{};
    }
    PublicRequestTypes get_public_request_types_to_cancel_on_update(
            LibFred::PublicRequest::Status::Enum _old_status,
            LibFred::PublicRequest::Status::Enum _new_status) const override
    {
        if ((_old_status == LibFred::PublicRequest::Status::opened) &&
            (_new_status == LibFred::PublicRequest::Status::invalidated))
        {
            return PublicRequestTypes{};
        }
        struct InvalidUsage : Fred::PublicRequest::Lib::CannotBeProcessed
        {
            const char* what() const noexcept override
            {
                return "get_public_request_types_to_cancel_on_update method can only be used to "
                       "invalidate open requests";
            }
        };
        throw InvalidUsage{};
    }
    LibFred::PublicRequest::OnStatusAction::Enum get_on_status_action(
            LibFred::PublicRequest::Status::Enum status) const override
    {
        if (status == LibFred::PublicRequest::Status::invalidated)
        {
            return LibFred::PublicRequest::OnStatusAction::scheduled;
        }
        struct InvalidUsage : Fred::PublicRequest::Lib::CannotBeProcessed
        {
            const char* what() const noexcept override
            {
                return "get_on_status_action method can only be used to invalidate open requests";
            }
        };
        throw InvalidUsage{};
    }
    std::string type_;
};

}//namespace {anonymous}

using namespace Fred::PublicRequest::Lib;

void Fred::PublicRequest::Lib::invalidate_public_request(
        const LibPg::PgRwTransaction& tx,
        const PublicRequestId& public_request_id,
        const LogEntryId& resolving_id)
{
    try
    {
        const auto public_request_data = get_public_request(tx, public_request_id);
        LibFred::OperationContext ctx{tx};
        LibFred::PublicRequestLockGuardById locked_request{ctx, public_request_data.id};
        if (public_request_data.status != LibFred::PublicRequest::Status::opened)
        {
            struct UnexpectedStatus : CannotBeProcessed
            {
                const char* what() const noexcept override { return "not opened status"; }
            };
            throw UnexpectedStatus{};
        }
        const auto public_request = FakePublicRequestForInvalidating{public_request_data.type};
        LibFred::UpdatePublicRequest update_op{};
        update_op.set_status(LibFred::PublicRequest::Status::invalidated);
        const auto result = run_if_is_integer(
                *resolving_id,
                [&](auto log_request_id)
                {
                    return update_op.exec(locked_request, public_request.iface(), log_request_id);
                },
                [&]()
                {
                    return update_op.exec(locked_request, public_request.iface());
                });
        if (result.affected_requests.empty())
        {
            throw MissingPublicRequest{};
        }
        if (1 < result.affected_requests.size())
        {
            struct TooManyRequests : CannotBeProcessed
            {
                const char* what() const noexcept override { return "too many requests affected"; }
            };
            throw TooManyRequests{};
        }
        if (result.affected_requests[0] != public_request_data.id)
        {
            throw MissingPublicRequest{};
        }
    }
    catch (const LibFred::PublicRequest::PublicRequestDoesNotExist&)
    {
        throw MissingPublicRequest{};
    }
    catch (const LibFred::PublicRequestLockGuardById::Exception& e)
    {
        if (e.is_set_public_request_doesnt_exist())
        {
            throw MissingPublicRequest{};
        }
        throw UnexpectedException{};
    }
    catch (const LibFred::UpdatePublicRequest::Exception& e)
    {
        if (e.is_set_public_request_doesnt_exist())
        {
            throw MissingPublicRequest{};
        }
        if (e.is_set_bad_public_request_status())
        {
            struct BadStatus : CannotBeProcessed
            {
                const char* what() const noexcept override { return "bad public request status"; }
            };
            throw BadStatus{};
        }
        if (e.is_set_nothing_to_do())
        {
            struct NothingToDo : CannotBeProcessed
            {
                const char* what() const noexcept override { return "nothing to do"; }
            };
            throw NothingToDo{};
        }
        throw UnexpectedException{};
    }
    catch (const Exception&)
    {
        throw;
    }
    catch (...)
    {
        throw UnexpectedException{};
    }
}
