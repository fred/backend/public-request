/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COMMON_TYPES_HH_22E1EA5DC70F62A8C52317522FBF6211//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define COMMON_TYPES_HH_22E1EA5DC70F62A8C52317522FBF6211

#include "src/lib/strong_type.hh"

#include <string>

namespace Fred {
namespace PublicRequest {
namespace Lib {

using ContactId = StrongType<std::string, struct ContactId_Tag>;
using DomainId = StrongType<std::string, struct DomainId_Tag>;
using KeysetId = StrongType<std::string, struct KeysetId_Tag>;
using NssetId = StrongType<std::string, struct NssetId_Tag>;

using PublicRequestId = StrongType<std::string, struct PublicRequestId_Tag>;
using LogEntryId = StrongType<std::string, struct LogEntryId_Tag>;

}//namespace Fred::PublicRequest::Lib
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//COMMON_TYPES_HH_22E1EA5DC70F62A8C52317522FBF6211
