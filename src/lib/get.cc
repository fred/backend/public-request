/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/lib/get.hh"

#include <cstdlib>

using namespace Fred::PublicRequest::Lib;

LibFred::PublicRequest::PublicRequestData Fred::PublicRequest::Lib::get_public_request(
        const LibPg::PgTransaction& tx,
        const Lib::PublicRequestId& public_request_id)
{
    char* str_end = nullptr;
    const auto str_id = *public_request_id;
    const auto id = std::strtoull(str_id.c_str(), &str_end, 10);
    if (!str_id.empty() &&
        ((str_id.c_str() + str_id.length()) == str_end))
    {
        return LibFred::PublicRequest::GetPublicRequest{id}.exec(tx);
    }
    const auto uuid = boost::uuids::string_generator{}(str_id);
    return LibFred::PublicRequest::GetPublicRequest{uuid}.exec(tx);
}
