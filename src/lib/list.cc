/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/lib/list.hh"

#include <boost/uuid/string_generator.hpp>

#include <cstdlib>
#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Lib {

GetPublicRequests::GetPublicRequests(const LibPg::PgTransaction& tx)
    : tx_{tx}
{ }

namespace {

template <LibFred::Object_Type::Enum object_type>
std::vector<LibFred::PublicRequest::PublicRequestData> get_public_requests(
        const LibPg::PgTransaction& tx,
        const std::string& registry_object_reference)
{
    using GetPublicRequestsOp = LibFred::PublicRequest::GetPublicRequestsOf<object_type>;
    struct InvalidUuid {};
    bool is_valid_uuid;
    auto result = [&]()
    {
        try
        {
            const auto object_uuid = [&]()
            {
                try
                {
                    return boost::uuids::string_generator{}(registry_object_reference);
                }
                catch (const std::exception&)
                {
                    throw InvalidUuid{};
                }
            }();
            is_valid_uuid = true;
            return GetPublicRequestsOp{object_uuid}.exec(tx);
        }
        catch (const InvalidUuid&)
        {
            is_valid_uuid = false;
            return GetPublicRequestsOp{registry_object_reference}.exec(tx);
        }
    }();
    if (!is_valid_uuid)
    {
        char* str_end = nullptr;
        const auto object_numeric_id = std::strtoull(registry_object_reference.c_str(), &str_end, 10);
        const bool is_valid_number = !registry_object_reference.empty() &&
                                     (str_end == (registry_object_reference.c_str() + registry_object_reference.length()));
        if (is_valid_number)
        {
            auto result_by_id = GetPublicRequestsOp{object_numeric_id}.exec(tx);
            if (!result_by_id.empty())
            {
                result = std::move(result_by_id);
            }
        }
    }
    return result;
}

}//namespace Fred::PublicRequest::Lib::{anonymous}

std::vector<LibFred::PublicRequest::PublicRequestData> GetPublicRequests::operator()(
        const Lib::ContactId& contact_id) const
{
    return get_public_requests<LibFred::Object_Type::contact>(tx_, *contact_id);
}

std::vector<LibFred::PublicRequest::PublicRequestData> GetPublicRequests::operator()(
        const Lib::DomainId& domain_id) const
{
    return get_public_requests<LibFred::Object_Type::domain>(tx_, *domain_id);
}

std::vector<LibFred::PublicRequest::PublicRequestData> GetPublicRequests::operator()(
        const Lib::KeysetId& keyset_id) const
{
    return get_public_requests<LibFred::Object_Type::keyset>(tx_, *keyset_id);
}

std::vector<LibFred::PublicRequest::PublicRequestData> GetPublicRequests::operator()(
        const Lib::NssetId& nsset_id) const
{
    return get_public_requests<LibFred::Object_Type::nsset>(tx_, *nsset_id);
}

}//namespace Fred::PublicRequest::Lib
}//namespace Fred::PublicRequest
}//namespace Fred
