/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_HH_A569A0FEFA37536AAE94D21462C71932//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LIST_HH_A569A0FEFA37536AAE94D21462C71932

#include "src/lib/common_types.hh"
#include "src/unwrapper/list_request.hh"
#include "src/wrapper/list_reply.hh"

#include "libfred/public_request/get_public_request.hh"

#include "libpg/pg_transaction.hh"

namespace Fred {
namespace PublicRequest {
namespace Lib {

class GetPublicRequests
{
public:
    explicit GetPublicRequests(const LibPg::PgTransaction& tx);
    std::vector<LibFred::PublicRequest::PublicRequestData> operator()(
            const Lib::ContactId& contact_id) const;
    std::vector<LibFred::PublicRequest::PublicRequestData> operator()(
            const Lib::DomainId& domain_id) const;
    std::vector<LibFred::PublicRequest::PublicRequestData> operator()(
            const Lib::KeysetId& keyset_id) const;
    std::vector<LibFred::PublicRequest::PublicRequestData> operator()(
            const Lib::NssetId& nsset_id) const;
private:
    const LibPg::PgTransaction& tx_;
};

template <typename Request, typename Reply>
void list(
        const LibPg::PgTransaction& tx,
        const Unwrapper::ListRequest<Request>& request,
        Wrapper::ListReply<Reply>& response)
{
    try
    {
        const auto result = request.registry_object_reference()(GetPublicRequests{tx});
        response.set_public_requests(result.size(), begin(result), end(result));
        std::move(response).finish();
    }
    catch (...)
    {
    }
}

}//namespace Fred::PublicRequest::Lib
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//LIST_HH_A569A0FEFA37536AAE94D21462C71932
