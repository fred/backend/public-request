/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/lib/public_request_type_traits.hh"

#include <set>

namespace Fred {
namespace PublicRequest {
namespace Lib {

namespace {

bool is_public_request_resolvable(const std::string& name)
{
    static const auto non_resolvable_types = std::set<std::string>{
            "contact_conditional_identification",
            "contact_identification",
            "mojeid_conditionally_identified_contact_transfer",
            "mojeid_contact_conditional_identification",
            "mojeid_contact_identification",
            "mojeid_contact_reidentification",
            "mojeid_identified_contact_transfer",
            "mojeid_prevalidated_contact_transfer",
            "mojeid_prevalidated_unidentified_contact_transfer"
    };
    return non_resolvable_types.find(name) == end(non_resolvable_types);
}

}//namespace Fred::PublicRequest::Lib::{anonymous}

PublicRequestTypeTraits::PublicRequestTypeTraits(const std::string& name)
    : is_resolvable{is_public_request_resolvable(name)}
{ }

}//namespace Fred::PublicRequest::Lib
}//namespace Fred::PublicRequest
}//namespace Fred
