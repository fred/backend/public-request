/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_TYPE_TRAITS_HH_F96596BF55015F41B845DBCA4055BE63//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_TYPE_TRAITS_HH_F96596BF55015F41B845DBCA4055BE63

#include "src/lib/public_request_type_traits.hh"

#include "libpg/pg_transaction.hh"

#include <map>
#include <string>

namespace Fred {
namespace PublicRequest {
namespace Lib {

std::map<std::string, PublicRequestTypeTraits> get_type_traits(const LibPg::PgTransaction& tx);

}//namespace Fred::PublicRequest::Lib
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//GET_TYPE_TRAITS_HH_F96596BF55015F41B845DBCA4055BE63
