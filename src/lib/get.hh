/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef GET_HH_2221FFA64FA374782539270586A1FF2B//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_HH_2221FFA64FA374782539270586A1FF2B

#include "src/unwrapper/get_request.hh"
#include "src/wrapper/get_reply.hh"

#include "libfred/public_request/get_public_request.hh"
#include "libpg/pg_transaction.hh"

namespace Fred {
namespace PublicRequest {
namespace Lib {

LibFred::PublicRequest::PublicRequestData get_public_request(
        const LibPg::PgTransaction& tx,
        const Lib::PublicRequestId& public_request_id);

template <typename Request, typename Reply>
void get(
        const LibPg::PgTransaction& tx,
        const Unwrapper::GetRequest<Request>& request,
        Wrapper::GetReply<Reply>& response)
{
    try
    {
        response.set_data(get_public_request(tx, request.public_request_id()));
    }
    catch (const LibFred::PublicRequest::PublicRequestDoesNotExist&)
    {
        response.set_exception_public_request_does_not_exist();
    }
}

}//namespace Fred::PublicRequest::Lib
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//GET_HH_2221FFA64FA374782539270586A1FF2B
