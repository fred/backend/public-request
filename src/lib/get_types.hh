/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef GET_TYPES_HH_EE355280570D3863F0A01669AF218103//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_TYPES_HH_EE355280570D3863F0A01669AF218103

#include "src/wrapper/get_types_reply.hh"
#include "src/lib/get_type_traits.hh"

#include "libpg/pg_transaction.hh"

namespace Fred {
namespace PublicRequest {
namespace Lib {

template <typename Reply>
void get_types(const LibPg::PgTransaction& tx, Wrapper::GetTypesReply<Reply>& response)
{
    const auto type_traits = get_type_traits(tx);
    response.set_types(type_traits.size(), begin(type_traits), end(type_traits));
}

}//namespace Fred::PublicRequest::Lib
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//GET_TYPES_HH_EE355280570D3863F0A01669AF218103
