/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef RESOLVE_HH_50CC13A33D769B453BB41DFEBAAF2A61//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define RESOLVE_HH_50CC13A33D769B453BB41DFEBAAF2A61

#include "src/lib/exceptions.hh"
#include "src/unwrapper/resolve_request.hh"
#include "src/wrapper/resolve_reply.hh"

#include "libpg/pg_rw_transaction.hh"

namespace Fred {
namespace PublicRequest {
namespace Lib {

void resolve_public_request(
        const LibPg::PgRwTransaction& tx,
        const Lib::PublicRequestId& public_request_id,
        const Lib::LogEntryId& resolving_id);

template <typename Request, typename Reply>
void resolve(
        const LibPg::PgRwTransaction& tx,
        const Unwrapper::ResolveRequest<Request>& request,
        Wrapper::ResolveReply<Reply>& response)
{
    try
    {
        resolve_public_request(tx, request.public_request_id(), request.resolving_id());
    }
    catch (const PublicRequestDoesNotExist&)
    {
        response.set_exception_public_request_does_not_exist();
    }
    catch (const CannotBeProcessed&)
    {
        response.set_exception_cannot_be_processed();
    }
}

}//namespace Fred::PublicRequest::Lib
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//RESOLVE_HH_50CC13A33D769B453BB41DFEBAAF2A61
