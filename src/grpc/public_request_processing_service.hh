/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PUBLIC_REQUEST_PROCESSING_SERVICE_HH_8B92D697844F15678B36F0772BEAEEA1//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PUBLIC_REQUEST_PROCESSING_SERVICE_HH_8B92D697844F15678B36F0772BEAEEA1

#include "fred_api/public_request/service_public_request_processing_grpc.grpc.pb.h"

namespace Fred {
namespace PublicRequest {
namespace Grpc {

class PublicRequestProcessingService final : public Api::PublicRequestProcessing::Service
{
private:
    grpc::Status resolve(
            grpc::ServerContext* context,
            const Api::ResolveRequest* request,
            Api::ResolveReply* response);
    grpc::Status invalidate(
            grpc::ServerContext* context,
            const Api::InvalidateRequest* request,
            Api::InvalidateReply* response);
};

}//namespace Fred::PublicRequest::Grpc
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//PUBLIC_REQUEST_PROCESSING_SERVICE_HH_8B92D697844F15678B36F0772BEAEEA1
