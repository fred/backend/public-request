/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TOOLS_HH_EA701F85608946E53C1A630F1BEC951E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define TOOLS_HH_EA701F85608946E53C1A630F1BEC951E

#include <grpc++/grpc++.h>

#include <exception>

namespace Fred {
namespace PublicRequest {
namespace Grpc {
namespace Tools {

grpc::Status on_sql_error(const std::exception& e);

grpc::Status on_stream_cancelled(const std::exception& e);

grpc::Status on_stream_closed(const std::exception& e);

grpc::Status on_std_exception(const std::exception& e);

grpc::Status on_unknown_exception();

}//namespace Fred::PublicRequest::Grpc::Tools
}//namespace Fred::PublicRequest::Grpc
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//TOOLS_HH_EA701F85608946E53C1A630F1BEC951E
