/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/grpc/tools/tools.hh"

#include "liblog/liblog.hh"

using namespace Fred::PublicRequest::Grpc::Tools;

grpc::Status Fred::PublicRequest::Grpc::Tools::on_sql_error(const std::exception& e)
{
    LIBLOG_WARNING("SQL error: {}", e.what());
    return grpc::Status(grpc::StatusCode::INTERNAL, "SQL error", e.what());
}

grpc::Status Fred::PublicRequest::Grpc::Tools::on_stream_cancelled(const std::exception& e)
{
    LIBLOG_WARNING("Cancelled caught: {}", e.what());
    return grpc::Status(grpc::StatusCode::CANCELLED, "Deadline exceeded or Client cancelled, abandoning.", e.what());
}

grpc::Status Fred::PublicRequest::Grpc::Tools::on_stream_closed(const std::exception& e)
{
    LIBLOG_WARNING("Closed caught: {}", e.what());
    return grpc::Status(grpc::StatusCode::UNKNOWN, "Write failed", e.what());
}

grpc::Status Fred::PublicRequest::Grpc::Tools::on_std_exception(const std::exception& e)
{
    LIBLOG_ERROR("std::exception caught: {}", e.what());
    return grpc::Status(grpc::StatusCode::UNKNOWN, "Unknown exception based on std::exception");
}

grpc::Status Fred::PublicRequest::Grpc::Tools::on_unknown_exception()
{
    LIBLOG_ERROR("unknown exception caught");
    return grpc::Status(grpc::StatusCode::UNKNOWN, "Unknown exception");
}
