/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/grpc/public_request_processing_service.hh"
#include "src/grpc/tools/tools.hh"
#include "src/lib/invalidate.hh"
#include "src/lib/resolve.hh"
#include "src/unwrapper/grpc/invalidate_request.hh"
#include "src/unwrapper/grpc/resolve_request.hh"
#include "src/util/util.hh"
#include "src/wrapper/grpc/invalidate_reply.hh"
#include "src/wrapper/grpc/resolve_reply.hh"

#include "liblog/liblog.hh"

#include "libpg/pg_exception.hh"

#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Grpc {

grpc::Status PublicRequestProcessingService::resolve(
        grpc::ServerContext*,
        const Api::ResolveRequest* request,
        Api::ResolveReply* response)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, "PublicRequestProcessing", __func__);
    try
    {
        auto tx = Util::make_rw_transaction();
        auto wrapper = Wrapper::Grpc::make_wrapper(*response);
        Lib::resolve(
                tx,
                Unwrapper::Grpc::make_unwrapper(*request),
                wrapper);
        commit(std::move(tx));
        return grpc::Status::OK;
    }
    catch (const LibPg::ExecFailure& e)
    {
        return Tools::on_sql_error(e);
    }
    catch (const std::exception& e)
    {
        return Tools::on_std_exception(e);
    }
    catch (...)
    {
        return Tools::on_unknown_exception();
    }
}

grpc::Status PublicRequestProcessingService::invalidate(
        grpc::ServerContext*,
        const Api::InvalidateRequest* request,
        Api::InvalidateReply* response)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, "PublicRequestProcessing", __func__);
    try
    {
        auto tx = Util::make_rw_transaction();
        auto wrapper = Wrapper::Grpc::make_wrapper(*response);
        Lib::invalidate(
                tx,
                Unwrapper::Grpc::make_unwrapper(*request),
                wrapper);
        commit(std::move(tx));
        return grpc::Status::OK;
    }
    catch (const LibPg::ExecFailure& e)
    {
        return Tools::on_sql_error(e);
    }
    catch (const std::exception& e)
    {
        return Tools::on_std_exception(e);
    }
    catch (...)
    {
        return Tools::on_unknown_exception();
    }
}

}//namespace Fred::PublicRequest::Grpc
}//namespace Fred::PublicRequest
}//namespace Fred
