/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/grpc/public_request_info_service.hh"
#include "src/grpc/tools/tools.hh"
#include "src/lib/exceptions.hh"
#include "src/lib/get.hh"
#include "src/lib/get_types.hh"
#include "src/lib/list.hh"
#include "src/unwrapper/grpc/get_request.hh"
#include "src/unwrapper/grpc/list_request.hh"
#include "src/wrapper/grpc/common_types.hh"
#include "src/wrapper/grpc/get_reply.hh"
#include "src/wrapper/grpc/get_types_reply.hh"
#include "src/wrapper/grpc/list_reply.hh"
#include "src/wrapper/grpc/public_request_type.hh"
#include "src/util/util.hh"

#include "liblog/liblog.hh"

#include "libpg/pg_exception.hh"

#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Grpc {

grpc::Status PublicRequestInfoService::get_types(
        grpc::ServerContext*,
        const google::protobuf::Empty*,
        Api::GetTypesReply* response)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, "PublicRequestInfo", __func__);
    try
    {
        auto tx = Util::make_ro_transaction();
        auto wrapper = Wrapper::Grpc::make_wrapper(*response);
        Lib::get_types(tx, wrapper);
        commit(std::move(tx));
        return grpc::Status::OK;
    }
    catch (const LibPg::ExecFailure& e)
    {
        return Tools::on_sql_error(e);
    }
    catch (const std::exception& e)
    {
        return Tools::on_std_exception(e);
    }
    catch (...)
    {
        return Tools::on_unknown_exception();
    }
}

grpc::Status PublicRequestInfoService::list(
        grpc::ServerContext* context,
        const Api::ListRequest* request,
        grpc::ServerWriter<Api::ListReply>* writer)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, "PublicRequestInfo", __func__);
    try
    {
        auto tx = Util::make_ro_transaction();
        auto wrapper = Wrapper::Grpc::make_wrapper(*writer, context);
        Lib::list(tx, Unwrapper::Grpc::make_unwrapper(*request), wrapper);
        commit(std::move(tx));
        return grpc::Status::OK;
    }
    catch (const Lib::Cancelled& e)
    {
        return Tools::on_stream_cancelled(e);
    }
    catch (const Lib::Closed& e)
    {
        return Tools::on_stream_closed(e);
    }
    catch (const LibPg::ExecFailure& e)
    {
        return Tools::on_sql_error(e);
    }
    catch (const std::exception& e)
    {
        return Tools::on_std_exception(e);
    }
    catch (...)
    {
        return Tools::on_unknown_exception();
    }
}

grpc::Status PublicRequestInfoService::get(
        grpc::ServerContext*,
        const Api::GetRequest* request,
        Api::GetReply* response)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, "PublicRequestInfo", __func__);
    try
    {
        auto tx = Util::make_ro_transaction();
        auto wrapper = Wrapper::Grpc::make_wrapper(*response);
        Lib::get(tx, Unwrapper::Grpc::make_unwrapper(*request), wrapper);
        commit(std::move(tx));
        return grpc::Status::OK;
    }
    catch (const LibPg::ExecFailure& e)
    {
        return Tools::on_sql_error(e);
    }
    catch (const std::exception& e)
    {
        return Tools::on_std_exception(e);
    }
    catch (...)
    {
        return Tools::on_unknown_exception();
    }
}

}//namespace Fred::PublicRequest::Grpc
}//namespace Fred::PublicRequest
}//namespace Fred
