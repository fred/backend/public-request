/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PUBLIC_REQUEST_INFO_SERVICE_HH_A3D4A30DFA70BED090A55EBD9061E04B//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PUBLIC_REQUEST_INFO_SERVICE_HH_A3D4A30DFA70BED090A55EBD9061E04B

#include "fred_api/public_request/service_public_request_info_grpc.grpc.pb.h"

namespace Fred {
namespace PublicRequest {
namespace Grpc {

class PublicRequestInfoService final : public Api::PublicRequestInfo::Service
{
private:
    grpc::Status get_types(
            grpc::ServerContext* context,
            const google::protobuf::Empty* request,
            Fred::PublicRequest::Api::GetTypesReply* response) override;
    grpc::Status list(
            grpc::ServerContext* context,
            const Fred::PublicRequest::Api::ListRequest* request,
            grpc::ServerWriter<Fred::PublicRequest::Api::ListReply>* writer) override;
    grpc::Status get(
            grpc::ServerContext* context,
            const Fred::PublicRequest::Api::GetRequest* request,
            Fred::PublicRequest::Api::GetReply* response) override;
};

}//namespace Fred::PublicRequest::Grpc
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//PUBLIC_REQUEST_INFO_SERVICE_HH_A3D4A30DFA70BED090A55EBD9061E04B
