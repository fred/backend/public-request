/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_REQUEST_HH_4A748F0D13A6B5392A53A10271002B18//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_REQUEST_HH_4A748F0D13A6B5392A53A10271002B18

#include "src/lib/common_types.hh"

namespace Fred {
namespace PublicRequest {
namespace Unwrapper {

template <typename T>
class GetRequest
{
public:
    decltype(auto) public_request_id() const
    {
        return static_cast<const T*>(this)->public_request_id();
    }
};

}//namespace Fred::PublicRequest::Unwrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//GET_REQUEST_HH_4A748F0D13A6B5392A53A10271002B18
