/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef RESOLVE_REQUEST_HH_9A2D6F272EE5656B286FF6F2970B1414//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define RESOLVE_REQUEST_HH_9A2D6F272EE5656B286FF6F2970B1414

#include "src/unwrapper/resolve_request.hh"

#include "fred_api/public_request/service_public_request_processing_grpc.pb.h"

#include <string>

namespace Fred {
namespace PublicRequest {
namespace Unwrapper {
namespace Grpc {

class ResolveRequest;

ResolveRequest make_unwrapper(const Api::ResolveRequest& data);

class ResolveRequest : public Unwrapper::ResolveRequest<ResolveRequest>
{
public:
    Lib::PublicRequestId public_request_id() const;
    bool has_resolving_id() const;
    Lib::LogEntryId resolving_id() const;
private:
    explicit ResolveRequest(const Api::ResolveRequest& data);
    const Api::ResolveRequest& data_;
    friend ResolveRequest make_unwrapper(const Api::ResolveRequest& data);
};

}//namespace Fred::PublicRequest::Unwrapper::Grpc
}//namespace Fred::PublicRequest::Unwrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//RESOLVE_REQUEST_HH_9A2D6F272EE5656B286FF6F2970B1414
