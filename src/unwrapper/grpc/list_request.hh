/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_REQUEST_HH_2763008456ECD655E3D49AD738A86FB9//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LIST_REQUEST_HH_2763008456ECD655E3D49AD738A86FB9

#include "src/unwrapper/list_request.hh"
#include "src/unwrapper/grpc/common_types.hh"

#include "fred_api/public_request/service_public_request_info_grpc.pb.h"

namespace Fred {
namespace PublicRequest {
namespace Unwrapper {
namespace Grpc {

class ListRequest;

ListRequest make_unwrapper(const Api::ListRequest& data);

class ListRequest : public Unwrapper::ListRequest<ListRequest>
{
public:
    RegistryObjectReference registry_object_reference() const;
private:
    explicit ListRequest(const Api::ListRequest& data);
    const Api::ListRequest& data_;
    friend ListRequest make_unwrapper(const Api::ListRequest& data);
};

}//namespace Fred::PublicRequest::Unwrapper::Grpc
}//namespace Fred::PublicRequest::Unwrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//LIST_REQUEST_HH_2763008456ECD655E3D49AD738A86FB9
