/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/unwrapper/grpc/invalidate_request.hh"

namespace Fred {
namespace PublicRequest {
namespace Unwrapper {
namespace Grpc {

InvalidateRequest::InvalidateRequest(const Api::InvalidateRequest& data)
    : data_{data}
{ }

Lib::PublicRequestId InvalidateRequest::public_request_id() const
{
    return Lib::PublicRequestId{data_.has_public_request_id() ? data_.public_request_id().value()
                                                              : std::string{}};
}

bool InvalidateRequest::has_resolving_id() const
{
    return data_.has_resolving_id();
}

Lib::LogEntryId InvalidateRequest::resolving_id() const
{
    return Lib::LogEntryId{data_.has_resolving_id() ? data_.resolving_id().value()
                                                    : std::string{}};
}

}//namespace Fred::PublicRequest::Unwrapper::Grpc
}//namespace Fred::PublicRequest::Unwrapper
}//namespace Fred::PublicRequest
}//namespace Fred

using namespace Fred::PublicRequest::Unwrapper::Grpc;

InvalidateRequest Fred::PublicRequest::Unwrapper::Grpc::make_unwrapper(const Api::InvalidateRequest& data)
{
    return InvalidateRequest{data};
}
