/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/unwrapper/grpc/common_types.hh"

namespace Fred {
namespace PublicRequest {
namespace Unwrapper {
namespace Grpc {

RegistryObjectReference::RegistryObjectReference(const Api::RegistryObjectReference& data)
    : data_{data}
{ }

}//namespace Fred::PublicRequest::Unwrapper::Grpc
}//namespace Fred::PublicRequest::Unwrapper
}//namespace Fred::PublicRequest
}//namespace Fred

using namespace Fred::PublicRequest::Unwrapper::Grpc;

RegistryObjectReference Fred::PublicRequest::Unwrapper::Grpc::make_unwrapper(const Api::RegistryObjectReference& data)
{
    return RegistryObjectReference{data};
}
