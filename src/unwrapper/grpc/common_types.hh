/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COMMON_TYPES_HH_A45C89D6C38653AE6560B87C71C7391E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define COMMON_TYPES_HH_A45C89D6C38653AE6560B87C71C7391E

#include "src/lib/common_types.hh"
#include "src/unwrapper/common_types.hh"

#include "fred_api/public_request/common_types.pb.h"

#include <exception>

namespace Fred {
namespace PublicRequest {
namespace Unwrapper {
namespace Grpc {

class RegistryObjectReference;

RegistryObjectReference make_unwrapper(const Api::RegistryObjectReference& data);

class RegistryObjectReference : public Unwrapper::RegistryObjectReference<RegistryObjectReference>
{
public:
    template <typename Visitor>
    decltype(auto) operator()(const Visitor& visitor) const
    {
        switch (data_.type())
        {
            case Api::RegistryObjectType::contact :
                return visitor(Lib::ContactId{data_.value()});
            case Api::RegistryObjectType::domain:
                return visitor(Lib::DomainId{data_.value()});
            case Api::RegistryObjectType::keyset:
                return visitor(Lib::KeysetId{data_.value()});
            case Api::RegistryObjectType::nsset:
                return visitor(Lib::NssetId{data_.value()});
            case Api::RegistryObjectType::undefined:
                struct UndefinedObjectType : std::exception
                {
                    const char* what() const noexcept override { return "Undefined object type"; }
                };
                throw UndefinedObjectType{};
            default:
                break;
        }
        struct UnknownRegistryObjectType : std::exception
        {
            const char* what() const noexcept override { return "Unknown object type"; }
        };
        throw UnknownRegistryObjectType{};
    }
private:
    explicit RegistryObjectReference(const Api::RegistryObjectReference& data);
    const Api::RegistryObjectReference& data_;
    friend RegistryObjectReference make_unwrapper(const Api::RegistryObjectReference& data);
};

}//namespace Fred::PublicRequest::Unwrapper::Grpc
}//namespace Fred::PublicRequest::Unwrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//COMMON_TYPES_HH_A45C89D6C38653AE6560B87C71C7391E
