/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_REQUEST_HH_59CD8E4890F2596270F9480A81440B04//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_REQUEST_HH_59CD8E4890F2596270F9480A81440B04

#include "src/lib/common_types.hh"
#include "src/unwrapper/get_request.hh"

#include "fred_api/public_request/service_public_request_info_grpc.pb.h"

#include <string>

namespace Fred {
namespace PublicRequest {
namespace Unwrapper {
namespace Grpc {

class GetRequest;

GetRequest make_unwrapper(const Api::GetRequest& data);

class GetRequest : public Unwrapper::GetRequest<GetRequest>
{
public:
    Lib::PublicRequestId public_request_id() const;
private:
    explicit GetRequest(const Api::GetRequest& data);
    const Api::GetRequest& data_;
    friend GetRequest make_unwrapper(const Api::GetRequest& data);
};

}//namespace Fred::PublicRequest::Unwrapper::Grpc
}//namespace Fred::PublicRequest::Unwrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//GET_REQUEST_HH_59CD8E4890F2596270F9480A81440B04
