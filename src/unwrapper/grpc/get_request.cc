/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/unwrapper/grpc/get_request.hh"

namespace Fred {
namespace PublicRequest {
namespace Unwrapper {
namespace Grpc {

GetRequest::GetRequest(const Api::GetRequest& data)
    : data_{data}
{ }

Lib::PublicRequestId GetRequest::public_request_id() const
{
    return Lib::PublicRequestId{data_.has_public_request_id() ? data_.public_request_id().value()
                                                              : std::string{}};
}

}//namespace Fred::PublicRequest::Unwrapper::Grpc
}//namespace Fred::PublicRequest::Unwrapper
}//namespace Fred::PublicRequest
}//namespace Fred

using namespace Fred::PublicRequest::Unwrapper::Grpc;

GetRequest Fred::PublicRequest::Unwrapper::Grpc::make_unwrapper(const Api::GetRequest& data)
{
    return GetRequest{data};
}
