/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_REQUEST_HH_35B5A14DE7B024E5498AF2B7605A5EE9//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LIST_REQUEST_HH_35B5A14DE7B024E5498AF2B7605A5EE9

namespace Fred {
namespace PublicRequest {
namespace Unwrapper {

template <typename T>
class ListRequest
{
public:
    decltype(auto) registry_object_reference() const
    {
        return static_cast<const T*>(this)->registry_object_reference();
    }
};

}//namespace Fred::PublicRequest::Unwrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//LIST_REQUEST_HH_35B5A14DE7B024E5498AF2B7605A5EE9
