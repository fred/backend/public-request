/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INVALIDATE_REQUEST_HH_69BFB648406E7D614EC730C1D194F5B4//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define INVALIDATE_REQUEST_HH_69BFB648406E7D614EC730C1D194F5B4

#include "src/lib/common_types.hh"

namespace Fred {
namespace PublicRequest {
namespace Unwrapper {

template <typename T>
class InvalidateRequest
{
public:
    Lib::PublicRequestId public_request_id() const
    {
        return static_cast<const T*>(this)->public_request_id();
    }
    bool has_resolving_id() const
    {
        return static_cast<const T*>(this)->has_resolving_id();
    }
    Lib::LogEntryId resolving_id() const
    {
        return static_cast<const T*>(this)->resolving_id();
    }
};

}//namespace Fred::PublicRequest::Unwrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//INVALIDATE_REQUEST_HH_69BFB648406E7D614EC730C1D194F5B4
