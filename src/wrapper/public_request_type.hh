/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PUBLIC_REQUEST_TYPE_HH_D57AC24014DC6BBD2643EBA59BC165BA//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PUBLIC_REQUEST_TYPE_HH_D57AC24014DC6BBD2643EBA59BC165BA

#include <string>
#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {

template <typename T>
class PublicRequestType
{
public:
    PublicRequestType& set_name(std::string value)
    {
        static_cast<T*>(this)->set_name(std::move(value));
        return *this;
    }
    PublicRequestType& set_is_resolvable(bool value)
    {
        static_cast<T*>(this)->set_is_resolvable(value);
        return *this;
    }
};

}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//PUBLIC_REQUEST_TYPE_HH_D57AC24014DC6BBD2643EBA59BC165BA
