/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef GET_TYPES_REPLY_HH_BFCE3D909B12D52021D2053F4EF968ED//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_TYPES_REPLY_HH_BFCE3D909B12D52021D2053F4EF968ED

#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {

template <typename T>
class GetTypesReply
{
public:
    template <typename Size, typename IterB, typename IterE>
    GetTypesReply& set_types(Size size, IterB&& begin, IterE&& end)
    {
        static_cast<T*>(this)->set_types(size, std::forward<IterB>(begin), std::forward<IterE>(end));
        return *this;
    }
protected:
    ~GetTypesReply() = default;
};

}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//GET_TYPES_REPLY_HH_BFCE3D909B12D52021D2053F4EF968ED
