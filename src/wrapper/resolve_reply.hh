/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef RESOLVE_REPLY_HH_C3298E78E0D2E9BB17AED7959C8B7729//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define RESOLVE_REPLY_HH_C3298E78E0D2E9BB17AED7959C8B7729

#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {

template <typename T>
class ResolveReply
{
public:
    ResolveReply& set_exception_public_request_does_not_exist()
    {
        static_cast<T*>(this)->set_exception_public_request_does_not_exist();
        return *this;
    }
    ResolveReply& set_exception_cannot_be_processed()
    {
        static_cast<T*>(this)->set_exception_cannot_be_processed();
        return *this;
    }
protected:
    ~ResolveReply() = default;
};

}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//RESOLVE_REPLY_HH_C3298E78E0D2E9BB17AED7959C8B7729
