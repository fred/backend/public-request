/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COMMON_TYPES_HH_1FF783CA7C517F7871C575B25523992E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define COMMON_TYPES_HH_1FF783CA7C517F7871C575B25523992E

#include "src/lib/common_types.hh"

#include "libfred/public_request/public_request_data.hh"

#include <boost/uuid/uuid_io.hpp>

#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {

template <typename T>
class PublicRequest
{
public:
    PublicRequest& operator()(LibFred::PublicRequest::PublicRequestData data)
    {
        auto& derived = *static_cast<T*>(this);
        derived.set_public_request_id(to_string(data.uuid));
        derived.set_numeric_id(data.id);
        class RegistryObjectReferenceSetter : boost::static_visitor<>
        {
        public:
            explicit RegistryObjectReferenceSetter(T& derived)
                : derived_{derived}
            { }
            using Data = LibFred::PublicRequest::PublicRequestData;
            void operator()(const Data::ContactDataLight& contact) const
            {
                derived_.set_registry_object_reference(Lib::ContactId{to_string(contact.uuid)});
            }
            void operator()(const Data::DomainDataLight& domain) const
            {
                derived_.set_registry_object_reference(Lib::DomainId{to_string(domain.uuid)});
            }
            void operator()(const Data::KeysetDataLight& keyset) const
            {
                derived_.set_registry_object_reference(Lib::KeysetId{to_string(keyset.uuid)});
            }
            void operator()(const Data::NssetDataLight& nsset) const
            {
                derived_.set_registry_object_reference(Lib::NssetId{to_string(nsset.uuid)});
            }
        private:
            T& derived_;
        };
        boost::apply_visitor(RegistryObjectReferenceSetter{derived}, data.object_data);
        derived.set_type(std::move(data.type));
        if (data.registrar_data != boost::none)
        {
            derived.set_registrar_id(data.registrar_data->handle);
        }
        derived.set_response_email(std::move(data.email_to_answer));
        derived.set_create_time(data.create_time);
        derived.set_creation_id(std::move(data.create_request_id));
        if (data.resolve_time != boost::none)
        {
            derived.set_resolve_time(*data.resolve_time);
        }
        derived.set_resolving_id(std::move(data.resolve_request_id));
        derived.set_status(data.status);
        derived.set_on_status_action(data.on_status_action);
        return *this;
    }
};

}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//COMMON_TYPES_HH_1FF783CA7C517F7871C575B25523992E
