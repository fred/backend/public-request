/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIST_REPLY_HH_16AF67B5BBD7C212F2D68DF292C9F051//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LIST_REPLY_HH_16AF67B5BBD7C212F2D68DF292C9F051

#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {

template <typename T>
class ListReply
{
public:
    template <typename Size, typename IterB, typename IterE>
    ListReply& set_public_requests(Size size, IterB&& begin, IterE&& end)
    {
        static_cast<T*>(this)->set_public_requests(
                size,
                std::forward<IterB>(begin),
                std::forward<IterE>(end));
        return *this;
    }
    void finish() &&
    {
        std::move(*static_cast<T*>(this)).finish();
    }
protected:
    ~ListReply() = default;
};

}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//LIST_REPLY_HH_16AF67B5BBD7C212F2D68DF292C9F051
