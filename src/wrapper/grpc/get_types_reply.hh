/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_TYPES_REPLY_HH_4B2308B347C1C48BB53168018A34A0B7//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_TYPES_REPLY_HH_4B2308B347C1C48BB53168018A34A0B7

#include "src/wrapper/get_types_reply.hh"
#include "src/lib/public_request_type_traits.hh"

#include "fred_api/public_request/service_public_request_info_grpc.pb.h"

#include <map>
#include <string>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {
namespace Grpc {

class GetTypesReply;

GetTypesReply make_wrapper(Api::GetTypesReply& data);

class GetTypesReply : public Wrapper::GetTypesReply<GetTypesReply>
{
public:
    using TypeTraitsIterator = std::map<std::string, Lib::PublicRequestTypeTraits>::const_iterator;
    void set_types(std::size_t size, TypeTraitsIterator begin, TypeTraitsIterator end);
private:
    explicit GetTypesReply(Api::GetTypesReply& data);
    Api::GetTypesReply& data_;
    friend GetTypesReply make_wrapper(Api::GetTypesReply& data);
};

}//namespace Fred::PublicRequest::Wrapper::Grpc
}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//GET_TYPES_REPLY_HH_4B2308B347C1C48BB53168018A34A0B7
