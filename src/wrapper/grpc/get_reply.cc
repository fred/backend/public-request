/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/wrapper/grpc/get_reply.hh"
#include "src/wrapper/grpc/common_types.hh"

#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {
namespace Grpc {

GetReply::GetReply(Api::GetReply& reply)
    : reply_{reply}
{
    reply_.Clear();
}

void GetReply::set_data(LibFred::PublicRequest::PublicRequestData data) const
{
    auto wrapper = make_wrapper(*reply_.mutable_data());
    wrapper(std::move(data));
}

void GetReply::set_exception_public_request_does_not_exist() const
{
    reply_.mutable_exception()->mutable_public_request_does_not_exist();
}

}//namespace Fred::PublicRequest::Wrapper::Grpc
}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

using namespace Fred::PublicRequest::Wrapper::Grpc;

GetReply Fred::PublicRequest::Wrapper::Grpc::make_wrapper(Api::GetReply& reply)
{
    return GetReply{reply};
}
