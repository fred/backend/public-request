/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/wrapper/grpc/common_types.hh"

#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {
namespace Grpc {

namespace {

void set_time(
        const LibFred::PublicRequest::PublicRequestData::TimePoint& src,
        google::protobuf::Timestamp& dst)
{
    const auto t = std::chrono::duration_cast<std::chrono::nanoseconds>(src.time_since_epoch()).count();
    dst.set_seconds(t / 1'000'000'000);
    dst.set_nanos(t % 1'000'000'000);
}

}//namespace Fred::PublicRequest::Wrapper::Grpc::{anonymous}

PublicRequest::PublicRequest(Api::PublicRequest& data)
    : data_{data}
{
    data_.Clear();
}

void PublicRequest::set_public_request_id(std::string value) const
{
    data_.mutable_public_request_id()->set_value(std::move(value));
}

void PublicRequest::set_numeric_id(std::uint64_t value) const
{
    data_.set_numeric_id(value);
}

void PublicRequest::set_registry_object_reference(Lib::ContactId value) const
{
    auto& registry_object_reference = *data_.mutable_registry_object_reference();
    registry_object_reference.set_type(Api::RegistryObjectType::contact);
    registry_object_reference.set_value(*std::move(value));
}

void PublicRequest::set_registry_object_reference(Lib::DomainId value) const
{
    auto& registry_object_reference = *data_.mutable_registry_object_reference();
    registry_object_reference.set_type(Api::RegistryObjectType::domain);
    registry_object_reference.set_value(*std::move(value));
}

void PublicRequest::set_registry_object_reference(Lib::KeysetId value) const
{
    auto& registry_object_reference = *data_.mutable_registry_object_reference();
    registry_object_reference.set_type(Api::RegistryObjectType::keyset);
    registry_object_reference.set_value(*std::move(value));
}

void PublicRequest::set_registry_object_reference(Lib::NssetId value) const
{
    auto& registry_object_reference = *data_.mutable_registry_object_reference();
    registry_object_reference.set_type(Api::RegistryObjectType::nsset);
    registry_object_reference.set_value(*std::move(value));
}

void PublicRequest::set_type(std::string value) const
{
    data_.set_type(std::move(value));
}

void PublicRequest::set_registrar_id(std::string value) const
{
    data_.mutable_registrar_id()->set_value(std::move(value));
}

void PublicRequest::set_response_email(std::string value) const
{
    data_.set_response_email(std::move(value));
}

void PublicRequest::set_create_time(const LibFred::PublicRequest::PublicRequestData::TimePoint& value) const
{
    set_time(value, *data_.mutable_create_time());
}

void PublicRequest::set_creation_id(std::string value) const
{
    data_.mutable_creation_id()->set_value(std::move(value));
}

void PublicRequest::set_resolve_time(const LibFred::PublicRequest::PublicRequestData::TimePoint& value) const
{
    set_time(value, *data_.mutable_resolve_time());
}

void PublicRequest::set_resolving_id(std::string value) const
{
    data_.mutable_resolving_id()->set_value(std::move(value));
}

void PublicRequest::set_status(LibFred::PublicRequest::Status::Enum value) const
{
    data_.set_status([&]()
    {
        switch (value)
        {
            case LibFred::PublicRequest::Status::opened:
                return Api::Status::opened;
            case LibFred::PublicRequest::Status::resolved:
                return Api::Status::resolved;
            case LibFred::PublicRequest::Status::invalidated:
                return Api::Status::invalidated;
        }
        return Api::Status::undefined;
    }());
}

void PublicRequest::set_on_status_action(LibFred::PublicRequest::OnStatusAction::Enum value) const
{
    data_.set_on_status_action([&]()
    {
        switch (value)
        {
            case LibFred::PublicRequest::OnStatusAction::scheduled:
                return Api::OnStatusAction::scheduled;
            case LibFred::PublicRequest::OnStatusAction::processed:
                return Api::OnStatusAction::processed;
            case LibFred::PublicRequest::OnStatusAction::failed:
                return Api::OnStatusAction::failed;
        }
        return Api::OnStatusAction::undefined;
    }());
}

}//namespace Fred::PublicRequest::Wrapper::Grpc
}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

using namespace Fred::PublicRequest::Wrapper::Grpc;

PublicRequest Fred::PublicRequest::Wrapper::Grpc::make_wrapper(Api::PublicRequest& data)
{
    return PublicRequest{data};
}
