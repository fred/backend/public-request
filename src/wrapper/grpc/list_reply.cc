/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/wrapper/grpc/list_reply.hh"
#include "src/wrapper/grpc/common_types.hh"
#include "src/lib/exceptions.hh"

#include <algorithm>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {
namespace Grpc {

ListReply::ListReply(grpc::ServerWriter<Api::ListReply>& writer, grpc::ServerContext* context)
    : writer_{writer},
      context_{context},
      reply_{}
{ }

namespace {

struct StreamHasBeenCancelled : Lib::Cancelled
{
    const char* what() const noexcept override { return "the stream has been cancelled"; }
};

struct StreamHasBeenClosed : Lib::Closed
{
    const char* what() const noexcept override { return "the stream has been closed"; }
};

}//namespace Fred::PublicRequest::Wrapper::Grpc::{anonymous}

void ListReply::set_public_requests(std::size_t size, Iterator begin, Iterator end)
{
    if (context_->IsCancelled())
    {
        throw StreamHasBeenCancelled{};
    }

    static constexpr auto chunk_max_size = 0x0100;
    for (std::size_t chunk_offset = 0; chunk_offset < size; chunk_offset += chunk_max_size)
    {
        reply_.clear_data();
        auto& data = *reply_.mutable_data();
        const auto items_rest = size - chunk_offset;
        const auto chunk_size = items_rest < chunk_max_size ? items_rest
                                                            : chunk_max_size;
        auto& public_requests = *data.mutable_public_requests();
        public_requests.Reserve(chunk_size);
        const auto chunk_end = begin + chunk_size;
        std::for_each(begin, chunk_end, [&](auto&& src)
        {
            auto wrapper = make_wrapper(*public_requests.Add());
            wrapper(src);
        });
        begin = chunk_end;
        const bool last_chunk = begin == end;
        if (!last_chunk)
        {
            if (context_->IsCancelled())
            {
                throw StreamHasBeenCancelled{};
            }
            const bool success = writer_.Write(reply_);
            if (!success)
            {
                throw StreamHasBeenClosed{};
            }
        }
    }
}

void ListReply::finish() &&
{
    if (context_->IsCancelled())
    {
        throw StreamHasBeenCancelled{};
    }
    auto options = ::grpc::WriteOptions{};
    const bool success = writer_.Write(reply_, options.set_last_message());
    if (!success)
    {
        throw StreamHasBeenClosed{};
    }
}

}//namespace Fred::PublicRequest::Wrapper::Grpc
}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

using namespace Fred::PublicRequest::Wrapper::Grpc;

ListReply Fred::PublicRequest::Wrapper::Grpc::make_wrapper(
        grpc::ServerWriter<Api::ListReply>& writer,
        grpc::ServerContext* context)
{
    return ListReply{writer, context};
}
