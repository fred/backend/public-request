/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_REPLY_HH_448E01B76C45256F8013E94E101E55C4//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_REPLY_HH_448E01B76C45256F8013E94E101E55C4

#include "src/wrapper/get_reply.hh"

#include "libfred/public_request/get_public_request.hh"

#include "fred_api/public_request/service_public_request_info_grpc.grpc.pb.h"

namespace Fred {
namespace PublicRequest {
namespace Wrapper {
namespace Grpc {

class GetReply;

GetReply make_wrapper(Api::GetReply& reply);

class GetReply : public Wrapper::GetReply<GetReply>
{
public:
    void set_data(LibFred::PublicRequest::PublicRequestData data) const;
    void set_exception_public_request_does_not_exist() const;
private:
    explicit GetReply(Api::GetReply& reply);
    Api::GetReply& reply_;
    friend GetReply make_wrapper(Api::GetReply& reply);
};

}//namespace Fred::PublicRequest::Wrapper::Grpc
}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//GET_REPLY_HH_448E01B76C45256F8013E94E101E55C4
