/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RESOLVE_REPLY_HH_AF6BC444DA102DD10377E112FABEF97C//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define RESOLVE_REPLY_HH_AF6BC444DA102DD10377E112FABEF97C

#include "src/wrapper/resolve_reply.hh"

#include "libfred/public_request/get_public_request.hh"

#include "fred_api/public_request/service_public_request_processing_grpc.grpc.pb.h"

namespace Fred {
namespace PublicRequest {
namespace Wrapper {
namespace Grpc {

class ResolveReply;

ResolveReply make_wrapper(Api::ResolveReply& reply);

class ResolveReply : public Wrapper::ResolveReply<ResolveReply>
{
public:
    void set_exception_public_request_does_not_exist() const;
    void set_exception_cannot_be_processed() const;
private:
    explicit ResolveReply(Api::ResolveReply& reply);
    Api::ResolveReply& reply_;
    friend ResolveReply make_wrapper(Api::ResolveReply& reply);
};

}//namespace Fred::PublicRequest::Wrapper::Grpc
}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//RESOLVE_REPLY_HH_AF6BC444DA102DD10377E112FABEF97C
