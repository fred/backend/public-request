/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/wrapper/grpc/get_types_reply.hh"
#include "src/wrapper/grpc/public_request_type.hh"

#include <algorithm>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {
namespace Grpc {

GetTypesReply::GetTypesReply(Api::GetTypesReply& data)
    : data_{data}
{
    data_.Clear();
}

void GetTypesReply::set_types(std::size_t size, TypeTraitsIterator begin, TypeTraitsIterator end)
{
    auto* const types = data_.mutable_data()->mutable_types();
    types->Reserve(size);
    std::for_each(begin, end, [&](auto&& type_traits)
    {
        auto item = Wrapper::Grpc::make_wrapper(*(types->Add()));
        item.set_name(type_traits.first);
        item.set_is_resolvable(type_traits.second.is_resolvable);
    });
}

}//namespace Fred::PublicRequest::Wrapper::Grpc
}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

using namespace Fred::PublicRequest::Wrapper::Grpc;

GetTypesReply Fred::PublicRequest::Wrapper::Grpc::make_wrapper(Api::GetTypesReply& data)
{
    return GetTypesReply{data};
}
