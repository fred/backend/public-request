/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PUBLIC_REQUEST_TYPE_HH_1605E6CCA27738994735DCF30DAE804E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PUBLIC_REQUEST_TYPE_HH_1605E6CCA27738994735DCF30DAE804E

#include "src/wrapper/public_request_type.hh"

#include "fred_api/public_request/common_types.pb.h"

namespace Fred {
namespace PublicRequest {
namespace Wrapper {
namespace Grpc {

class PublicRequestType;

PublicRequestType make_wrapper(Api::PublicRequestType& data);

class PublicRequestType : public Wrapper::PublicRequestType<PublicRequestType>
{
public:
    void set_name(std::string value);
    void set_is_resolvable(bool value);
private:
    explicit PublicRequestType(Api::PublicRequestType& data);
    Api::PublicRequestType& data_;
    friend PublicRequestType make_wrapper(Api::PublicRequestType& data);
};

}//namespace Fred::PublicRequest::Wrapper::Grpc
}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//PUBLIC_REQUEST_TYPE_HH_1605E6CCA27738994735DCF30DAE804E
