/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/wrapper/grpc/resolve_reply.hh"

#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {
namespace Grpc {

ResolveReply::ResolveReply(Api::ResolveReply& reply)
    : reply_{reply}
{
    reply_.Clear();
}

void ResolveReply::set_exception_public_request_does_not_exist() const
{
    reply_.mutable_exception()->mutable_public_request_does_not_exist();
}

void ResolveReply::set_exception_cannot_be_processed() const
{
    reply_.mutable_exception()->mutable_cannot_be_processed();
}

}//namespace Fred::PublicRequest::Wrapper::Grpc
}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

using namespace Fred::PublicRequest::Wrapper::Grpc;

ResolveReply Fred::PublicRequest::Wrapper::Grpc::make_wrapper(Api::ResolveReply& reply)
{
    return ResolveReply{reply};
}
