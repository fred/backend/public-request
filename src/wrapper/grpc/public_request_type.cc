/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/wrapper/grpc/public_request_type.hh"

#include <utility>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {
namespace Grpc {

PublicRequestType::PublicRequestType(Api::PublicRequestType& data)
    : data_{data}
{
    data_.Clear();
}

void PublicRequestType::set_name(std::string value)
{
    data_.set_name(std::move(value));
}

void PublicRequestType::set_is_resolvable(bool value)
{
    data_.set_is_resolvable(value);
}

}//namespace Fred::PublicRequest::Wrapper::Grpc
}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

using namespace Fred::PublicRequest::Wrapper::Grpc;

PublicRequestType Fred::PublicRequest::Wrapper::Grpc::make_wrapper(Api::PublicRequestType& data)
{
    return PublicRequestType{data};
}
