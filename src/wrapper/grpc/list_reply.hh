/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_REPLY_HH_274CAB7DE815BB886275A80BADB92485//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LIST_REPLY_HH_274CAB7DE815BB886275A80BADB92485

#include "src/wrapper/list_reply.hh"
#include "src/lib/common_types.hh"

#include "libfred/public_request/get_public_request.hh"

#include "fred_api/public_request/service_public_request_info_grpc.grpc.pb.h"

#include <vector>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {
namespace Grpc {

class ListReply;

ListReply make_wrapper(grpc::ServerWriter<Api::ListReply>& writer, grpc::ServerContext* context);

class ListReply : public Wrapper::ListReply<ListReply>
{
public:
    using Iterator = std::vector<LibFred::PublicRequest::PublicRequestData>::const_iterator;
    void set_public_requests(std::size_t size, Iterator begin, Iterator end);
    void finish() &&;
private:
    explicit ListReply(grpc::ServerWriter<Api::ListReply>& writer, grpc::ServerContext* context);
    grpc::ServerWriter<Api::ListReply>& writer_;
    grpc::ServerContext* context_;
    Api::ListReply reply_;
    friend ListReply make_wrapper(grpc::ServerWriter<Api::ListReply>& writer, grpc::ServerContext* context);
};

}//namespace Fred::PublicRequest::Wrapper::Grpc
}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//LIST_REPLY_HH_274CAB7DE815BB886275A80BADB92485
