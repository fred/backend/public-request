/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COMMON_TYPES_HH_BFA7226DF6F9ED86863B19DCC4F1DAE8//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define COMMON_TYPES_HH_BFA7226DF6F9ED86863B19DCC4F1DAE8

#include "src/wrapper/common_types.hh"

#include "fred_api/public_request/service_public_request_info_grpc.pb.h"

#include <cstdint>
#include <string>

namespace Fred {
namespace PublicRequest {
namespace Wrapper {
namespace Grpc {

class PublicRequest;

PublicRequest make_wrapper(Api::PublicRequest& data);

class PublicRequest : public Wrapper::PublicRequest<PublicRequest>
{
public:
    void set_public_request_id(std::string value) const;
    void set_numeric_id(std::uint64_t value) const;
    void set_registry_object_reference(Lib::ContactId value) const;
    void set_registry_object_reference(Lib::DomainId value) const;
    void set_registry_object_reference(Lib::KeysetId value) const;
    void set_registry_object_reference(Lib::NssetId value) const;
    void set_type(std::string value) const;
    void set_registrar_id(std::string value) const;
    void set_response_email(std::string value) const;
    void set_create_time(const LibFred::PublicRequest::PublicRequestData::TimePoint& value) const;
    void set_creation_id(std::string value) const;
    void set_resolve_time(const LibFred::PublicRequest::PublicRequestData::TimePoint& value) const;
    void set_resolving_id(std::string value) const;
    void set_status(LibFred::PublicRequest::Status::Enum value) const;
    void set_on_status_action(LibFred::PublicRequest::OnStatusAction::Enum value) const;
private:
    explicit PublicRequest(Api::PublicRequest& data);
    Api::PublicRequest& data_;
    friend PublicRequest make_wrapper(Api::PublicRequest& data);
};

}//namespace Fred::PublicRequest::Wrapper::Grpc
}//namespace Fred::PublicRequest::Wrapper
}//namespace Fred::PublicRequest
}//namespace Fred

#endif//COMMON_TYPES_HH_BFA7226DF6F9ED86863B19DCC4F1DAE8
