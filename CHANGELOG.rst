CHANGELOG
=========

.. contents:: Releases
   :backlinks: none
   :local:

1.1.0 (2024-11-13)
------------------

Reflect the renaming in the API:
* Rename ``ObjectType`` to ``RegistryObjectType``.
* Rename ``ObjectId`` to ``RegistryObjectReference``.
* Rename ``PublicRequest.object_id`` to ``PublicRequest.registry_object_reference``.
* Rename ``ListRequest.object_id`` to ``ListRequest.registry_object_reference``.

1.0.1 (2023-06-09)
------------------

* Fix - freeze dependencies (``libfred``, ``libpg``)

1.0.0 (2023-05-23)
------------------

* Initial version
